#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/pa_200.out
#SBATCH -e ./out/pa_200.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J pa_200
# Queue:
#SBATCH --partition=broadwell
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
#SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=80
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=threads
export SLURM_HINT=multithread 


module load gcc/8
module load anaconda/3/5.1
module load scikit-learn/0.19.1


# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/pa.py stratified_subsample bigram 10 200
echo "job finished"