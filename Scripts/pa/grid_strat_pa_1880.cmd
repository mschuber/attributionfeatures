#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/strat_svm_1880.out
#SBATCH -e ./out/strat_svm_1880.err

# Initial working directory:
#SBATCH -D ./
# strat_paob Name:
#SBATCH -strat_pa strat_svm_1880
# Queue:
#SBATCH --partition=broadwell
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
#SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=80
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=threads
export SLURM_HINT=multithread 


module load gcc/8
module load anaconda/3/5.1
module load scikit-learn/0.19.1

# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/grid_search_PA_stratified.py stratified_subsample bigram 3 1880
echo "strat_paob finished"