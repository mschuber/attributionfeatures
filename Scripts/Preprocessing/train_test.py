import json
import os
import multiprocessing as mp
import gc
import argparse
import sys

import ndjson
import jsonlines
import pandas as pd
from itertools import chain
from collections import Counter

from imblearn.under_sampling import RandomUnderSampler
import numpy as np
from sklearn.model_selection import  train_test_split
import scipy as sci




def get_tweet_distrib(path, thresh):

    counter = {}
    f = open(path, 'r', encoding='utf-8')
    for line in f:
        if not line:
            continue
        try:
            line = ndjson.loads(line)[0]
            counter[line['ID']] = counter.get(line['ID'], 0) + 1
        except IOError as e:
            print('line load failed...probably end of file with its many line-breaks')

    vals = np.array(list(chain(counter.values())))
    #get the thresh percent quantile of num tweets  - we want all authors who are above this threshold
    vals = np.quantile(vals, q=thresh, overwrite_input=True)
    return counter, vals

def split_path_unix_win(path):
    #make windows-unix problem go away
    if  '\\' in path:
        path = path.split('\\')
    elif '/' in path:
        path = path.split('/')

    return path

def make_birth_year_bracket(by):
    """ convert the birthyears of a certain range to the center point.
     This is to reduce the number of classes when doing a classification model over regression on age
     """
    by = int(by)
    if 1940 <= by <= 1955:
        return int(1947)
    elif 1956 <= by <= 1969:
        return int(1963)
    elif 1970 <= by <= 1980:
        return int(1975)
    elif 1981 <= by <= 1989:
        return int(1985)
    elif 1990 <= by <= 2000: #change to 2000 from 1999 as our data set has a huge spike there
        return int(1995)  #change center point to 1995

def get_gram_names(path, ranges = [100, 250, 500], parts = 'train_test_val',
                   authorn = '', #or string such as '_1000_authors
                   typ = ('r', 'Reader'), todos =[],
                   mktraintest = False):
    #important function either for iteration over all files to read or to write
    try:
        assert os.path.isdir(path)
    except IsADirectoryError as e:
        raise e

    # get parent dirs (num, char etc.)
    name_dic = {}
    #make selection of original data possible:
    if '_' in parts:
        parts = parts.split('_')
    else:
        parts = [parts]


    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)) and (name in todos or len(todos) ==0):
            name_dic[name] = {}
            #get correct/all child folders
            for subname in os.listdir(os.path.join(path, name)):
                if os.path.isfile(os.path.join(path, name, subname)) and '_concat' in subname:
                    grams = subname.split('_concat')[0]
                    name_dic[name][grams] = {}
                    if not name in ['num', 'emoticon_c', 'polarity', 'vectors']:
                        name_dic[name][grams]["keys"] = name_dic[name][grams].get("keys", list()) + [grams]
                    elif name == 'emoticon_c':
                        name_dic[name][grams]["keys"] = ['emoji', 'emoticon']
                    elif name == 'polarity':
                        name_dic[name][grams]["keys"] = ['polarity', 'subjectivity']
                    elif name == 'num':
                        name_dic[name][grams]["keys"] = ["init_len","prepr_len","mentions","tags","urls","times","emotic_num",
                                                         "emojis_num","numericals"]

                    for ran in ranges:
                        for st in parts:
                            if st == '':
                                key = ''
                            else:
                                key = st
                                st = st+'_'

                            #add path to files and open the files with handles
                            name_dic[name][grams][str(ran)] = {}
                            if mktraintest:
                                #open initial file with all ids in order to be able to make a subset
                                name_dic[name][grams][str(ran)]['f_init'] = open(os.path.join(path, name, str(ran), '{}_{}.ndjson'.format(grams, ran)),
                                                                                 'r', encoding='utf-8')
                                name_dic[name][grams][str(ran)]['reader_init'] = jsonlines.Reader(name_dic[name][grams][str(ran)]['f_init'])

                            os.makedirs(os.path.join(path, name, str(ran), key), exist_ok=True)
                            p = os.path.join(path, name, str(ran), key, '{}{}_{}{}.ndjson'.format(st, grams, ran, authorn))
                            name_dic[name][grams][str(ran)][st+'path'] = p
                            name_dic[name][grams][str(ran)][st+'f'] = open(p, typ[0], encoding='uft-8')
                            if typ[0] == 'r':
                                name_dic[name][grams][str(ran)][st+'reader'] = jsonlines.Reader(name_dic[name][grams][str(ran)][st+'f'])
                            elif typ[0] =='w':
                                name_dic[name][grams][str(ran)][st+'writer'] = jsonlines.Writer(name_dic[name][grams][str(ran)][st+'f'])

    return name_dic

def concat_values(x, cols):
    st = ''
    for col in cols:
        st +='_{}'.format(x[col])
    return st[1:]

def make_train_test_split(ids, strat):
    #split set into three train, val test (we need val for the dyn-part of dyn-aa (otherwise we mix train and test)

    train_ids, test_ids, train_strat, test_strat = train_test_split(ids, strat, train_size=0.5, random_state=123456, shuffle=True, stratify=strat)
    val_ids, test_ids = train_test_split(test_ids, train_size=0.6, random_state=654321,
                                                                  shuffle=True, stratify=test_strat)
    print('train has {} tweets...'.format(len(train_ids)))
    print('val has {} tweets'.format((len(val_ids))))
    print('test has {} tweets'.format((len(test_ids))))
    return train_ids, val_ids, test_ids

def _stratify_data(ids, strats, strategy='not minority', path = False):
#stratifies in an undersampling strategy according to the stras passeed
    if path:
        with open(path, 'r', encoding='utf-8') as f:
            data = ndjson.load(f)
            data = pd.DataFrame(data)
            ids = data[ids].to_numpy().reshape(-1, 1)
            if type(strats) == type(list()):
                strats = np.array(data.apply(lambda x: concat_values(x, strats), axis=1)).reshape(-1, 1)
            else:
                strats = data[strats].to_numpy().reshape(-1, 1)
    else:
        ids = np.array(ids).reshape(-1, 1)
        strats = np.array(strats).reshape(-1, 1)
    print('have initial distirbution {}'.format(Counter(strats.flatten().tolist())))
    rus = RandomUnderSampler(sampling_strategy=strategy, random_state=int(1234567))
    ids, strats = rus.fit_resample(ids, strats)
    print('have new dsitrbution {}'.format(Counter(strats.flatten().tolist())))
    return ids.flatten().tolist()

def _equalize(labels, authors, balance, isfirst='both'):
    # we equalize across gender and then across age also (we exlude the nonbinary gender ones as they are so very few
    labels = labels.loc[labels['gender'].isin(['male', 'female']), :]
    # for gender
    ids_gender = None
    ids_age = None
    if isfirst or isfirst == 'both':
        print('sampling for gender')
        strat = labels.apply(lambda x: concat_values(x, balance[0]), axis=1).to_numpy()
        ids_gender = _stratify_data(labels['id'].to_numpy().reshape(-1, 1), strat.reshape(-1, 1),
                                   strategy={'male': authors // 2, 'female': authors // 2},
                                   path=False)
        # make dic for fast check whether id is in set or not

        ids_gender = {key: labels.loc[labels['id'] == key, ['gender', 'centered_age','birthyear']].to_dict('records')[0] for key in
                      ids_gender}
    
    if not isfirst or isfirst == 'both':
        # stratify across age and gender
        strat = labels.apply(lambda x: concat_values(x, balance[1]), axis=1).to_numpy()

        un = np.unique(strat)
        strategy = {str(key): (authors // len(un)) for key in un}
        print('sampling for age')
        print(strategy)
        print(un)
        ids_age = _stratify_data(labels['id'].to_numpy().reshape(-1, 1), strat.reshape(-1, 1), strategy=strategy, path=False)
        # make dic for fast check whether id is in set or not
        ids_age = {key: labels.loc[labels['id'] == key, ['gender', 'centered_age','birthyear']].to_dict('records')[0] for key in
                   ids_age}


    return ids_gender, ids_age

def _make_strat_ids(authors, ids_gender, ids_age, num_path, balance, ranges=[100,250,500], is_subset=False):
    #makes the selction from numm for selected labels and then equalizes over amount of tweets


    file_ids_age = {}
    file_ids_gender = {}
    es = 0
    gend_c = 0
    for rn in ranges:
        print('make for min text len of {} chars...'.format(rn))
        if not is_subset: #only for first run we need to traverse the whole file
            file_ids_gender[str(rn)] = []
            file_ids_age[str(rn)] = []
            f = open(os.path.join(num_path, str(rn), 'num_{}.ndjson'.format(str(rn))), 'r', encoding='utf-8')
            for line in f:
                if not line:
                    continue

                try:
                    if type(line) == type(''):
                        try:
                            line = json.loads(line)
                        except:
                            print('try manual loads')
                            line = line.split('{')[1]
                            line = line.split('}')[0]
                            line = '{'+line+'}'
                            line = json.loads(line)
                            print('manual loading worked...')
                    #append to ids dic if they are part of subset
                    if line['ID'] in ids_gender.keys():
                        file_ids_gender[str(rn)].append({'ID': line['ID'], 'tweetID': line['tweetID'], 'uID': '|'.join(line['tweetID']),
                                                      'centered_age': ids_gender[line['ID']]['centered_age'],
                                                      'gender': ids_gender[line['ID']]['gender'],
                                                      'birthyear': ids_gender[line['ID']]['birthyear']})
                        gend_c +=1
                    if line['ID'] in ids_age.keys():
                        file_ids_age[str(rn)].append({'ID': line['ID'], 'tweetID': line['tweetID'], 'uID': '|'.join(line['tweetID']),
                                                      'centered_age': ids_age[line['ID']]['centered_age'],
                                                      'gender': ids_age[line['ID']]['gender'],
                                                      'birthyear': ids_age[line['ID']]['birthyear']})


                except Exception as e:
                    print(type(line))
                    print(line)
                    es += 1
                    print(es)
                    raise e
            f.close()
            gend = pd.DataFrame(file_ids_gender[str(rn)]).drop_duplicates(subset=['uID'])
            file_ids_gender[str(rn)] = gend.copy(deep=True) #from here on we pass pandas frames
            age = pd.DataFrame(file_ids_age[str(rn)]).drop_duplicates(subset=['uID'])
            file_ids_age[str(rn)] = age.copy(deep=True) #from here on we pass pandas frames
            gc.collect()
        else: #we already have the subset and only traverse that one
            file_ids_age[str(rn)] = ids_age[str(rn)].copy(deep=True) #here we get our pandas frames back - now these here refer to pandas frames
            age = ids_age[str(rn)].copy(deep=True)
            file_ids_gender[str(rn)] = ids_gender[str(rn)].copy(deep=True)
            gend = ids_gender[str(rn)].copy(deep=True)

        #put the now unbalanced (in terms of tweets) ids to file
        sp_path = split_path_unix_win(num_path)[:-1]
        os.makedirs(os.path.join(*(sp_path + ['gender_ids'])), exist_ok=True)
        os.makedirs(os.path.join(*(sp_path + ['age_ids'])), exist_ok=True)
        gend.to_json(os.path.join(*(sp_path + ['gender_ids', 'ids_gender_{}_{}_authors_unbalanced.json'.format(rn, authors)])))
        age.to_json(os.path.join(*(sp_path + ['age_ids', 'ids_age_unbalanced_{}_{}_authors_unbalanced.json'.format(rn, authors)])))


        #rebalance the ids also in terms of tweets (sample the number of ids for every author should appear the same amount of times)
        gend['strat'] = ['_'.join(a) for a in zip(*[gend[el].astype(str) for el in balance[0]])]
        ids = _stratify_data(gend['uID'].to_numpy().reshape(-1, 1), gend['strat'].to_numpy().reshape(-1, 1))
        gend = gend.loc[gend['uID'].isin(ids), :]
        print('restratified for {}..that is the final balance'.format(balance[0]))
        print(Counter(gend["_".join(balance[0])]))

        tr, val, test = make_train_test_split(np.array(ids).flatten(), gend['strat'].to_numpy().flatten())
        #save to file
        for name, idx in [['train', tr], ['val', val], ['test',test]]:
            os.makedirs(os.path.join(*(sp_path + ['gender_ids', name])), exist_ok=True)
            print('when saving {}_age has {} rows'.format(name, gend.loc[gend['uID'].isin(idx), :].shape[0]))
            gend.loc[gend['uID'].isin(idx), :].to_json(os.path.join(*(sp_path + ['gender_ids', name,'{}_ids_gender_{}_{}_authors_balanced.json'.format(name, rn, authors)])))


        gend.to_json(os.path.join(*(sp_path + ['gender_ids', 'ids_gender_{}_{}_authors_balanced.json'.format(rn, authors)])))

        age['strat'] = ['_'.join(a) for a in zip(*[age[el].astype(str) for el in balance[1]])]
        ids = _stratify_data(age['uID'].to_numpy().reshape(-1, 1), age['strat'].to_numpy().reshape(-1, 1))
        age = age.loc[age['uID'].isin(ids), :]
        print('restratified for {}..that is the final balance'.format(balance[1]))
        print(Counter(age[balance[1][0]]))
        tr, val, test = make_train_test_split(np.array(ids).flatten(), age['strat'].to_numpy().flatten())
        #save to file
        for name, idx in [['train', tr], ['val', val], ['test',test]]:
            os.makedirs(os.path.join(*(sp_path + ['age_ids', name])), exist_ok=True)
            print('when saving {}_age has {} rows'.format(name, age.loc[age['uID'].isin(idx), :].shape[0]))
            age.loc[age['uID'].isin(idx), :].to_json(os.path.join(*(sp_path + ['age_ids', name, '{}_ids_age_{}_{}_authors_balanced.json'.format(name, rn, authors)])))

        age = age.loc[age['uID'].isin(ids), :]
        age.to_json(os.path.join(*(sp_path + ['age_ids', 'ids_age_{}_{}_authors_balanced.json'.format(rn, authors)])))
        #file_ids_age[str(rn)] = age.to_dict('records')

    return file_ids_gender, file_ids_age

def make_label_subset(labelpath, num_path, threshids, ranges=[100,250,500], authors=[1000,500,150,50], sbs='creator', age_gender=True):
    '''function works in the following order:
    1. create a balanced subset for the each class in classes (i.e. author, age)
    2. then make take the subset of the ids and load the tweet_num file to make a new ids file
    3. subsample the ids file again as it is now imbalanced due to the fact that the authors have different amaount of tweets
    -> so we balance it again
    That should be the fastest order as the num-file is quite large (between 300000 and 4million lines)
    '''
    #make labels over which to balance for age
    if age_gender:
        balance = [['gender'], ['centered_age', 'gender']]
    else:
        balance = [['gender'], ['centered_age']]


    with open(labelpath, 'r', encoding='utf-8') as labs:
        labels = pd.DataFrame(ndjson.load(labs))
    if threshids != []:
        labels = labels.loc[labels['id'].isin(threshids),:]

    #make upper boundary for birthyear
    labels = labels.loc[(labels['birthyear'] <= 2000) & (labels['occupation']==sbs),:]

    labels['centered_age'] = labels.apply(lambda x: make_birth_year_bracket(x['birthyear']), axis=1)
    authors = sorted(authors, reverse=True)
    auth = authors[0]
    print('doing num authors {}'.format(auth))
    ids_gender, ids_age = _equalize(labels, auth, balance=balance)
    file_ids_gender, file_ids_age = _make_strat_ids(authors= auth, ids_gender=ids_gender, ids_age=ids_age, balance=balance,
                                                    num_path=num_path, ranges=ranges, is_subset=False)

    #now weiterate over the rest; as these are subsets of the initial subset, we can simply look through the smaller one...makes everything go (much) faster
    for auth in authors[1:]:
        print('doing num authors: {}'.format(auth))
        #only use id subset from here on
        _, ids_age = _equalize(labels.loc[labels['id'].isin(list(ids_age.keys())),:], auth, balance, isfirst=False)
        ids_gender, _ = _equalize(labels.loc[labels['id'].isin(list(ids_gender.keys())),:], auth, balance, isfirst=True)
        for rn in ranges:
            #subset our pandas frames accordingly
            file_ids_gender[str(rn)] = file_ids_gender[str(rn)].loc[file_ids_gender[str(rn)]['ID'].isin(ids_gender),:]
            print('Have distrib {} for {} authors and tweetlen {}'.format(Counter(file_ids_gender[str(rn)]['gender']), auth, rn))
            file_ids_age[str(rn)] = file_ids_age[str(rn)].loc[file_ids_age[str(rn)]['ID'].isin(ids_age), :]
            print('Have distrib {} for {} authors and tweetlen {}'.format(Counter(file_ids_gender[str(rn)]['centered_age']), auth, rn))
        file_ids_gender, file_ids_age = _make_strat_ids(authors = auth, ids_gender = file_ids_gender, ids_age=file_ids_age, balance=balance,
                                                        ranges = ranges, is_subset=True, num_path=num_path)


    print('made the author subsets with gender and age balance for {}...'.format(sbs))

def _main(path, workset, parts, labelpath, minchars = [100, 250, 500], authors = [1000, 500, 150, 50], thresh = 0.1):
    path = split_path_unix_win(path)

    for p in parts:
        path_p = os.path.join(*(path + [workset, p]))

        counter, thresh_c = get_tweet_distrib(os.path.join(path_p, 'num', 'num_concat.ndjson'),  thresh=thresh)
        #print('minimum count for {} is {}'.format(p, min(counter.values())))
        #print('minimum count for {} is {}'.format(p, max(counter.values())))
        #print('the variance in tweet amounts is {}'.format(np.var(list(chain(counter.values())))))
        #print('the threshold for the 10% tweet-amount quantile is {}'.format(thresh))

        #make idlist
        idlist = [key for key, value in counter.items() if key >=thresh_c]
        #idlist = []
        make_label_subset(labelpath=labelpath, num_path=os.path.join(path_p, 'num'), threshids=idlist, ranges=minchars, authors=authors, sbs=p)

        #subselect the other


if __name__ == "__main__":
    command = False
    if not command:
        args = {}
        args["path"] = "../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/preprocessed"
        args["labelpath"] = "../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/labels.ndjson"
        args["workset"] ='workset'
        args["part"] =['creator', 'performer']
        args["minchars"] = [100, 250, 500]
        args["authors"] = [1000, 500, 150, 50]
        args["threshold"] = 0.1




    else:
        # parse arguements
        argparser = argparse.ArgumentParser(description='Arguements for preprocessing and making the ngrams')
        argparser.add_argument_group('required arguments')
        argparser.add_argument('-p', '--path', help='Path to parent input directory (relative or absolute)',
                               required=True)
        argparser.add_argument('-l', '--labelpath', help='Path to labels.ndjson', required=True)
        argparser.add_argument('--workset',
                               help='Sub-Directory of parent input-directory (if it exists). Helpful if script is executed in loop on many worksets', required=True)
        argparser.add_argument('--part', required=True, nargs='*',
                               help='Sub-Sub-Directory of parent input-directory (if it exists). Helpful if script is executed in loop on many types')
        argparser.add_argument('-a', '--authors', nargs='*', help='Number of authors in subset', required=True)
        argparser.add_argument('-c', '--minchars', nargs='*', help='min char files per learning instance', required=True)
        argparser.add_argument('-t', '--threshold', nargs='*',help='threshold for minimum number of tweets per author', required=True)


        # parse arguements
        args = vars(argparser.parse_args())



    _main(path=args['path'], workset=args['workset'], parts=args['part'],
              labelpath=args['labelpath'], minchars = args['minchars'], authors = args['authors'], thresh = args['threshold'])


    print('done')


