from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, HashingVectorizer, TfidfVectorizer
from sklearn.preprocessing import MinMaxScaler, StandardScaler, OneHotEncoder
from sklearn.linear_model import LinearRegression
from sklearn.decomposition import PCA, TruncatedSVD, IncrementalPCA
from sklearn.pipeline import Pipeline
from tensorflow.keras.utils import to_categorical
import pandas as pd
import demoji
import pkg_resources
pkg_resources.require("Scipy==1.7.0")
import scipy
import numpy as np
import random
import pickle
import math
import statistics
import json
import ndjson
import os
import gc
import multiprocessing as mp
import argparse
import ast
import sys
import psutil
sys.path.append('..')
sys.path.append('../ML')
from ML import ml_utils

'''def make_tfidf_old(featuretyp:str, minTw_gram_auth:list, path:str, id_type:str, subset ='train', rerun =False, hash =True):

    minTW = minTw_gram_auth[0]
    numAuthors = minTw_gram_auth[2]
    gram_files = {}
    vocab_exists = False
    id0 = id_type.split('_')[0]
    mak_key =0
    vectorizer_dic = {}
    path_parts = ml_utils.split_path_unix_win(path)
    id_file = "{}_ids_{}_{}_{}_authors_balanced.json".format(subset, id0, minTw_gram_auth[0], minTw_gram_auth[2])
    id_file_val = "{}_ids_{}_{}_{}_authors_balanced.json".format('val', id0, minTw_gram_auth[0],
                                                             minTw_gram_auth[2])
    id_file_test = "{}_ids_{}_{}_{}_authors_balanced.json".format('test', id0, minTw_gram_auth[0],
                                                             minTw_gram_auth[2])
    with open(os.path.join(*path_parts, id_type, subset, id_file), 'r', encoding='utf-8') as id:
        ids = set(pd.read_json(id)['uID'].to_list())
    #so that we save some passes (very time expensive)
    with open(os.path.join(*path_parts, id_type, 'val', id_file_val), 'r', encoding='utf-8') as id:
        ids_val = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type, 'test', id_file_test), 'r', encoding='utf-8') as id:
        ids_test = set(pd.read_json(id)['uID'].to_list())
    csr_saver = False

    for gram in sorted(minTw_gram_auth[1]):
        if hash:
            vectorizer = HashingVectorizer(analyzer=ml_utils.identity_analyzer, norm=None, alternate_sign=False)
            vectorizer_dic[gram] = vectorizer
            #do this for non-hash
        else:
            path_vocab = os.path.join(*path_parts,
                                         featuretyp, str(minTW) ,'vocab', id0)
            os.makedirs(path_vocab, exist_ok=True)
            path_vocab_red = os.path.join(path_vocab,
                                      '{}_grams_{}_{}_{}_authors_reduced.vocab'.format(featuretyp, gram, minTW, numAuthors))
            path_vocab = os.path.join(path_vocab, '{}_grams_{}_{}_{}_authors.vocab'.format(featuretyp, gram, minTW, numAuthors))
            vocab_exists = os.path.exists(path_vocab)
            #if not train, we already have a vocab (normally)
            if vocab_exists and not rerun:
                vocab = ml_utils.get_vocab(path_vocab_red)
            else:
                vocab = {}

        ##acess files i.e. char_grams_3_500, char_grams_4_500, char_grams_5_500
        gram_files['smpl_load'] = os.path.exists(os.path.join(*path_parts,featuretyp, str(minTW) ,subset,
                                                                '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                             featuretyp, gram, minTW, numAuthors)))
        print(os.path.join(*path_parts,featuretyp, str(minTW) ,subset,
                                                                '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                             featuretyp, gram, minTW, numAuthors)))
        print(gram_files['smpl_load'])
        if not gram_files['smpl_load'] or rerun:
            #TODO
            sys.exit('Error wanted to redo files...but tehy should already be there...')

            os.makedirs(os.path.join(*path_parts,featuretyp, str(minTW) ,id0,subset), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW),id0, 'test'), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW),id0, 'val'), exist_ok=True)

            t = os.path.join(*path_parts, featuretyp, str(minTW),
                                       '{}_grams_{}_{}.ndjson'.format(featuretyp, gram, minTW))

            if numAuthors != 1000 and os.path.exists(os.path.join(*path_parts,
                                                   featuretyp, str(minTW) ,id0,subset,
                                                          '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, 1000))):

                t =os.path.join(*path_parts,featuretyp, str(minTW) ,id0,subset,
                                '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, 1000))


            gram_files[str(gram)+'save'] = open(os.path.join(*path_parts,
                                                   featuretyp, str(minTW) ,id0,subset,
                                                          '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, numAuthors)),
                                           'w',encoding='utf-8')
            gram_files[str(gram) + 'save_test'] = open(os.path.join(*path_parts,
                                                               featuretyp, str(minTW),id0, 'test',
                                                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format('test',
                                                                                                            featuretyp,
                                                                                                            gram, minTW,
                                                                                                            numAuthors)),
                                                  'w', encoding='utf-8')
            gram_files[str(gram) + 'save_val'] = open(os.path.join(*path_parts,
                                                               featuretyp, str(minTW),id0, 'val',
                                                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format('val',
                                                                                                            featuretyp,
                                                                                                            gram, minTW,
                                                                                                            numAuthors)),
                                                  'w', encoding='utf-8')

        else:
            #we already have a subset - simply load it
            t = os.path.join(*path_parts,featuretyp, str(minTW),id0 ,subset,
                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, numAuthors))
        print(t)
        gram_files[str(gram)] = open(t, 'r', encoding='utf-8')

        #first we check whether the id for current line is present in ids
        #if yes, we hash - we do this for every gram type
        #set dummy in line
        lines = [1]
        doc_counter = 0
        #only run the loop if we do a hash or vocab does not exists
        if not vocab_exists or hash:
            while True:
                if doc_counter %1000 == 0:
                    print("wrote {} to file... :)".format(doc_counter))
                #read the first 1000 lines
                lines = ml_utils.batch_read(gram_files[str(gram)], batchsize=1000, to_json=True)
                if not lines:
                    break
                lines = pd.DataFrame.from_dict(lines, orient='columns')
                # if we do not have subseted the files yet
                if (not gram_files['smpl_load']  or rerun):
                    val_lines = ml_utils.select_uids(lines, ids_val)
                    test_lines = ml_utils.select_uids(lines, ids_test)
                    lines = ml_utils.select_uids(lines, ids)
                    if lines.shape[0] ==0:
                        #no ids in current lines
                        continue
                    #update doc_counter
                    doc_counter += lines.shape[0]
                    #write subset to file
                    ml_utils.pandas_to_ndjson(lines, gram_files[str(gram)+'save'])
                    ml_utils.pandas_to_ndjson(val_lines, gram_files[str(gram) + 'save_val'])
                    ml_utils.pandas_to_ndjson(test_lines, gram_files[str(gram) + 'save_test'])
                #otherwise the lines are part of the subset and we do not have to select and save to file
                if hash:
                    if str(gram) + '_sparse' in gram_files.keys():
                        #if we do have a sparse matrix in dict and we have to do hash
                        gram_files[str(gram) + '_sparse'] = scipy.sparse.vstack((gram_files[str(gram) + '_sparse'],
                                                                            vectorizer.transform(lines['{}_grams_{}'.format(
                                                                                featuretyp, gram)]))
                                                                           )
                    else:
                        ##use hash vectorizer to keep in memory low
                        gram_files[str(gram) + '_sparse'] = vectorizer.transform(
                            lines['{}_grams_{}'.format(featuretyp, gram)])

                else:
                    vocab = ml_utils.update_vocab(vocab, lines, key='{}_grams_{}'.format(featuretyp, gram))

            if not vocab_exists and not hash:
                #save vocab to file
                ml_utils.make_vocab(vocab, path_vocab)
                one_percent = round(doc_counter * 0.01)
                vocab, max_key = ml_utils.reduce_vocab(vocab, one_percent)
                #save reduced vocab to file
                ml_utils.make_vocab(vocab, path_vocab_red)

            if hash:
                # update our csr matrix and set all words to zero with a doc-frequency below 1%
                numdocs = gram_files[str(gram) + '_sparse'].get_shape()[0]
                colsums = np.array(gram_files[str(gram) + '_sparse'].sum(axis=0)).flatten()
                prop = colsums / numdocs
                colinds = (prop > 0.01).nonzero()
                gram_files[str(gram) + '_sparse'] = ml_utils.zero_columns(gram_files[str(gram) + '_sparse'], colinds)


            #important all files with their tweetids are in the same order
            #we iterate over the length of grams in ascending order...conseqently in each run we can make a tfidf-transformer/vectorizer and save it to disk
            # i.e. first for chargrams1, then for chargrams 1-2, then for chargrams1-2-3 and so forth
            #these here are the parameters taken from Custodio et al. 2021
        if not hash:
            #here we make our sparse matrix because only now do we have the full vocab
            vectorizer = CountVectorizer(vocabulary=vocab, analyzer=ml_utils.identity_analyzer)
            vectorizer_dic[gram] = vectorizer
            if gram_files['smpl_load']:
                filelines = gram_files[str(gram)]
                filelines.seek(0)
            else:
                filelines = open(gram_files[gram+'save'].name, 'r', encoding='utf-8')
            #make sure max_key is set
            #if max_key == 0:
            #    for key in vocab.keys():
            #        if key > max_key:
            #            max_key = key

            lines = [1]
            while lines:
                lines = ml_utils.batch_read(filelines, batchsize=1000, to_json=True)
                if not lines:
                    continue
                lines = pd.DataFrame.from_dict(lines, orient='columns')

                #csr = ml_utils.construct_tdm(lines['{}_grams_{}'.format(featuretyp, gram)].to_list(), vocab, maxcol=max_key)
                csr = vectorizer.transform(lines['{}_grams_{}'.format(featuretyp, gram)].to_list())
                if str(gram) + '_sparse' in gram_files.keys():
                    gram_files[str(gram) + '_sparse'] = scipy.sparse.vstack((gram_files[str(gram) + '_sparse'], csr))
                else:
                    gram_files[str(gram) + '_sparse'] = csr

                lines = [1]
        #add csr column-wise together
        if csr_saver:
            fin_csr = scipy.sparse.hstack((fin_csr, gram_files[str(gram) + '_sparse']))
        else:
            csr_saver = True
            fin_csr = gram_files[str(gram) + '_sparse'].copy()
        print(fin_csr)
        #close files
        if not gram_files['smpl_load'] or rerun:
            gram_files[str(gram) + 'save'].close()
            gram_files[str(gram) + 'save_val'].close()
            gram_files[str(gram) + 'save_test'].close()
        gram_files[str(gram)].close()
    #now we make our tfidf transformation
    gramsstring = '_'.join([str(el) for el in minTw_gram_auth[1]])
    if subset == 'train':
        trans = TfidfTransformer(norm ='l2', use_idf=False, smooth_idf=True, sublinear_tf=True)
        trans = trans.fit(fin_csr)

        os.makedirs(os.path.join(*path_parts[0:-3], 'models', *path_parts[4:], featuretyp, id0), exist_ok=True)
        with open(os.path.join(*path_parts[0:-3], 'models', *path_parts[4:], featuretyp, id0,
                               'tfidf_{}_grams_{}_hash_{}.p'.format(featuretyp, gramsstring, hash)), 'wb') as p:
            pickle.dump(trans, p)
    else:

        with open(os.path.join(*path_parts[0:-3], 'models', *path_parts[4:], featuretyp, id0,
                               'tfidf_{}_grams_{}_hash_{}.p'.format(featuretyp, gramsstring, hash)), 'rb') as p:
            trans = pickle.load(p)

    tfidf = trans.transform(fin_csr)

    return trans, vectorizer_dic, tfidf'''


def make_logistic(output_shape):
    if output_shape == 1:
        activation = 'sigmoid'
        loss = 'binary_crossentropy'
    else:
        activation = 'softmax'
        loss = 'categorical_crossentropy'

    model = tf.keras.models.Sequential()
    # use dselayed building - no input shape specified till we actually build the model (i.e. when we have the data)
    model.add(tf.keras.layers.Dense(output_shape, kernel_regularizer=tf.keras.regularizers.l2(float=0.01),
                                    activation=activation))
    model.compile(optimizer='sgd', loss=loss, metrics=['accuracy'])

    return model


def make_emoji_vector(featuretyp: str, minTw_gram_auth: list, path: str, id_type: str, subset='train', id_subset=[],
                      vectorizer=None, load_sparse=False):
    # get dic for emoji names
    set_ids = {}
    set_ids[featuretyp + ''] = []
    path_parts = ml_utils.split_path_unix_win(path)

    id0 = id_type.split('_')[0]
    model_dir = ml_utils.make_save_dirs(path)
    os.makedirs(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0), exist_ok=True)
    subset_path = os.path.join(*path_parts, featuretyp, str(minTw_gram_auth[0]), id0, subset)
    os.makedirs(os.path.join(subset_path, 'vectorized'), exist_ok=True)
    sparse_path = os.path.join(subset_path, 'vectorized', "vectorized_{}_{}_{}_{}_authors".format(subset, featuretyp,
                                                                                                  minTw_gram_auth[0],
                                                                                                  minTw_gram_auth[2]))
    scaled_path = os.path.join(subset_path, 'vectorized', "vec_scaled_{}_{}_{}_{}_authors".format(subset, featuretyp,
                                                                                                  minTw_gram_auth[0],
                                                                                                  minTw_gram_auth[2]))
    if subset == 'crossval':
        subset = 'train'
        true_train = False
    elif subset == 'train':
        true_train = True
    else:
        true_train = False

    f = open(os.path.join(*path_parts, featuretyp, str(minTw_gram_auth[0]), id0, subset,
                          "{}_{}_{}_{}_authors.ndjson".format(subset,
                                                              featuretyp,
                                                              minTw_gram_auth[0],
                                                              minTw_gram_auth[2])),
             'r', encoding='utf-8')

    # correct emoji count errors
    with open(os.path.join(*path_parts, 'num', str(minTw_gram_auth[0]), id0, subset,
                           "{}_{}_{}_{}_authors.ndjson".format(subset,
                                                               'num',
                                                               minTw_gram_auth[0],
                                                               minTw_gram_auth[2])),
              'r', encoding='utf-8') as n:
        num = ndjson.load(n)
    num = pd.DataFrame.from_dict(num, orient='columns')
    num = num.drop_duplicates(subset=['uID'], keep='first').reset_index(drop=True)


    if true_train:
        demojifile = os.path.join(demoji.DIRECTORY, 'codes.json')
        with open(demojifile, 'r') as d:
            demojis = json.load(d)
        # make demojivocab
        vocab = {}
        i = 0
        for emoji in demojis['codes'].values():
            if emoji not in vocab.keys():
                vocab[emoji] = i
                i += 1

        ##add emoticons to vocab
        for emot in [r':\\', r':-/', r':-\\', r':)', r';)', r':-)', r';-)', r':(', r':-(', r':-o', r':o', r'<3']:
            vocab[emot] = i
            i += 1

        vectorizer = CountVectorizer(analyzer=ml_utils.identity_analyzer, vocabulary=vocab)
        scaler = StandardScaler()
        with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                  'vectorizer_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[1], minTw_gram_auth[2])),
                  'wb') as p:
            pickle.dump(vectorizer, p)


    else:
        if not vectorizer:
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                      'vectorizer_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[1], minTw_gram_auth[2])),
                      'rb') as p:
                vectorizer = pickle.load(p)

            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                      'scaler_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[1], minTw_gram_auth[2])),
                      'rb') as p:
                scaler = pickle.load(p)
    if not load_sparse:
        exists = False
        id_tester = set()
        while True:
            lines = ml_utils.batch_read(fileobj=f, batchsize=10000, to_json=True)
            if not lines:
                break
            lines = pd.DataFrame.from_dict(lines, orient='columns')
            lines, id_tester = ml_utils.duplicate_uid_tester(lines, id_tester)
            num = ml_utils.reset_num(num, lines)
            lines, id_tester = ml_utils.duplicate_uid_tester(lines, id_tester)
            if lines.shape[0] == 0:
                # no ids in current lines
                continue
            if id_subset:
                lines = ml_utils.select_uids(lines, id_subset)
                if lines.shape[0] == 0:
                    # no ids in current lines
                    continue
            # save the ids in order
            set_ids[featuretyp + ''].extend(lines['uID'].to_list())
            emoti_sparse = vectorizer.transform(lines['emoticon'].to_numpy())
            emoji_sparse = vectorizer.transform((lines['emoji'].to_numpy()))
            if exists:
                csr_out = scipy.sparse.vstack((csr_out, emoji_sparse + emoti_sparse))
            else:
                csr_out = emoji_sparse + emoti_sparse
                exists = True
        if true_train:
            scaled = scaler.fit(csr_out)
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                      'scaler_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[1], minTw_gram_auth[2])),
                      'wb') as p:
                pickle.dump(scaler,p)
        else:
            scaled = scaler.transform(csr_out)
        np.save(file=scaled_path, arr=scaled)
        #save sparse
        ml_utils.save_sparse(csr_out, sparse_path)
        with open(os.path.join(subset_path, "vectorized","ids_{}_{}_{}_{}_authors".format(subset, featuretyp, minTw_gram_auth[0], minTw_gram_auth[2])),'wb') as w:
            pickle.dump(set_ids, w)
    else:
        csr_out = ml_utils.load_sparse(sparse_path)
        scaled = np.load(scaled_path)
        with open(os.path.join(subset_path, "vectorized","ids_{}_{}_{}_{}_authors".format(subset, featuretyp, minTw_gram_auth[0], minTw_gram_auth[2])),'wb') as w:
            set_ids = pickle.load(w)
    # save fixed num
    with open(os.path.join(*path_parts, 'num', str(minTw_gram_auth[0]), id0, subset,
                           "{}_{}_{}_{}_authors.ndjson".format(subset,
                                                               'num',
                                                               minTw_gram_auth[0],
                                                               minTw_gram_auth[2])),
              'w', encoding='utf-8') as n:
        ndjson.dump(num.to_dict(orient='records'), n)

    return [vectorizer, scaler], scaled, set_ids


def make_tfidf(featuretyp: str, minTw_gram_auth: list, path: str, id_type: str, subset='train', hash=True, id_subset=[],
               vectorizers=None, trans_in=None,
               load_sparse=False):
    minTW = minTw_gram_auth[0]
    numAuthors = minTw_gram_auth[2]
    gram_files = {}
    id0 = id_type.split('_')[0]
    set_ids = {}
    model_dir = ml_utils.make_save_dirs(path)
    vectorizer_dic = {}

    path_parts = ml_utils.split_path_unix_win(path)
    if subset == 'crossval':
        subset = 'train'
        true_train = False
    elif subset == 'train':
        true_train = True
    else:
        true_train = False
    csr_saver = False
    path_vocab_base = os.path.join(model_dir,
                                   featuretyp, str(minTW), id0, 'vocab')
    os.makedirs(path_vocab_base, exist_ok=True)
    # hash-vectorizer is not completely stateless

    for gram in sorted(minTw_gram_auth[1]):
        set_ids[featuretyp + str(gram)] = []
        vectorizer_dic[str(gram)] = {}
        path_colids = os.path.join(path_vocab_base,
                                   '{}_grams_{}_{}_{}_authors_reduced.colids'.format(featuretyp, gram, minTW,
                                                                                     numAuthors))
        path_vocab_red = os.path.join(path_vocab_base,
                                      '{}_grams_{}_{}_{}_authors_reduced.vocab'.format(featuretyp, gram, minTW,
                                                                                       numAuthors))
        path_vocab = os.path.join(path_vocab_base,
                                  '{}_grams_{}_{}_{}_authors.vocab'.format(featuretyp, gram, minTW, numAuthors))
        if hash:
            if not vectorizers:
                vectorizer = HashingVectorizer(analyzer=ml_utils.identity_analyzer, norm=None, alternate_sign=False)
                vectorizer_dic[str(gram)]['vect'] = vectorizer
                colinds = vectorizers[str(gram)]['colinds']
            else:
                vectorizer_dic[str(gram)]['vect'] = vectorizers[str(gram)]['vect']
                vectorizer = vectorizers[str(gram)]['vect']

        if true_train:
            if not hash:
                vocab = {}

        gram_files['smpl_load'] = os.path.exists(os.path.join(*path_parts, featuretyp, str(minTW), id0, subset,
                                                              '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                           featuretyp,
                                                                                                           gram, minTW,
                                                                                                           numAuthors)))
        print(os.path.join(*path_parts, featuretyp, str(minTW), id0, subset,
                           '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                        featuretyp, gram, minTW, numAuthors)))
        if not gram_files['smpl_load']:
            # TODO
            sys.exit('Files not there...but they should already be there...')

        else:
            # we already have a subset - simply load it
            t = os.path.join(*path_parts, featuretyp, str(minTW), id0, subset,
                             '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, numAuthors))

            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id0, subset, 'tfidf'), exist_ok=True)
            matrix_name = os.path.join(*path_parts, featuretyp, str(minTW), id0, subset, 'tfidf',
                                       'sparse_{}_{}_grams_{}_{}_{}_authors'.format(subset, featuretyp, gram, minTW,
                                                                                    numAuthors))

            matrix_base = os.path.join(*path_parts, featuretyp, str(minTW), id0, subset, 'tfidf',
                                       'sparse_{}_{}_grams_{}_{}_{}_authors_base'.format(subset, featuretyp, gram, minTW,
                                                                                    numAuthors))

            matrix_ids = os.path.join(*path_parts, featuretyp, str(minTW), id0, subset, 'tfidf',
                                      'sparse_uID_{}_{}_grams_{}_{}_{}_authors.p'.format(subset, featuretyp, gram,
                                                                                         minTW, numAuthors))

            sparse_exists = False
            if load_sparse:
                sparse_exists = os.path.exists(matrix_name + '.npz') and os.path.exists(matrix_ids)

        gram_files[str(gram)] = open(t, 'r', encoding='utf-8')

        # first we check whether the id for current line is present in ids
        # if yes, we hash - we do this for every gram type
        # set dummy in line
        lines = [1]
        # only run the loop if we do a hash or vocab does not exists

        doc_counter = 0

        if (true_train or hash) and not sparse_exists:
            id_tester = set()
            while True:
                # read the first 1000 lines
                lines = ml_utils.batch_read(gram_files[str(gram)], batchsize=1000, to_json=True)
                if not lines:
                    break
                lines = pd.DataFrame.from_dict(lines, orient='columns')
                if lines.shape[0] == 0:
                    # no ids in current lines
                    continue
                lines, id_tester = ml_utils.duplicate_uid_tester(lines, id_tester)
                if lines.shape[0] == 0:
                    # no ids in current lines
                    continue
                if id_subset:
                    lines = ml_utils.select_uids(lines, id_subset)
                    if lines.shape[0] == 0:
                        # no ids in current lines
                        continue
                if hash:
                    # save the ids in order
                    set_ids[featuretyp + str(gram)].extend(lines['uID'].to_list())
                doc_counter += lines.shape[0]
                if hash:
                    if str(gram) + '_sparse' in gram_files.keys():
                        # if we do have a sparse matrix in dict and we have to do hash
                        gram_files[str(gram) + '_sparse'] = scipy.sparse.vstack((gram_files[str(gram) + '_sparse'],
                                                                                 vectorizer.transform(
                                                                                     lines['{}_grams_{}'.format(
                                                                                         featuretyp, gram)]))
                                                                                )
                    else:
                        ##use hash vectorizer to keep in memory low
                        gram_files[str(gram) + '_sparse'] = vectorizer.transform(
                            lines['{}_grams_{}'.format(featuretyp, gram)])

                else:
                    vocab = ml_utils.update_vocab(vocab, lines, key='{}_grams_{}'.format(featuretyp, gram))

            if not hash:
                # save vocab to file
                ml_utils.make_vocab(vocab, path_vocab)
                vocab_base, _ = ml_utils.reduce_vocab(vocab, 0)
                vectorizer_base = CountVectorizer(vocabulary=vocab_base, analyzer=ml_utils.identity_analyzer)
                cutoff = 0.01
                if doc_counter >25000:
                    cutoff = 0.005
                one_percent = round(doc_counter * cutoff)
                print('Number of documents {}'.format(doc_counter))
                sys.stdout.flush()
                vocab, max_key = ml_utils.reduce_vocab(vocab, one_percent)
                # vocab, max_key = ml_utils.vocab_maker(vocab)
                # save reduced vocab to file
                ml_utils.make_vocab(vocab, path_vocab_red)

                # here we make our sparse matrix because only now do we have the full vocab
                vectorizer = CountVectorizer(vocabulary=vocab, analyzer=ml_utils.identity_analyzer)

                vectorizer_dic[str(gram)]['vect'] = vectorizer
                vectorizer_dic[str(gram)]['vocab'] = vocab

                # make sure max_key is set
                # if max_key == 0:
                #    for key in vocab.keys():
                #        if key > max_key:
                #            max_key = key



            elif hash and true_train:
                # update our csr matrix and set all words to zero with a doc-frequency below 1%
                numdocs = gram_files[str(gram) + '_sparse'].get_shape()[0]
                colsums = np.array(gram_files[str(gram) + '_sparse'].sum(axis=0)).flatten()
                prop = colsums / numdocs
                # colinds make it not stateless - we have to save this
                cutoff = 0.01
                if numdocs >25000:
                    cutoff = 0.005
                colinds = (prop > cutoff).nonzero()
                with open(path_colids, 'wb') as p:
                    pickle.dump(colinds, p)
            elif hash and not vectorizers:
                with open(path_colids, 'rb') as p:
                    colinds = pickle.load(p)
            if hash:
                gram_files[str(gram) + '_sparse_base'] = gram_files[str(gram) + '_sparse'].copy()
                gram_files[str(gram) + '_sparse'] = ml_utils.zero_columns(gram_files[str(gram) + '_sparse'], colinds)

            # important all files with their tweetids are in the same order
            # we iterate over the length of grams in ascending order...conseqently in each run we can make a tfidf-transformer/vectorizer and save it to disk
            # i.e. first for chargrams1, then for chargrams 1-2, then for chargrams1-2-3 and so forth
            # these here are the parameters taken from Custodio et al. 2021



        elif not sparse_exists:

            if vectorizers:
                vocab = vectorizer_dic[str(gram)]['vocab']
            else:
                vocab = ml_utils.get_vocab(path_vocab_red)
                # vocab = ml_utils.get_vocab(path, path_vocab)
            vocab_base = ml_utils.get_vocab(path_vocab)
            vocab_base, _  = ml_utils.reduce_vocab(vocab_base, 0)
            print('we only loaded the vocabs...')
            sys.stdout.flush()
            if not vectorizers:
                vectorizer = CountVectorizer(vocabulary=vocab, analyzer=ml_utils.identity_analyzer)
                vectorizer_dic[str(gram)] = {}
                vectorizer_dic[str(gram)]['vect'] = vectorizer
                vectorizer_dic[str(gram)]['vocab'] = vocab

            else:
                vectorizer_dic[gram] = vectorizers[str(gram)]['vect']
                vectorizer = vectorizers[str(gram)]['vect']
            vectorizer_base = CountVectorizer(vocabulary=vocab_base, analyzer=ml_utils.identity_analyzer)
        # do this for train and test
        set_ids[featuretyp + str(gram)+'_base'] = []
        set_ids[featuretyp + str(gram)] = []
        if not hash and not sparse_exists:
            if gram_files['smpl_load']:
                filelines = gram_files[str(gram)]
                filelines.seek(0)
            else:
                sys.exit('Error - wrong file to make tfidf from...')
            lines = [1]
            #id_tester= set()
            set_ids[featuretyp + str(gram)] = []
            while lines:
                lines = ml_utils.batch_read(filelines, batchsize=1000, to_json=True)
                if not lines:
                    break
                lines = pd.DataFrame.from_dict(lines, orient='columns')
                #lines, id_tester = ml_utils.duplicate_uid_tester(lines, id_tester)
                #if lines.shape[0] == 0:
                #    # no ids in current lines
                #    continue
                if id_subset:
                    lines = ml_utils.select_uids(lines, id_subset)
                    if lines.shape[0] == 0:
                        # no ids in current lines
                        continue
                # save the ids in order
                set_ids[featuretyp + str(gram)].extend(lines['uID'].to_list())
                set_ids[featuretyp + str(gram) + '_base'].extend(lines['uID'].to_list())
                # csr = ml_utils.construct_tdm(lines['{}_grams_{}'.format(featuretyp, gram)].to_list(), vocab, maxcol=max_key)
                csr = vectorizer.transform(lines['{}_grams_{}'.format(featuretyp, gram)].to_list())
                csr_base = vectorizer_base.transform(lines['{}_grams_{}'.format(featuretyp, gram)].to_list())
                # for some reason we sometimes have a coo matrix instead of a csr matrix - reform to csr if coo
                if type(csr) == scipy.sparse.coo.coo_matrix:
                    print('reform vctorizer output from coo to csr')
                    sys.stdout.flush()
                    csr = csr.tocsr()
                if type(csr_base) == scipy.sparse.coo.coo_matrix:
                    print('reform vectorizer base output from coo to csr')
                    sys.stdout.flush()
                    csr_base = csr_base.tocsr()

                if str(gram) + '_sparse' in gram_files.keys():

                    gram_files[str(gram) + '_sparse'] = scipy.sparse.vstack((gram_files[str(gram) + '_sparse'], csr))
                    gram_files[str(gram) + '_sparse_base'] = scipy.sparse.vstack((gram_files[str(gram) + '_sparse_base'], csr_base))
                else:
                    gram_files[str(gram) + '_sparse'] = csr
                    gram_files[str(gram) + '_sparse_base'] =csr_base
                # for some reason we sometimes have a coo matrix instead of a csr matrix - reform to csr if coo
                if type(gram_files[str(gram) + '_sparse']) == scipy.sparse.coo.coo_matrix:
                    print('reform from coo to csr')
                    sys.stdout.flush()
                    gram_files[str(gram) + '_sparse'] = gram_files[str(gram) + '_sparse'].tocsr()
                if type(gram_files[str(gram) + '_sparse_base']) == scipy.sparse.coo.coo_matrix:
                    print('reform base from coo to csr')
                    sys.stdout.flush()
                    gram_files[str(gram) + '_sparse_base'] = gram_files[str(gram) + '_sparse_base'].tocsr()
                #while loop ends here
                lines = [1]
            #remove doubled entries - if there are any
            set_ids[featuretyp + str(gram)], sel_ids = ml_utils.duplicate_fast_uid_tester(set_ids[featuretyp + str(gram)])
            #subset sparse array via indices
            gram_files[str(gram) + '_sparse'] = gram_files[str(gram) + '_sparse'][sel_ids,:]
            # remove doubled entries - if there are any
            set_ids[featuretyp + str(gram)+'_base'], sel_ids = ml_utils.duplicate_fast_uid_tester(set_ids[featuretyp + str(gram)+'_base'])
            #subset sparse array via indices
            gram_files[str(gram) + '_sparse'+'_base'] = gram_files[str(gram) + '_sparse'+'_base'][sel_ids, :]

            print('Have {} (documents,features) in in matrix...'.format(gram_files[str(gram) + '_sparse'].shape))
            sys.stdout.flush()

        if not sparse_exists:
            ml_utils.save_sparse(gram_files[str(gram) + '_sparse'], path=matrix_name)
            ml_utils.save_uids(set_ids[featuretyp + str(gram)], path=matrix_ids)

            ml_utils.save_sparse(gram_files[str(gram) + '_sparse_base'], path=matrix_base)
        else:
            gram_files[str(gram) + '_sparse'] = ml_utils.load_sparse(matrix_name)
            set_ids[featuretyp + str(gram)] = ml_utils.load_uids(matrix_ids)

            gram_files[str(gram) + '_sparse_base'] = ml_utils.load_sparse(matrix_base)
        # add csr column-wise together

        if type(gram_files[str(gram) + '_sparse']) == scipy.sparse.coo.coo_matrix:
            print('reform from coo to csr')
            sys.stdout.flush()
            gram_files[str(gram) + '_sparse'] = gram_files[str(gram) + '_sparse'].tocsr()
        if type(gram_files[str(gram) + '_sparse_base']) == scipy.sparse.coo.coo_matrix:
            print('reform base from coo to csr')
            sys.stdout.flush()
            gram_files[str(gram) + '_sparse_base'] = gram_files[str(gram) + '_sparse_base'].tocsr()

        if csr_saver:
            # make sure matrices have same dimensions
            fin_csr, \
            set_ids[featuretyp + str(gram - 1)], \
            gram_files[str(gram) + '_sparse'], set_ids[featuretyp + str(gram)] = ml_utils.assess_equal(fin_csr,
                                                                                                       set_ids[featuretyp + str(gram - 1)],
                                                                                                       gram_files[str(gram) + '_sparse'],
                                                                                                       set_ids[featuretyp + str(gram)])

            fin_csr_base, \
            set_ids[featuretyp + str(gram - 1) + '_base'], \
            gram_files[str(gram) + '_sparse_base'], set_ids[featuretyp + str(gram)+'_base'] = ml_utils.assess_equal(fin_csr_base,
                                                                                                       set_ids[featuretyp + str(gram - 1)+'_base'],
                                                                                                       gram_files[str(gram) + '_sparse_base'],
                                                                                                       set_ids[featuretyp + str(gram)+'_base'])
            try:
                assert fin_csr.shape[0] == gram_files[str(gram) + '_sparse'].shape[0]
            except AssertionError as e:
                e.args += ('feature: {}, minTw: {}, grams:{}, numAuthors:{}'.format(featuretyp, minTW, gram ,numAuthors))
                raise
            try:
                assert fin_csr_base.shape[0] == gram_files[str(gram) + '_sparse_base'].shape[0]
            except AssertionError as e:
                e.args += ('feature: {}, minTw: {}, grams:{}, numAuthors:{}'.format(featuretyp, minTW, gram ,numAuthors))
                raise

            fin_csr = scipy.sparse.hstack((fin_csr, gram_files[str(gram) + '_sparse']))
            fin_csr_base = scipy.sparse.hstack((fin_csr_base, gram_files[str(gram) + '_sparse_base']))
        else:
            csr_saver = True
            fin_csr = gram_files[str(gram) + '_sparse'].copy()
            fin_csr_base = gram_files[str(gram) + '_sparse_base'].copy()

        # close files
        gram_files[str(gram)].close()
        del gram_files[str(gram) + '_sparse']
        del gram_files[str(gram) + '_sparse_base']
        gc.collect()

        # now we make our tfidf transformation
        gramsstring = '_'.join([str(el) for el in minTw_gram_auth[1] if el <=gram])
        if true_train:
            trans = TfidfTransformer(norm='l2', use_idf=False, smooth_idf=True, sublinear_tf=True)
            trans = trans.fit(fin_csr)

            os.makedirs(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0), exist_ok=True)
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   'tf_{}_grams_{}_{}_{}_hash_{}.p'.format(featuretyp, gramsstring, minTW, numAuthors,
                                                                              hash)), 'wb') as p:
                pickle.dump(trans, p)

            trans_tmp = TfidfTransformer(norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=True)
            trans_tmp = trans_tmp.fit(fin_csr)
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   'tfidf_{}_grams_{}_{}_{}_hash_{}.p'.format(featuretyp, gramsstring, minTW, numAuthors,
                                                                              hash)), 'wb') as p:
                pickle.dump(trans_tmp, p)

            trans_tmp = TfidfTransformer(norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=True)
            trans_tmp = trans_tmp.fit(fin_csr_base)

            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   'tfidf_{}_grams_{}_{}_{}_hash_{}_base.p'.format(featuretyp, gramsstring, minTW, numAuthors,
                                                                              hash)), 'wb') as p:
                pickle.dump(trans_tmp, p)
        else:
            #print(gramsstring)
            #sys.stdout.flush()
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   'tfidf_{}_grams_{}_{}_{}_hash_{}.p'.format(featuretyp, gramsstring, minTW, numAuthors,
                                                                              hash)), 'rb') as p:
                trans = pickle.load(p)

            #print('the tfidf non base has the idf-shape of {}'.format(trans.idf_.shape))
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   'tfidf_{}_grams_{}_{}_{}_hash_{}_base.p'.format(featuretyp, gramsstring, minTW,
                                                                              numAuthors,
                                                                              hash)), 'rb') as p:
                trans = pickle.load(p)

            #print('the tfidf-base has the idf-shape of {}'.format(trans.idf_.shape))

            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   'tf_{}_grams_{}_{}_{}_hash_{}.p'.format(featuretyp, gramsstring, minTW, numAuthors,
                                                                              hash)), 'rb') as p:
                trans = pickle.load(p)

    if trans_in:
        trans = trans_in

    tfidf = trans.transform(fin_csr)

    return trans, vectorizer_dic, tfidf, set_ids


def make_scaler(featuretyp:str, minTw_gram_auth:list, path:str, id_type:str, subset ='train', id_subset=[], scaler =None, load_scaled=True):
    set_ids = {}
    set_ids[featuretyp+''] = []

    if featuretyp == 'num':
        keys = ["init_len", "prepr_len", "mentions", "tags", "urls", "times",
                "emotic_num", 'emojis_num', 'numericals']
    else:
        keys = ['polarity', 'subjectivity']


    if subset == 'crossval':
        subset = 'train'
        true_train = False
    elif subset == 'train':
        true_train = True
    else:
        true_train = False
    path_parts = ml_utils.split_path_unix_win(path)
    id0 = id_type.split('_')[0]
    model_dir = ml_utils.make_save_dirs(path)
    subset_path = os.path.join(*path_parts, featuretyp, str(minTw_gram_auth[0]), id0, subset)
    os.makedirs(os.path.join(subset_path, 'scaled'), exist_ok=True)
    #read in file
    scaled_path = os.path.join(subset_path, 'scaled', "scaled_{}_{}_{}_{}_authors".format(subset,featuretyp,
                                                                                                 minTw_gram_auth[0],
                                                                                                 minTw_gram_auth[2]))
    ids_path = os.path.join(subset_path, 'scaled',"scaled_uID_{}_{}_{}_{}_authors.p".format(subset,
                                                                                                    featuretyp,
                                                                                                    minTw_gram_auth[0],
                                                                                                    minTw_gram_auth[2]))
    scaled_exists = os.path.exists(scaled_path+'.npy') & os.path.exists(ids_path)
    if not load_scaled or not scaled_exists:
        with open(os.path.join(subset_path, "{}_{}_{}_{}_authors.ndjson".format(subset,featuretyp,minTw_gram_auth[0],minTw_gram_auth[2])),
                  'r', encoding='utf-8') as f:
            dat = ndjson.load(f)

        dat = pd.DataFrame.from_dict(dat, orient='columns')
        dat.drop_duplicates(subset=['uID'], keep='first', inplace=True)
        dat.reset_index(drop=True, inplace=True)
        print('Shape after dropping doubled uIDs: {}'.format(dat.shape))
        for key in keys:
            if dat[key].dtype != 'float':
                print('scale {}'.format(key))
                sys.stdout.flush()
                dat[key] = dat[key].apply(lambda x: statistics.mean(x))

        if id_subset:
            dat = ml_utils.select_uids(dat, id_subset)

        if true_train:
            scaler = StandardScaler(copy=False)
            # save the ids in order
            set_ids[featuretyp+''].extend(dat['uID'].to_list())
            print('fit scaler')
            sys.stdout.flush()
            dat = scaler.fit_transform(dat[keys].to_numpy())
            print('fitted scaler...')
            sys.stdout.flush()

            #save scaler
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                      'scaler_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[0], minTw_gram_auth[2])), 'wb') as p:
                pickle.dump(scaler, p)
        else:
            if not scaler:
                with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                      'scaler_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[0], minTw_gram_auth[2])), 'rb') as p:
                    scaler = pickle.load( p)


            # save the ids in order
            set_ids[featuretyp+''].extend(dat['uID'].to_list())
            dat = scaler.transform(dat[keys].to_numpy())


        if not id_subset:
            np.save(file=scaled_path, arr=dat)
            with open(os.path.join(subset_path, 'scaled',"scaled_uID_{}_{}_{}_{}_authors.p".format(subset,
                                                                                                            featuretyp,
                                                                                                            minTw_gram_auth[0],
                                                                                                            minTw_gram_auth[2])),'wb') as f:
                pickle.dump(set_ids, f)

    else:

        dat = np.load(file=scaled_path+'.npy')
        with open(os.path.join(subset_path, 'scaled',"scaled_uID_{}_{}_{}_{}_authors.p".format(subset,
                                                                                                        featuretyp,
                                                                                                        minTw_gram_auth[0],
                                                                                                        minTw_gram_auth[2])),'rb') as f:
            set_ids = pickle.load(f)

        if not scaler:
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                      'scaler_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[0], minTw_gram_auth[2])), 'rb') as p:
                scaler = pickle.load(p)


    return scaler, dat, set_ids


def make_pca(dat, featuretyp, minTw_gram_auth, path, id_type, subset='train', pca=None, load_pca=True):
    if subset == 'crossval':
        subset = 'train'
        true_train = False
    elif subset == 'train':
        true_train = True
    else:
        true_train = False
    path = ml_utils.split_path_unix_win(path)
    pca_dat_path = os.path.join(path, featuretyp, str(minTw_gram_auth[0]), id_type.split['_'][0], subset, 'pca')
    os.makedirs(pca_dat_path, exist_ok=True)

    model_dir = ml_utils.make_save_dirs(path)
    id0 = id_type.split('_')[0]
    if featuretyp in ['num', 'polarity']:
        filen = 'pca_{}_{}_{}_authors.p'.format(featuretyp, minTw_gram_auth[0], minTw_gram_auth[2])
    else:
        gramsstring = '_'.join([str(el) for el in minTw_gram_auth[1]])
        filen = 'pca_{}_grams_{}_{}_{}_authors.p'.format(featuretyp, gramsstring, minTw_gram_auth[0],
                                                         minTw_gram_auth[2])

    # set existence marker
    pca_dat = filen.split('.')[0] + '.npy'
    load_pca = load_pca & os.path.exists(os.path.join(pca_dat_path, pca_dat))

    if true_train and not load_pca:
        if featuretyp in ['num', 'polarity']:
            pca = PCA(whiten=True, n_components=0.99, random_state=123456)
            dat = pca.fit_transform(dat)
        else:
            start = round(0.8 * dat.shape[1])
            # pca = IncrementalPCA(n_components=start, batch_size=1000)
            pca = TruncatedSVD(n_components=start, random_state=123456, n_iter=3, algorithm='randomized')

            pca = ml_utils.find_variance_cutoff(pca, dat, cutoff=0.99, start=start)
            dat = pca.transform(dat)

        with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                               filen), 'wb') as p:
            pickle.dump(pca, p)

        np.save(os.path.join(pca_dat_path, pca_dat), arr=dat)


    else:
        if not pca:
            with open(os.path.join(model_dir, featuretyp, str(minTw_gram_auth[0]), id0,
                                   filen), 'rb') as p:
                pca = pickle.load(p)
        if not load_pca:
            dat = pca.transform(dat)
        else:
            dat = np.load(os.path.join(pca_dat_path, pca_dat))

    return pca, dat


def process_input(featuretyp: str, target: str, minTw_gram_auth, path: str, subset='train', id_subset=[],
                  components=None, hash=False, recalc=False):
    # find the number of features for pca which explains 99% variance - this takes a while
    id_type = target + '_ids'

    # depending on type of data we have to vectorize the data first
    if not components:
        components = {}
    if featuretyp in ['num', 'polarity']:
        scaler = components.get('vectorizing', None)
        if scaler:
            scaler = scaler[0]
        scaler, dat, ids = make_scaler(featuretyp, minTw_gram_auth, path, id_type, subset=subset, id_subset=id_subset,
                                       scaler=scaler, load_scaled=recalc)
        components.update({'vectorizing': [scaler]})

    elif featuretyp == 'emoticon_c':
        scaler = components.get('vectorizing', None)
        if scaler:
            scaler = scaler[0]
        vectorizer, dat, ids = make_emoji_vector(featuretyp, minTw_gram_auth, path, id_type, subset=subset,
                                                 id_subset=id_subset, vectorizer=scaler)
        components.update({'vectorizing': [vectorizer]})

    else:
        vectorizers = components.get('vectorizing', [None, None])
        vectorizer = vectorizers[0]
        transformer = vectorizers[1]

        tfidf_trans, vectorizer, dat, ids = make_tfidf(featuretyp, minTw_gram_auth, path, id_type, subset=subset,
                                                       id_subset=id_subset, vectorizers=vectorizer, trans_in=transformer,
                                                       hash=hash, load_sparse=False)

        components.update({'vectorizing': [vectorizer, tfidf_trans]})


    if recalc:
        print('making pca...')
        sys.stdout.flush()
        pca = components.get('pca', None)
        if pca:
            pca = pca[0]
        pca, dat = make_pca(featuretyp=featuretyp, minTw_gram_auth=minTw_gram_auth, path=path,
                            id_type=id_type, subset='train', dat=dat, pca=pca, load_pca=False)

        components.update({'pca': [pca]})

    return components, dat, ids


def compute(featuretyp: str, target: str, minTw_gram_auth, path: str, y: pd.DataFrame, id_subset: list = None,
               hash=False, subset='train', recalc=False):
    path = os.path.join(*ml_utils.split_path_unix_win(path))
    model_dir = os.path.join(path, featuretyp, str(minTw_gram_auth[0]), target)
    model_dir = ml_utils.make_save_dirs(model_dir)

    # make correct target
    cats, target_key = ml_utils.y_categories_target_key(target)

    #encoder = OneHotEncoder(drop='if_binary', categories=[cats])
    if id_subset:
        y = y.loc[y.uID.isin(id_subset), :]
    #y_encoded = encoder.fit_transform(y[target_key].to_numpy().reshape((y.shape[0], -1)))

    #with open(os.path.join(model_dir, 'encoder_{}_{}_{}_authors.p'.format(target, minTw_gram_auth[0],
    #                                                                      minTw_gram_auth[2])), 'wb') as w:
    #    pickle.dump(encoder, w)



    components, dat, ids = process_input(featuretyp=featuretyp, target=target,
                                         minTw_gram_auth=minTw_gram_auth,
                                         path=path, id_subset=id_subset, hash=hash, subset=subset, recalc=recalc)


    print('done with this part...')
    sys.stdout.flush()

def process_wrapper(jobpart:list):



    pid = mp.current_process()
    print('{} has started to work'.format(pid))
    sys.stdout.flush()

    id_subset= None
    hash = False
    for i in range(0, len(jobpart)):

        featuretyp =  jobpart[i][0]
        target=jobpart[i][1]
        minTw_gram_auth = jobpart[i][2]
        path=jobpart[i][3]

        import tensorflow as tf
        pp = ml_utils.split_path_unix_win(path)

        for subset in ['train', 'val', 'test']:
        #for subset in ['val']:
            yp = os.path.join(*pp, target, subset,
                                          '{}_ids_{}_{}_{}_authors_balanced.json'.format(subset, target.split('_')[0],
                                                                                         minTw_gram_auth[0],minTw_gram_auth[2]))

            with open(yp, 'r', encoding='utf-8') as r:
                y = pd.read_json(r)

            compute(featuretyp=featuretyp, target=target.split('_')[0], minTw_gram_auth=minTw_gram_auth, path=path, y = y, id_subset = id_subset,
                    hash = False, subset = subset, recalc=False)

    print('{} has finished work...'.format(pid))
    sys.stdout.flush()


if __name__ == '__main__':

    tester = False
    if not tester:
        ncpus = mp.cpu_count()-1
        if ncpus < 80-1:
            print('failed to get 80 cpus - only got {}'.format(ncpus))
        #ncpus = 40-1
    else:
        ncpus = 1

    ftyp_dic = {'emoticon_c': [[100, 250, 500], [1], [1000, 500, 150, 50]],
                      'char': [[100, 250, 500], list(range(2, 5 + 1)), [1000, 500, 150, 50]],
                      'word': [[100, 250, 500], list(range(1, 2 + 1)), [1000, 500, 150, 50]],
                      'tag': [[100, 250, 500], list(range(1, 3 + 1)), [1000, 500, 150, 50]],
                      'dep': [[100, 250, 500], list(range(1, 3 + 1)), [1000, 500, 150, 50]],
                      'asis': [[100, 250, 500], list(range(2, 5 + 1)), [1000, 500, 150, 50]],
                      'lemma': [[100, 250, 500], list(range(1, 2 + 1)), [1000, 500, 150, 50]],
                      'dist': [[100, 250, 500], list(range(2, 5 + 1)), [1000, 500, 150, 50]],
                      'pos': [[100, 250, 500], list(range(1, 3 + 1)), [1000, 500, 150, 50]],
                      'num': [[100, 250, 500], [1], [1000, 500, 150, 50]],
                      'polarity': [[100, 250, 500], [1], [1000, 500, 150, 50]]
                      }
    path = '../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/preprocessed/workset/'
    path = ml_utils.split_path_unix_win(path)
    subtypes = ['creator']
    id_type = ['gender_ids', 'age_ids']
    joblist = []
    if not tester:

        argparser = argparse.ArgumentParser(description='Arguements making the sparse matrices')
        argparser.add_argument_group('required arguments')
        argparser.add_argument('-f', '--features', help='Features which should be processed',
                               required=True, nargs='*')
        args = vars(argparser.parse_args())
        featuretyp_dic={}
        for key in args['features']:
            featuretyp_dic[key] = ftyp_dic[key]

        pool = mp.Pool(ncpus)
        jobs = []
        for sub in subtypes:
            for key in featuretyp_dic.keys():
                for tweetLen in featuretyp_dic[key][0]:
                    for authors in featuretyp_dic[key][2]:
                        for id in id_type:

                            c_path = os.path.join(*path, 'creator')
                            minTw_gram_auth = [tweetLen, featuretyp_dic[key][1], authors]
                            joblist.append((key, id, minTw_gram_auth, c_path))

        random.shuffle(joblist)
        joblist = ml_utils.chunkify(joblist, ncpus)
        runs=0
        for i in range(0,len(joblist)):
            job = pool.apply_async(process_wrapper, (joblist[i],))
            jobs.append(job)
            gc.collect()
            runs += 1
        print("made {} tasks to pool".format(runs))
        # collect results from the workers through the pool result queue
        print('collect results from job cycle...')
        sys.stdout.flush()
        for i in range(0, len(jobs)):
            tmp = jobs.pop(0)
            tmp.get()
            del tmp
        print('sleep after cycle...')
        sys.stdout.flush()
        print('closing down the pool and exit :)')
        sys.stdout.flush()
        pool.close()
        pool.join()

    else:
        c_path = os.path.join(*path, 'creator')
        key ='char'
        minTw_gram_auth = [100, [2,3,4,5], 500]
        id = 'gender_ids'
        process_wrapper([[key, id, minTw_gram_auth, c_path]])
    print('done')
    sys.stdout.flush()