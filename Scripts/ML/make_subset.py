import os
import numpy as np
import scipy
import pandas as pd
import sys
sys.path.append('..')
from ML import ml_utils
import multiprocessing as mp



def make_subsets(featuretyp:str, minTw_gram_auth:list, path:str, id_type:list, subset ='train', rerun =False):

    minTW = minTw_gram_auth[0]
    numAuthors = minTw_gram_auth[2]
    gram_files = {}
    vocab_exists = False

    id0 = id_type[0].split('_')[0]
    id1 = id_type[1].split('_')[0]

    path_parts = ml_utils.split_path_unix_win(path)

    id_file0 = "{}_ids_{}_{}_{}_authors_balanced.json".format(subset, id_type[0].split('_')[0], minTw_gram_auth[0], minTw_gram_auth[2])
    id_file0_val = "{}_ids_{}_{}_{}_authors_balanced.json".format('val', id_type[0].split('_')[0], minTw_gram_auth[0],
                                                             minTw_gram_auth[2])
    id_file0_test = "{}_ids_{}_{}_{}_authors_balanced.json".format('test', id_type[0].split('_')[0], minTw_gram_auth[0],
                                                             minTw_gram_auth[2])
    id_file1 = "{}_ids_{}_{}_{}_authors_balanced.json".format(subset, id_type[1].split('_')[0], minTw_gram_auth[0], minTw_gram_auth[2])
    id_file1_val = "{}_ids_{}_{}_{}_authors_balanced.json".format('val', id_type[1].split('_')[0], minTw_gram_auth[0],
                                                             minTw_gram_auth[2])
    id_file1_test = "{}_ids_{}_{}_{}_authors_balanced.json".format('test', id_type[1].split('_')[0], minTw_gram_auth[0],
                                                             minTw_gram_auth[2])


    with open(os.path.join(*path_parts, id_type[0], subset, id_file0), 'r', encoding='utf-8') as id:
        ids0 = set(pd.read_json(id)['uID'].to_list())
    #so that we save some passes (very time expensive)
    with open(os.path.join(*path_parts, id_type[0], 'val', id_file0_val), 'r', encoding='utf-8') as id:
        ids_val0 = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type[0], 'test', id_file0_test), 'r', encoding='utf-8') as id:
        ids_test0 = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type[1], subset, id_file1), 'r', encoding='utf-8') as id:
        ids1 = set(pd.read_json(id)['uID'].to_list())
    #so that we save some passes (very time expensive)
    with open(os.path.join(*path_parts, id_type[1], 'val', id_file1_val), 'r', encoding='utf-8') as id:
        ids_val1 = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type[1], 'test', id_file1_test), 'r', encoding='utf-8') as id:
        ids_test1 = set(pd.read_json(id)['uID'].to_list())


    for gram in sorted(minTw_gram_auth[1]):
        ##acess files i.e. char_grams_3_500, char_grams_4_500, char_grams_5_500
        gram_files['smpl_load_0'] = os.path.exists(os.path.join(*path_parts,featuretyp, str(minTW) , id0 ,subset,
                                                                '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                             featuretyp, gram, minTW, numAuthors)))

        gram_files['smpl_load_1'] = os.path.exists(os.path.join(*path_parts, featuretyp, str(minTW), id1, subset,
                                                              '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                           featuretyp,
                                                                                                           gram, minTW,
                                                                                                           numAuthors)))
        print(os.path.join(*path_parts,featuretyp, str(minTW) ,subset, id0,
                                                                '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                             featuretyp, gram, minTW, numAuthors)))
        print(gram_files['smpl_load_1'])
        if rerun:
            os.makedirs(os.path.join(*path_parts,featuretyp, str(minTW) ,id0,subset), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW),id0, 'test'), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW),id0, 'val'), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id1, subset), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id1, 'test'), exist_ok=True)
            os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id1, 'val'), exist_ok=True)

            t = os.path.join(*path_parts, featuretyp, str(minTW),
                                       '{}_grams_{}_{}.ndjson'.format(featuretyp, gram, minTW))


            gram_files[str(gram)+'save0'] = open(os.path.join(*path_parts,
                                                   featuretyp, str(minTW) ,id0,subset,
                                                          '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, numAuthors)),
                                           'w',encoding='utf-8')
            gram_files[str(gram) + 'save_test0'] = open(os.path.join(*path_parts,
                                                               featuretyp, str(minTW),id0, 'test',
                                                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format('test',
                                                                                                            featuretyp,
                                                                                                            gram, minTW,
                                                                                                            numAuthors)),
                                                  'w', encoding='utf-8')
            gram_files[str(gram) + 'save_val0'] = open(os.path.join(*path_parts,
                                                               featuretyp, str(minTW),id0, 'val',
                                                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format('val',
                                                                                                            featuretyp,
                                                                                                            gram, minTW,
                                                                                                            numAuthors)),
                                                  'w', encoding='utf-8')

            gram_files[str(gram)+'save1'] = open(os.path.join(*path_parts,
                                                   featuretyp, str(minTW) ,id1,subset,
                                                          '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, numAuthors)),
                                           'w',encoding='utf-8')
            gram_files[str(gram) + 'save_test1'] = open(os.path.join(*path_parts,
                                                               featuretyp, str(minTW),id1, 'test',
                                                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format('test',
                                                                                                            featuretyp,
                                                                                                            gram, minTW,
                                                                                                            numAuthors)),
                                                  'w', encoding='utf-8')
            gram_files[str(gram) + 'save_val1'] = open(os.path.join(*path_parts,
                                                               featuretyp, str(minTW),id1, 'val',
                                                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format('val',
                                                                                                            featuretyp,
                                                                                                            gram, minTW,
                                                                                                            numAuthors)),
                                                  'w', encoding='utf-8')

        else:
            #we already have a subset - simply load it
            t = os.path.join(*path_parts,featuretyp, str(minTW) ,id0, subset,
                               '{}_{}_grams_{}_{}_{}_authors.ndjson'.format(subset, featuretyp, gram, minTW, numAuthors))
        print(t)
        gram_files[str(gram)] = open(t, 'r', encoding='utf-8')

        #first we check whether the id for current line is present in ids
        #if yes, we hash - we do this for every gram type
        #set dummy in line
        lines = [1]
        doc_counter = 0
        #only run the loop if we do a hash or vocab does not exists

        while True:
            if doc_counter %1000 == 0:
                print("wrote {} to file... :)".format(doc_counter))
            #read the first 1000 lines
            lines = ml_utils.batch_read(gram_files[str(gram)], batchsize=1000, to_json=True)
            if not lines:
                #end if we already went trough the test file also
                print('read complete file {}...exiting'.format(gram_files[str(gram)].name))
                sys.stdout.flush()
                break
            lines = pd.DataFrame.from_dict(lines, orient='columns')
            # if we do not have subseted the files yet
            val_lines0 = ml_utils.select_uids(lines, ids_val0)
            test_lines0 = ml_utils.select_uids(lines, ids_test0)
            lines0 = ml_utils.select_uids(lines, ids0)
            val_lines1 = ml_utils.select_uids(lines, ids_val1)
            test_lines1 = ml_utils.select_uids(lines, ids_test1)
            lines1 = ml_utils.select_uids(lines, ids1)
            for ss in [['0', lines0], ['_val0', val_lines0],['_test0', test_lines0],
                        ['1', lines1], ['_val1', val_lines1],['_test1', test_lines1]]:
                 #update doc_counter
                 if ss[1].shape[0] !=0:
                    doc_counter += ss[1].shape[0]
                    #write subset to file
                    ml_utils.pandas_to_ndjson(ss[1], gram_files[str(gram)+'save'+ss[0]])
                #otherwise the lines are part of the subset and we do not have to select and save to file

        gram_files[str(gram) + 'save0'].close()
        gram_files[str(gram) + 'save_val0'].close()
        gram_files[str(gram) + 'save_test0'].close()
        gram_files[str(gram) + 'save1'].close()
        gram_files[str(gram) + 'save_val1'].close()
        gram_files[str(gram) + 'save_test1'].close()
        gram_files[str(gram)].close()


    #now we make our tfidf transformation

    print('done....wrote everything to subfiles now...')
    sys.stdout.flush()

def make_numeric_subsets(featuretyp:str, minTw_gram_auth:list, path:str, id_type:list, subset ='train', rerun =False):
    minTW = minTw_gram_auth[0]
    numAuthors = minTw_gram_auth[2]

    gram_files = {}

    id0 = id_type[0].split('_')[0]
    id1 = id_type[1].split('_')[0]

    path_parts = ml_utils.split_path_unix_win(path)

    id_file0 = "{}_ids_{}_{}_{}_authors_balanced.json".format(subset, id_type[0].split('_')[0], minTw_gram_auth[0],
                                                              minTw_gram_auth[2])
    id_file0_val = "{}_ids_{}_{}_{}_authors_balanced.json".format('val', id_type[0].split('_')[0], minTw_gram_auth[0],
                                                                  minTw_gram_auth[2])
    id_file0_test = "{}_ids_{}_{}_{}_authors_balanced.json".format('test', id_type[0].split('_')[0], minTw_gram_auth[0],
                                                                   minTw_gram_auth[2])
    id_file1 = "{}_ids_{}_{}_{}_authors_balanced.json".format(subset, id_type[1].split('_')[0], minTw_gram_auth[0],
                                                              minTw_gram_auth[2])
    id_file1_val = "{}_ids_{}_{}_{}_authors_balanced.json".format('val', id_type[1].split('_')[0], minTw_gram_auth[0],
                                                                  minTw_gram_auth[2])
    id_file1_test = "{}_ids_{}_{}_{}_authors_balanced.json".format('test', id_type[1].split('_')[0], minTw_gram_auth[0],
                                                                   minTw_gram_auth[2])

    with open(os.path.join(*path_parts, id_type[0], subset, id_file0), 'r', encoding='utf-8') as id:
        ids0 = set(pd.read_json(id)['uID'].to_list())
    # so that we save some passes (very time expensive)
    with open(os.path.join(*path_parts, id_type[0], 'val', id_file0_val), 'r', encoding='utf-8') as id:
        ids_val0 = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type[0], 'test', id_file0_test), 'r', encoding='utf-8') as id:
        ids_test0 = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type[1], subset, id_file1), 'r', encoding='utf-8') as id:
        ids1 = set(pd.read_json(id)['uID'].to_list())
    # so that we save some passes (very time expensive)
    with open(os.path.join(*path_parts, id_type[1], 'val', id_file1_val), 'r', encoding='utf-8') as id:
        ids_val1 = set(pd.read_json(id)['uID'].to_list())
    with open(os.path.join(*path_parts, id_type[1], 'test', id_file1_test), 'r', encoding='utf-8') as id:
        ids_test1 = set(pd.read_json(id)['uID'].to_list())


    if rerun:
        t = os.path.join(*path_parts, featuretyp, str(minTW),
                         '{}_{}.ndjson'.format(featuretyp, minTW))
        print(t)
        file_read = open(t, 'r', encoding='utf-8')
        os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id0, subset), exist_ok=True)
        os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id0, 'test'), exist_ok=True)
        os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id0, 'val'), exist_ok=True)
        os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id1, subset), exist_ok=True)
        os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id1, 'test'), exist_ok=True)
        os.makedirs(os.path.join(*path_parts, featuretyp, str(minTW), id1, 'val'), exist_ok=True)

        t = os.path.join(*path_parts, featuretyp, str(minTW),
                         '{}_{}.ndjson'.format(featuretyp, minTW))

        gram_files['save0'] = open(os.path.join(*path_parts,
                                                            featuretyp, str(minTW), id0, subset,
                                                            '{}_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                         featuretyp,
                                                                                                        minTW,
                                                                                                         numAuthors)),
                                               'w', encoding='utf-8')
        gram_files['save_test0'] = open(os.path.join(*path_parts,
                                                                 featuretyp, str(minTW), id0, 'test',
                                                                 '{}_{}_{}_{}_authors.ndjson'.format('test',
                                                                                                              featuretyp,
                                                                                                              minTW,
                                                                                                              numAuthors)),
                                                    'w', encoding='utf-8')
        gram_files['save_val0'] = open(os.path.join(*path_parts,
                                                                featuretyp, str(minTW), id0, 'val',
                                                                '{}_{}_{}_{}_authors.ndjson'.format('val',
                                                                                                             featuretyp,
                                                                                                             minTW,
                                                                                                             numAuthors)),
                                                   'w', encoding='utf-8')

        gram_files['save1'] = open(os.path.join(*path_parts,
                                                            featuretyp, str(minTW), id1, subset,
                                                            '{}_{}_{}_{}_authors.ndjson'.format(subset,
                                                                                                         featuretyp,minTW,
                                                                                                         numAuthors)),
                                               'w', encoding='utf-8')
        gram_files['save_test1'] = open(os.path.join(*path_parts,
                                                                 featuretyp, str(minTW), id1, 'test',
                                                                 '{}_{}_{}_{}_authors.ndjson'.format('test',
                                                                                                              featuretyp,
                                                                                                              minTW,
                                                                                                              numAuthors)),
                                                    'w', encoding='utf-8')
        gram_files['save_val1'] = open(os.path.join(*path_parts,
                                                                featuretyp, str(minTW), id1, 'val',
                                                                '{}_{}_{}_{}_authors.ndjson'.format('val',
                                                                                                             featuretyp,
                                                                                                             minTW,
                                                                                                             numAuthors)),
                                                   'w', encoding='utf-8')

        doc_counter = 0
        # only run the loop if we do a hash or vocab does not exists

        while True:
            if doc_counter % 1000 == 0:
                print("wrote {} to file... :)".format(doc_counter))
            # read the first 1000 lines
            lines = ml_utils.batch_read(file_read, batchsize=1000, to_json=True)
            if not lines:
                # end if we already went trough the test file also
                print('read complete file {}...exiting'.format(file_read.name))
                sys.stdout.flush()
                break
            lines = pd.DataFrame.from_dict(lines, orient='columns')
            # if we do not have subseted the files yet
            val_lines0 = ml_utils.select_uids(lines, ids_val0)
            test_lines0 = ml_utils.select_uids(lines, ids_test0)
            lines0 = ml_utils.select_uids(lines, ids0)
            val_lines1 = ml_utils.select_uids(lines, ids_val1)
            test_lines1 = ml_utils.select_uids(lines, ids_test1)
            lines1 = ml_utils.select_uids(lines, ids1)
            for ss in [['0', lines0], ['_val0', val_lines0], ['_test0', test_lines0],
                       ['1', lines1], ['_val1', val_lines1], ['_test1', test_lines1]]:
                # update doc_counter
                if ss[1].shape[0] != 0:
                    doc_counter += ss[1].shape[0]
                    # write subset to file
                    ml_utils.pandas_to_ndjson(ss[1], gram_files['save' + ss[0]])
            # otherwise the lines are part of the subset and we do not have to select and save to file

        gram_files['save0'].close()
        gram_files['save_val0'].close()
        gram_files['save_test0'].close()
        gram_files['save1'].close()
        gram_files['save_val1'].close()
        gram_files['save_test1'].close()
        file_read.close()

    # now we make our tfidf transformation

    print('done....wrote everything to subfiles now...')
    sys.stdout.flush()


if __name__ == '__main__':

    tester = False
    if not tester:
        ncpus = mp.cpu_count()-1
        if ncpus < 80-1:
            print('failed to get 80 cpus - only got {}'.format(ncpus))
        #ncpus = 40-1
    else:
        ncpus = 6

    featuretyp_dic = {'char':  [[100,250, 500], list(range(2, 5+1)), [1000, 500, 150, 50]],
                      'word':[[100,250, 500], list(range(1, 2+1)), [1000, 500, 150, 50]],
                      'tag':[[100,250, 500],list(range(1, 3+1)), [1000, 500, 150, 50]],
                      'dep':[[100,250, 500], list(range(1, 3+1)), [1000, 500, 150, 50]],
                      'asis':[[100,250, 500], list(range(2, 5+1)), [1000, 500, 150, 50]],
                      'lemma':[[100,250, 500], list(range(1, 2+1)), [1000, 500, 150, 50]],
                      'dist':[[100,250, 500], list(range(2, 5+1)), [1000, 500, 150, 50]],
                      'pos': [[100,250, 500], list(range(1, 3+1)), [1000, 500, 150, 50]],
                      'num': [[100, 250, 500], [1], [1000, 500, 150, 50]],
                      'polarity': [[100, 250, 500], [1], [1000, 500, 150, 50]],
                      'emoticon_c': [[100, 250, 500], [1], [1000, 500, 150, 50]]
    }

    path = '../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/preprocessed/workset/'
    path = ml_utils.split_path_unix_win(path)
    subtypes = ['creator', 'performer']
    id_type = ['gender_ids', 'age_ids']

    pool = mp.Pool(ncpus)
    jobs = []
    for sub in subtypes:
        for key in featuretyp_dic.keys():
            for tweetLen in featuretyp_dic[key][0]:
                for authors in featuretyp_dic[key][2]:
                    for length in featuretyp_dic[key][1]:
                        c_path = os.path.join(*path, sub)
                        minTw_gram_auth = [tweetLen, [length], authors]

                        if not key in ['num', 'polarity', 'emoticon_c']:
                            pass
                            #job = pool.apply_async(make_subsets, (key, minTw_gram_auth, c_path, id_type, 'train', True))
                            #jobs.append(job)
                        else:
                            job = pool.apply_async(make_numeric_subsets, (key, minTw_gram_auth, c_path, id_type, 'train', True))
                            jobs.append(job)



    # collect results from the workers through the pool result queue
    print('collect results from job cycle...')
    sys.stdout.flush()
    for i in range(0, len(jobs)):
        tmp = jobs.pop(0)
        tmp.get()
        del tmp
    print('sleep after cycle...')
    sys.stdout.flush()
    print('closing down the pool and exit :)')
    sys.stdout.flush()
    pool.close()
    pool.join()
    print('done')
    sys.stdout.flush()




