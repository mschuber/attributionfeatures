from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, HashingVectorizer, TfidfVectorizer
import pandas as pd
import scipy
import numpy as np
import pickle
import math
import statistics
import json
import ndjson
import os
import sys
import gc
from ML import ml_utils


def make_true_train(subset):
    if subset == 'crossval':
        subset = 'train'
        true_train = False
    elif subset == 'train':
        true_train = True
    else:
        true_train = False

    return subset, true_train


def tfidf_hash(id, featuretyp, minTW, gram, numAuthors, vectorizers:dict=None):
    pass



def make_vectorizers():
    pass

def make_vocab_path(id, featuretyp, minTW, gram, numAuthors, path:str, hash='', cut= ('unchanged',0.1)):
    model_dir = ml_utils.make_save_dirs(path)
    vocab_dir = os.path.join(model_dir,
                                   featuretyp, str(minTW), id, 'vocab')
    typ=''
    if cut[1] != 0:
        typ = '_reduced'
    path_vocab = os.path.join(vocab_dir,
                              '{}_grams_{}_{}_{}_authors{}.vocab'.format(featuretyp, gram, minTW, numAuthors, typ))

    if hash:
        path_vocab = os.path.join(vocab_dir,
                     '{}_grams_{}_{}_{}_authors_reduced.colids'.format(featuretyp, gram, minTW,
                                                                       numAuthors))


    return path_vocab

def tfidf_no_hash(id, featuretyp, minTW, gram, numAuthors, model_dir, subset, true_train = False, cut= ('unchanged',0.1)):

    pass


def tfidf_sparse_load(id, featuretyp, minTW, gram, numAuthors, path:str, subset:str, vectorizers:dict=None, cut = ('unchanged',0.1), hash=''):
    base_str = ''
    if cut[1] == 0:
        base_str = '_base'

    if hash !='':
        hash = '_hash'

    matrix_file = os.path.join(path, featuretyp, str(minTW), id, subset, 'tfidf',
                 'sparse_{}_{}_grams_{}_{}_{}_authors{}{}'.format(subset, featuretyp, gram, minTW,
                                                                   numAuthors, hash, base_str))

    matrix_ids = os.path.join(path, featuretyp, str(minTW), id, subset, 'tfidf',
                              'sparse_uID_{}_{}_grams_{}_{}_{}_authors{}.p'.format(subset, featuretyp, gram,
                                                                                 minTW, numAuthors, hash))

    if os.path.exists(matrix_file+'.npz'):
        print('Loaded sparse matrix from disk...')
        sys.stdout.flush()
        mtx = ml_utils.load_sparse(matrix_file)
        ids = ml_utils.load_uids(matrix_ids)
        if cut[0] == 'unchanged':
            return mtx, ids
        else:
            #TODO
            pass

    print('Did not find matrix-file...redo it from scratch')
    sys.stdout.flush()
    return tfidf(id, featuretyp, minTW, gram, numAuthors, path, subset, vectorizers, cut)


def tfidf(id, featuretyp, minTW, gram, numAuthors, path:str, subset:str, vectorizers:dict=None, cut=0.01, hash=False):
    model_dir = ml_utils.make_save_dirs(path)
    os.makedirs(os.path.join(path, featuretyp, str(minTW), id, subset, 'tfidf'), exist_ok=True)

    #define if we should retrain or whether we simply are in cross val
    subset, true_train = make_true_train(subset)

    if true_train or not vectorizers:
        print('do training...')
        if hash:
            tfidf_hash()
        else:
            tfidf_no_hash()

        return 'return_stuff'

    elif










