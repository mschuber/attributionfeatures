#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time




path = 'preprocess_scripts/to_do/'


folders = ['random_sample', 'life_phase', 'gender_balanced']
fold_abrev = ['rs', 'lph', 'gend']
professions = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
abbrev = ['sp', 'per', 'cr', 'pol', 'man', 'sci', 'prof', 'rel']
grams = ['singlegram', 'bigram']
gram_abrevv = ['si', 'bi']
life_phases = ['child_18', 'young_adult_35', 'adult_50', 'old_adult_65', 'retiree']

for folder in folders:

	if folder != 'life_phase':
		for prof in professions:
			for gram in grams:
				index = grams.index(gram)
				prof_index = professions.index(prof)
				i_fold = folders.index(folder)
				script =  """#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/{}_{}_{}.out
#SBATCH -e ./out/{}_{}_{}.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J {}_{}_{}
# Queue:
#SBATCH --partition=broadwell
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
#SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=80
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=threads
export SLURM_HINT=multithread 


module load gcc/8
module load anaconda/3/5.1
module load scikit-learn/0.19.1


# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/preprocess_general_single.py {} {} {} {} {} &&
mv $(basename $BASH_SOURCE) /draco/u/mschuber/PAN/Scripts/preprocess_done
echo 'job finished'""".format(fold_abrev[i_fold], abbrev[prof_index], gram_abrevv[index], fold_abrev[i_fold], abbrev[prof_index],
						gram_abrevv[index], fold_abrev[i_fold], abbrev[prof_index],gram_abrevv[index], folder, prof, None, gram, index+1)

				with open(path+folder+'/{}_{}_{}.cmd'.format(folder, prof, gram_abrevv[index]), 'w', encoding = 'utf-8') as f:
					f.write(script)

	else:
		for prof in professions:
			for phase in life_phases:
				for gram in grams:
					index = grams.index(gram)
					prof_index = professions.index(prof)
					i_fold = folders.index(folder)
					script =  """#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/{}_{}_{}_{}.out
#SBATCH -e ./out/{}_{}_{}_{}.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J {}_{}_{}_{}
# Queue:
#SBATCH --partition=broadwell
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
#SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=80
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=threads
export SLURM_HINT=multithread 

module load gcc/8
module load anaconda/3/5.1
module load scikit-learn/0.19.1

# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/preprocess_general_single.py {} {} {} {} {} &&
mv $(basename $BASH_SOURCE) /draco/u/mschuber/PAN/Scripts/preprocess_done
echo 'job finished'""".format(fold_abrev[i_fold], phase, abbrev[prof_index], gram_abrevv[index], fold_abrev[i_fold], phase, abbrev[prof_index],
							gram_abrevv[index], fold_abrev[i_fold], phase, abbrev[prof_index], gram_abrevv[index],folder, prof, phase, gram, index+1)
					with open(path+folder+'/{}_{}_{}_{}.cmd'.format(folder, phase, prof, gram_abrevv[index]), 'w', encoding = 'utf-8') as f:
						f.write(script)




