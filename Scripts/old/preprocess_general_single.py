#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time

datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
subset = sys.argv[1]
profession = sys.argv[2]
life_phase = sys.argv[3]
gramtype = sys.argv[4:6]
gramtype[1] = int(gramtype[1])

gramtype = ['singlegram', 1, 'bigram', 2]


labels = 'labels_pre.json'

#regex for rpeprocessing
http = re.compile(r'(https{0,1}:\S+)')
www = re.compile(r'(www\.\S+)')
emoji = regex.compile(r'\X')
tagging = re.compile(r'#\S+')
retweet = re.compile(r'(RT\s{0,1}@\w+(?![@,\\])[\w]:{0,1})')
mention = re.compile(r'(@\w+(?![@,\\])\w)')
splitter = re.compile(r'(§RETWEET§|§MENTION§|§LINK§|§TAG§)')



#load labels
with open(datapath+labels, 'r', encoding='utf-8') as f:
    data = json.load(f)

def make_file_paths(path):
	for el in pathmodify:
		os.makedirs(path, exist_ok=True)
re.sub

def preprocess(text, q):

    text = retweet.sub('§RETWEET§', text)
    #find and substitute HTTP-URLs
    text = http.sub('§LINK§', text)
    #find and substitute www-URLs
    text = www.sub('§LINK§', text)
    #find and substitute mentions
    text = mention.sub('§MENTION§', text)
    #find and substitute Tagging
    text = tagging.sub('§TAG§', text)
    #split text
    text = splitter.split(text)
    tmp = ['§BEG§']
    tmp.extend(text)
    text = tmp.copy()
    text.append('§END§')
    del tmp
    #make character lists; keep unicode emojis as well as subsitutes as singel individual character
    text_l = []
    for el in text:
        if el not in ['§BEG§', '§RETWEET§', '§LINK§', '§MENTION§','§TAG§', '§END§']:
            text_l.extend(emoji.findall(el))
        else:
            text_l.append(el)
    del text
    gc.collect()

    return text_l



def zipngram(text, n, q):
    ngrams = zip(*[text[i:] for i in range(n)])
    return ["".join(ngram) for ngram in ngrams]



def combine(dic, n, typ, q):
    diclist = []
    i=0
    label = data[str(dic['id'])]
    for tweet in dic['text']:
        savedic = {}
        i +=1
        savedic['text_org'] = tweet
        savedic['tweet_id'] = str(dic['id'])+'_'+str(i)
        identifiers_l = str(dic['id'])+'_'+str(i)
        savedic['author_id'] = int(dic['id'])
        authorid_l = int(dic['id'])
        savedic['birthyear'] = int(label['birthyear'])
        savedic['gender'] = label['gender']
        savedic['occupation'] = label['occupation']
        savedic['fame'] = label['fame']
        if n> 1:
            savedic[typ] = zipngram(preprocess(savedic.pop('text_org', None), q), n, q=q)
        else:
            savedic[typ] = preprocess(savedic.pop('text_org', None), q)
        diclist.append(savedic)
    return diclist


def process_wrapper(chunkStart, fname, n, typ, q, chunkSize=None):
    with open(fname, 'r', encoding='utf-8') as f:
        f.seek(chunkStart)
        line = f.readline()
    if line or line !='':
        dic = ndjson.loads(line)[0]
        res = combine(dic, n, typ, q)
        q.put([res, n])
    return 1

def chunkify(fname,size=np.NaN):
    linebytes = []
    i = 0
    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            nextLineByte = f.tell()
            while True:
                linebytes.append(nextLineByte)
                line = f.readline()
                nextLineByte = f.tell() #returns the location of the next line
                #if i == 1000:
                #   break
                i+=1
                if not line or line == '':
                    break
    else:
        sys.exit('Chunking not yet implemented')
    return linebytes



def listener(fname1, fname2, n1, n2, q):

    f1 = open(fname1, 'w', encoding = 'utf-8')
    writer1 = jsonlines.Writer(f1, flush = True)
    f2 = open(fname2, 'w', encoding = 'utf-8')
    writer2 = jsonlines.Writer(f2, flush = True)    
    
    while True:
        mess = q.get() ##Q:
        if mess == 'kill':
            break

        if mess[1] == n1:
            for m in mess[0]:
                writer1.write(m)
        else:
            for m in mess[0]:
                writer2.write(m)            
        del mess


    f1.close()
    writer1.close()
    f2.close()
    writer2.close()
    print('writer is finished')
    sys.stdout.flush()

def main():
    print('make files...')
    if subset != 'life_phase':
        fname = subset+'_'+profession+'.ndjson'
        outname1 = subset+'_'+profession+'_preprocessed_'+gramtype[0]+'.ndjson'
        outname2 = subset+'_'+profession+'_preprocessed_'+gramtype[2]+'.ndjson'
        path = datapath+subset+'/'+profession+'/'

    else:
        fname = subset+'_'+life_phase+'_'+profession+'.ndjson'
        outname1 = subset+'_'+life_phase+'_'+profession+'_preprocessed_'+gramtype[0]+'.ndjson'
        outname2 = subset+'_'+life_phase+'_'+profession+'_preprocessed_'+gramtype[2]+'.ndjson'
        path = datapath+subset+'/'+life_phase+'/'+profession+'/'
    sys.stdout.flush()
    #make_file_paths()
    print('create managed queue...')
    manager = mp.Manager()
    q = manager.Queue()    
    #pool = mp.Pool(mp.cpu_count()-2)
    pool = mp.Pool(mp.cpu_count())
    print('create listener for saving of data...')
    #put listener to work first
    watcher = pool.apply_async(listener, (path+outname1, path+outname2, gramtype[1], gramtype[3], q,))
    #fire off workers
    jobs = []
    #create jobs
    print('make job queue...')
    print('collect lineBytes')
    sys.stdout.flush()
    lineBytes = chunkify(path+fname)
    #n = 8000 ###48000 lines
    #lineBytes = [lineBytes[i:i + n] for i in range(0, len(lineBytes), n)] ##chunkify list
    #for lineByte in lineBytes:
    print('enter cycle...')
    sys.stdout.flush()
    for i in range(0, len(lineBytes)):
        byte = lineBytes.pop(0)
        job = pool.apply_async(process_wrapper,(byte, path+fname, gramtype[1], gramtype[0], q,))
        job = pool.apply_async(process_wrapper,(byte, path+fname, gramtype[3], gramtype[2], q,))
        jobs.append(job)

    print('collect results from jobs...')
    sys.stdout.flush()
    # collect results from the workers through the pool result queue
    for j in range(0, len(jobs)):
        tmp = jobs.pop(0)
        tmp = tmp.get()
        del tmp
    print('sleep after cycle...')
    sys.stdout.flush()
    time.sleep(20)
    print('kill all remaining workers...')
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool and exit :)')
    pool.close()
    pool.join()
    print('done')


if __name__ == "__main__":
    main()
