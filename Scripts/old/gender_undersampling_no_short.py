#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
from statistics import median, mean, stdev
import math
import time
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import train_test_split


###read in data starting bytes, gender and 


datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '/draco/u/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
label = 'labels_pre.json'
feeds = 'feeds.ndjson'
#typset = ['workset', 'gender_balanced', 'random_sample', 'life_phase']
typset = ['workset']

with open(datapath+label, 'r') as f:
    labels = json.load(f)

##phases of life are: child (<18), young adult (<35), adult (<50), old adult (<65), retiree

def make_file_paths():
    dirs = ['workset', 'gender_balanced', 'random_sample', 'life_phase']
    occs = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
    lifePhase = ['child_18', 'young_adult_35', 'adult_50', 'old_adult_65', 'retiree']
    for el in dirs:
        os.makedirs(datapath+el, exist_ok=True)
        for oc in occs:
            if el == 'life_phase':
                for phase in lifePhase:
                    os.makedirs(datapath+el+'/'+phase+'/'+oc, exist_ok=True)
            else:
                os.makedirs(datapath+el+'/'+oc, exist_ok=True)



def balance_sampler(indices, gender):
    indices = np.array(indices).reshape(-1,1)
    rus = RandomUnderSampler(random_state=0)
    X_resampled, y_resampled = rus.fit_resample(indices, gender)
    return list(X_resampled.reshape(1,-1)[0])



def check_length(tweetList, std, inner):

    tweetList = [el for el in tweetList if len(el)>inner]

    if len(tweetList) < std:
        tweetList = []
    return tweetList




def listener(q, typeset):
    occs = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
    #lifePhase = ['child_18', 'young_adult_35', 'adult_50', 'old_adult_65', 'retiree']
    lifePhase = []
    writers = []
    filehandles = []
    ##for all other sets
    if typeset != 'life_phase':
        f = open(datapath+typeset+'/{}.ndjson'.format(typeset), mode='w', encoding='utf-8')
        writer = jsonlines.Writer(f, flush=True)
        filehandles.append(f)
        writers.append(writer)
        for oc in occs:
            f = open(datapath+typeset+'/'+oc+'/{}_{}.ndjson'.format(typeset, oc), mode='w', encoding='utf-8')
            writer = jsonlines.Writer(f, flush=True)
            filehandles.append(f)
            writers.append(writer)


        while True:
        #print('waiting for get...')
            mess = q.get() ##Q:
            if mess == 'kill':
                break
            index = occs.index(mess[0])+1

            writers[0].write(mess[2])
            writers[index].write(mess[2])


            del mess
    ###if setytpe is life_phase
    else:
        f = open(datapath+typeset+'/{}.ndjson'.format(typeset), mode='w', encoding='utf-8')
        writer = jsonlines.Writer(f, flush=True)
        filehandles.append(f)
        writers.append([writer])
        for phase in lifePhase:
            tmp = []
            f = open(datapath+typeset+'/'+phase+'/{}_{}.ndjson'.format(typeset, phase), mode='w', encoding='utf-8')
            writer = jsonlines.Writer(f, flush=True)
            filehandles.append(f)
            tmp.append(writer)
            for oc in occs:
                f = open(datapath+typeset+'/'+phase+'/'+oc+'/{}_{}_{}.ndjson'.format(typeset, phase, oc), mode='w', encoding='utf-8')
                writer = jsonlines.Writer(f, flush=True)
                filehandles.append(f)
                tmp.append(writer)
            writers.append(tmp)
        while True:
        #print('waiting for get...')
            mess = q.get() ##Q:

            if mess == 'kill':
                break


            index = occs.index(mess[0])+1
            index_phase = lifePhase.index(mess[1])+1

            writers[0][0].write(mess[2])
            writers[index_phase][0].write(mess[2])
            writers[index_phase][index].write(mess[2])

            del mess

    for r in range(0, len(writers)):
        if typeset == 'life_phase':
            for w in range(0, len(writers[r])):
                writers[r][w].close()
        else:
            writers[r].close()

    for r in range(0,len(filehandles)):
        filehandles[r].close()

    print('finished writing')
    sys.stdout.flush()



def chunkify(fname,labels, size=np.NaN):
    linebytes = []
    i = 0
    average_posts = []
    average_text = []
    gender = []
    indices = []
    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            print(fname)
            nextLineByte = f.tell()
            while True:
                line = f.readline()
                if line or line != '':               
                    line = ndjson.loads(line)[0]
                    if labels[str(line['id'])]['gender'] != 'nonbinary':
                        gender.append(labels[str(line['id'])]['gender'])
                        linebytes.append(nextLineByte)
                        average_posts.append(len(line['text']))
                        average_text.append([len(el) for el in line['text']])
                        indices.append(i)
                    nextLineByte = f.tell() #returns the location of the next line
                    #if i == 1000:
                    #   break
                    i+=1
                else:
                    break
    else:
        sys.exit('Chunking not yet implemented')
    return linebytes, average_posts, average_text, gender, indices



def process_wrapper(fname, lineByte, std, inner, typeset, q):
    with open(fname, 'r', encoding='utf-8') as f:
        f.seek(lineByte)
        lines = f.readline()
        sys.stdout.flush()
        dic = ndjson.loads(lines)[0]
    #dic['text'] = check_length(dic['text'], std, inner)
    age = 2019 - labels[str(dic['id'])]['birthyear']
    if age <21:
        lifePhase = 'child_18'

    elif age <36:
        lifePhase = 'young_adult_35'
    elif age <51:
        lifePhase = 'adult_50'
    elif age <66:
        lifePhase = 'old_adult_65'
    else:
        lifePhase = 'retiree'

    if dic['text']:
        q.put([labels[str(dic['id'])]['occupation'], lifePhase, dic])  ##result is list [occupation, dic]
    return 1

#####################################################################

def multiprocess(lineBytes, typeset, datapath, feeds, std, lower_oct_text):
    print('create managed queue...')
    sys.stdout.flush()
    manager = mp.Manager()
    q = manager.Queue()    
    #pool = mp.Pool(mp.cpu_count()-2)
    pool = mp.Pool(mp.cpu_count())
    print('create listener for saving of data...')
    sys.stdout.flush()
    #put listener to work first
    watcher = pool.apply_async(listener, (q, typeset,))
    #fire off workers
    jobs = []
    #create jobs
    print('make job queue...')
    sys.stdout.flush()
    print('enter cycle...')
    sys.stdout.flush()
    for i in range(0, len(lineBytes)):
        byte = lineBytes.pop(0)
        job = pool.apply_async(process_wrapper,(datapath+feeds, byte, std, lower_oct_text, typeset, q,))
        jobs.append(job)

    print('collect results from jobs...')
    sys.stdout.flush()
    # collect results from the workers through the pool result queue
    for j in range(0, len(jobs)):
        tmp = jobs.pop(0)
        tmp = tmp.get()
        del tmp
    #print('sleep after cycle...')
    #sys.stdout.flush()
    #time.sleep(20)
    print('kill all remaining workers...')
    sys.stdout.flush()
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool')
    sys.stdout.flush()
    pool.close()   
    pool.join()
    print('exit multiprocess')
    sys.stdout.flush()


def random_sample(x, strat, keep_ratio):
    keep, throw = train_test_split(x, random_state = 0, stratify= strat, train_size=keep_ratio)

    return keep



##main function


def main():
    print('make files...')
    sys.stdout.flush()
    make_file_paths()
    print('collect lineBytes')
    sys.stdout.flush()
    lineBytes, num_posts, leng_texts, gender, indices = chunkify(datapath+feeds, labels)
    #make standard dev for tweet amount and median of lower eigth of text-length for subsampling of data
    std = math.ceil(stdev(num_posts))
    count = 0
    #for i in range(0, len(num_posts)):
    #    if std > num_posts[i]:
    #        tmp = leng_texts.pop(i-count)
    #        l = lineBytes.pop(i-count)
    #        g = gender.pop(i-count)
    #        indi = indices.pop(i-count)
    #        count +=1
    leng_texts = [val for sublist in leng_texts for val in sublist]

    med = median(leng_texts)
    lower_quart_text = median([el for el in leng_texts if el < med])
    lower_oct_text = median([el for el in leng_texts if el < lower_quart_text])
    org_bytes = lineBytes.copy()
    for typ in typset:
        lineBytes = org_bytes.copy()
        print('analysing subset {}'.format(typ))
        sys.stdout.flush()
        if typ == 'gender_balanced':
            lineBytes = balance_sampler(lineBytes, gender)
        elif typ == 'random_sample':
            lineBytes = random_sample(lineBytes, gender, 0.3)
            print(len(lineBytes))



        multiprocess(lineBytes, typ , datapath, feeds, std, lower_oct_text)

    print('done')


if __name__ == "__main__":
    main()
