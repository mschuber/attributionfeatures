#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
from statistics import median, mean, stdev
import math
import time
from imblearn.under_sampling import RandomUnderSampler
from sklearn.model_selection import train_test_split





datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
subset = 'workset'
filebeg = 'workset_preprocessed_'
filend  = '.ndjson'
grams = ['singlegram', 'bigram']
outfolder = 'stratified_subsample'
sizes = [200, 500, 1000, 2000]


def make_file_paths(dirname):
    os.makedirs(dirname, exist_ok=True)

def chunkify(fname, size=np.NaN):
    i = 0
    strat = []
    ids = []
    authors = {}
    if np.isnan(size):
        print('size is nan...look at single lines only')
        sys.stdout.flush()
        
        with open(fname, 'r', encoding='utf-8') as f:
            nextLineByte = f.tell()
            while True:
                line = f.readline()
                if line or line != '':               
                    line = ndjson.loads(line)[0]
                    age = 2019-line['birthyear']
                    autId = line['author_id']
                    
                    if autId in authors:

                        authors[autId]['lineBytes'].append(nextLineByte)

                    else:

                        if age <22:
                            lifePhase = 'child_21'

                        elif age <36:
                            lifePhase = 'young_adult_35'
                        elif age < 51:
                            lifePhase = 'adult_50'
                        elif age <66:
                            lifePhase = 'old_adult_65'
                        else:
                            lifePhase = 'retiree'
                        strat.append(line['gender']+'_'+lifePhase)
                        print(strat[-1])
                        sys.stdout.flush()
                        ids.append(autId)

                        authors[autId] = {}
                        authors[autId]['lineBytes'] = [nextLineByte]
                        authors[autId]['age_group'] = lifePhase

                    nextLineByte = f.tell() #returns the location of the next line
                    #if i == 30000:
                    #   break
                    i+=1
                else:
                    break
    else:
        sys.exit('Chunking not yet implemented')
    return authors, strat, ids

def random_strat_draw(x, strat, size):

    keep, throw = train_test_split(x, random_state = 123456, train_size = size, test_size = None)

    return keep


def balance_sampler(indices, balance):
    indices = np.array(indices).reshape(-1,1)
    rus = RandomUnderSampler(random_state=123456)
    X_resampled, y_resampled = rus.fit_resample(indices, balance)
    return list(X_resampled.reshape(1,-1)[0]), list(y_resampled)


def write_to_file(authors, subsample,infile,  outfile):

    o = open(outfile, 'w', encoding='utf-8')
    writer = jsonlines.Writer(o, flush = True)


    f = open(infile, 'r', encoding='utf-8')

    for ident in subsample:
        author = authors[ident]
        lineBytes = author['lineBytes']
        for lineByte in lineBytes:
            f.seek(lineByte)
            lines = f.readline()
            if lines or lines != '':
                dic = ndjson.loads(lines)[0]
                dic['age_group'] = author['age_group']
                writer.write(dic)

    writer.close()
    f.close()
    o.close()



def process_wrapper(filepath, outfile, size, gram):
    print('get ids for {} with size {}'.format(gram, size))
    sys.stdout.flush()
    authors, strat, ids = chunkify(filepath)
    print('make balanced sample for {} with size {} from initially {}'.format(gram, size, len(ids)))
    sys.stdout.flush()
    ids, strat = balance_sampler(ids, strat)
    print('undersampled for {} with subset size {} - number of observations is now {}'.format(gram, size, len(ids)))
    sys.stdout.flush()
    printsize = len(ids)
    if len(ids) > size:
        printsize = size
        print('make random draw {} with size {}'.format(gram, size))
        sys.stdout.flush()
        ids = random_strat_draw(ids, strat, size)

    print('write to file for {} with size {}'.format(gram, size))
    sys.stdout.flush()
    write_to_file(authors, ids, filepath, outfile+str(printsize)+'.ndjson')
    print('done for {} with size {}'.format(gram, size))
    sys.stdout.flush()

    return 1






def main():
    pool = mp.Pool(mp.cpu_count())
    jobs = []

    print('make outdirs')
    sys.stdout.flush()
    make_file_paths(datapath+outfolder)
    print('done making dirs')
    sys.stdout.flush()

    for gram in grams:
        for size in sizes:
            job = pool.apply_async(process_wrapper, (datapath+subset+'/'+filebeg+gram+filend, datapath+outfolder+'/stratified_subsample_preprocessed_'+gram+'_', size, gram))
            jobs.append(job)

    for job in jobs:
        tmp = job.get()


    pool.close()
    pool.join()

    print('done...wil now exit :)')
    sys.stdout.flush()



if __name__ == "__main__":
    main()

