#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/grid_child18_bi.out
#SBATCH -e ./out/grid_child18_bi.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J grid_child18_bi
# Queue:
#SBATCH --partition=small
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=16
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=cores


module load gcc/8
module load anaconda/3/5.1
module load scikit-learn/0.19.1

# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/grid_search_general.py life_phase bigram 100 child_18
echo "job finished"