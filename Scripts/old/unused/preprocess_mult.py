#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc

#datapath = '/cobra/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'


combinefile = 'combined_preprocess'
combined = 'combined_mult.ndjson'

#regex for rpeprocessing
http = re.compile(r'(https{0,1}:\S+)')
www = re.compile(r'(www\.\S+)')
emoji = regex.compile(r'\X')
tagging = re.compile(r'#\S+')
retweet = re.compile(r'(RT\s{0,1}@\w+(?![@,\\])[\w]:{0,1})')
mention = re.compile(r'(@\w+(?![@,\\])\w)')

prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
writers = []
filehandles = []
pathmodify = ['singlegrams/', 'bigrams/', 'trigrams/', 'quadgrams/']
types =['', '_bigrams', '_trigrams', '_quadgrams'] 

for el in pathmodify:
    os.makedirs(datapath+el, exist_ok=True)

for i in range(0, len(types)):
    item = types[i]
    gramwriter = []
    f = open(datapath+pathmodify[i]+combinefile+'{}.ndjson'.format(item), mode='w+', encoding='utf-8')
    writer = jsonlines.Writer(f)

    gramwriter.append(writer)
    filehandles.append(f)

    for j in range(0, len(prof)):
        job = prof[j]

        f_p = open(datapath+pathmodify[i]+combinefile+'{}_{}.ndjson'.format(item, job), mode='w+', encoding='utf-8')
        writer_p = jsonlines.Writer(f_p)
        gramwriter.append(writer_p)
        filehandles.append(f_p)

    writers.append(gramwriter)


def preprocess(dic):

    #tweet_pre = ['§BEGIN§']
    tweet_pre = []
    tweet_pre.extend(retweet.split(dic.pop('text_org', None)))

    tweet_pre = [retweet.sub('§RETWEET§', tweet_pre[i]) for i in range(0, len(tweet_pre))]
    #tweet_pre.append('§END§')
    tmp_tweet = []
    #find and substitute HTTP-URLs
    for i in range(0, len(tweet_pre)):
        tmp = http.split(tweet_pre[i])
        tmp = [http.sub('§LINK§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute www-URLs
    for i in range(0, len(tweet_pre)):
        tmp = www.split(tweet_pre[i])
        tmp = [www.sub('§LINK§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
    
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute mentions
    for i in range(0, len(tweet_pre)):
        tmp = mention.split(tweet_pre[i])
        tmp = [mention.sub('§MENTION§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
        
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute Tagging
    for i in range(0, len(tweet_pre)):
        tmp = tagging.split(tweet_pre[i])
        tmp = [tagging.sub('§TAG§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
        
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #make character lists; keep unicode emojis as well as subsitutes as singel individual character
    for i in range(0, len(tweet_pre)):
        
        if tweet_pre[i] not in ['§RETWEET§', '§LINK§', '§MENTION§', '§TAGGING§', '§BEGIN§', '§END§']:
            tmp = emoji.findall(tweet_pre[i])
            tmp_tweet.extend(tmp)
        else:
            tmp_tweet.append(tweet_pre[i])
    #dic['text_pre'] = tmp_tweet
    #del tmp_tweet
    gc.collect()

    return dic, tmp_tweet

def process_wrapper(chunkStart):
    res = []
    grams = []
    with open(datapath+combined, 'r', encoding='utf-8') as f:
        f.seek(chunkStart)
        lines = f.readline()
        lines = ndjson.loads(lines)[0]
    
    dic, text_pre = preprocess(lines)
    res.append(dic)
    grams.append(text_pre)
    del text_pre
    del dic
    tmp = ngrams(grams[0])
    grams.extend(tmp)
    res.append(grams)
    del lines
    del grams
    del tmp
    gc.collect()
    return res


def chunkify(fname,size=np.NaN):
    linebytes = []
    if np.isnan(size):
        print('size is nan...look at single lines only')
        i = 0
        #fileEnd = os.path.getsize(fname)

        with open(fname, 'r', encoding='utf-8') as f:

            nextLineByte = f.tell()
            while True:
                linebytes.append(nextLineByte)
                line = f.readline()
                nextLineByte = f.tell() #returns the location of the next line
                if i == 1000:
                   break
                i+=1
                if not line or line == '':
                    break
    else:
        sys.exit('Chunking not yet implemented')


    return linebytes

def ngrams(charlist):
    bigrams = []
    trigrams = []
    quadgrams = []
    leng = len(charlist)
    for i in range(0, leng):
        if i <leng-4:
            quadgrams.append(charlist[i] + charlist[i+1] + 
                              charlist[i+2] + charlist[i+3])
            trigrams.append(charlist[i] + charlist[i+1] + charlist[i+2])
            bigrams.append(charlist[i] + charlist[i+1])
        elif i <leng-3:
            trigrams.append(charlist[i] + charlist[i+1] + charlist[i+2])
            bigrams.append(charlist[i] + charlist[i+1])            
        elif i<leng-1:
            bigrams.append(charlist[i] + charlist[i+1])

    return [bigrams, trigrams, quadgrams]


def save_to_file(resultlist):
    lock.acquire()
    dic = resultlist[0]
    grams = resultlist[1:5] #oder: bigrams trigrams quadgrams
    swriters = writers
    prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
    types = ['', '_bigrams', '_trigrams', '_quadgrams']
    index = prof.index(dic['occupation'])+1 ##since the writer list has the baseline writer in it

    for i in range(0, len(types)):

        dic['text_pre'] = grams[i]
        swriters[i][0].write(dic)
        swriters[i][index].write(dic)


    lock.release()








##make global lock
def init(lock_):

    global lock
    
    lock = lock_


def main():
    print('start pool...')
    sys.stdout.flush()

    lock = mp.Lock()
    p = mp.Pool(mp.cpu_count(), maxtasksperchild=500, initializer=init, initargs=(lock,))
    print('chunkify...')
    sys.stdout.flush()
    #p = mp.Pool(mp.cpu_count(), maxtasksperchild=5)
    #for lineByte in chunkify(datapath+combined, size = np.NaN): #get bytes for singular lines
    lineBytes = chunkify(datapath+combined)
    n = 10000
    lineBytes = [lineBytes[i:i + n] for i in range(0, len(lineBytes), n)] ##chunkify list

    print('finished chunkification...')
    print('make empty files...')
    sys.stdout.flush()






    print('finished making files..')
    print('starting with chunks...')
    sys.stdout.flush()


    for chunk in lineBytes:
        print('next chunk...')
        sys.stdout.flush()
        results = []
        m = 0
        for startByte in chunk:
            result = p.apply_async(process_wrapper, (startByte,), callback=save_to_file )
            results.append(result)


        for result in results:
            result.get()
        del results

    print('closing down the pool and exit :)')
    p.close()
    p.join()



    for i in range(0, len(writers)):
        for j in range(0, len(writers[i])):
            writers[i][j].close()

    for handle in filehandles:
        handle.close()

    print('exit')





if __name__ == "__main__":
    main()



