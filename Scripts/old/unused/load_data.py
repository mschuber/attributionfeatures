#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time




direct = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/workset/'
file = 'workset_preprocessed_bigram.ndsjon'

def read_data(fname,size=np.NaN):
    pid = os.getpid()
    process = psutil.Process(os.getpid())
    text = []
    author_id = []
    gender = []
    i = 0
    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            for line in f:
                dic = ndjson.loads(line)[0]
                text.append(dic['bigram'])
                author_id.append(dic['author_id'])
                gender.append(dic['gender'])
                i += 1
                if i%(2**10)==0:
                    print('Process Mem after {} tweets is {}'.format(i, process.memory_info()[1] / float(2 ** 20)))

                del dic


    else:
        sys.exit('Chunking not yet implemented')
    return text, author_id, gender



text, author_id, gender = read_data(direct+file)
