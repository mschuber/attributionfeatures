#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time
import collections




direct = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/workset/'

file = 'workset_preprocessed_bigram.ndsjon'


def read_data(fname,size=np.NaN):
    pid = os.getpid()
    process = psutil.Process(os.getpid())

    vocab = collections.Counter()

    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            for line in f:
                dic = ndjson.loads(line)[0]
                for el in dic['bigram']:
                    vocab[el] += 1
                del dic


    else:
        sys.exit('Chunking not yet implemented')
    return vocab



vocab = read_data(direct+file)

count = []
print('make count')
sys.stdout.flush()
count.extend(vocab.most_common((2**18)-1))
print('finished count')
sys.stdout.flush()
vocab = {}

i = 0
for token, tcount in count:
    vocab[token] = i
    i += 1

print('made vocab')
sys.stdout.flush()

with open(direct + 'workset_vocab_bigram.json', 'w') as f:
    json.dump(vocab, f)

print('saved vocab')
sys.stdout.flush()