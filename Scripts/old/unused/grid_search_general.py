#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import pickle
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import f1_score, make_scorer
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import PassiveAggressiveClassifier, RidgeClassifier, SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from joblib import dump, load
import collections



#Paramters for train-testsplit
test_size = 0.1
random_state = 123456


direct = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#direct = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
filen = '_preprocessed_'+sys.argv[2]+'.ndjson'
savedir = 'ml/'

subset = sys.argv[1]
grams = sys.argv[2]
phase = sys.argv[4]
copies = int(sys.argv[3])

cores = mp.cpu_count()
#cores = 16

if str(phase) != 'None':
    subdir = subset + '/' + phase
    filebeg = subset+'_'+ phase

else:
    subdir = subset
    filebeg = subset


def make_file_paths(dirname):
    os.makedirs(dirname, exist_ok=True)

def read_data(fname,size=np.NaN):
    text = []
    author_id = []
    gender = []
    indices = []
    age = []
    i = 0
    lineBytes = []
    offset = 0
    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            for line in f:
                lineBytes.append(offset)
                dic = ndjson.loads(line)[0]
                text.append(dic[grams])
                author_id.append(dic['author_id'])
                gender.append(dic['gender'])
                age.append(dic['birthyear'])
                indices.append(i)
                offset = f.tell()
                i+=1
                

    else:
        sys.exit('Chunking not yet implemented')
    return text, author_id, indices, gender, age, i, lineBytes



##function for identity since we already made ngrams and do not want any further tokenization or preprocessing
def identity_tokenizer(text):
    return text




make_file_paths(direct+subdir+'/'+savedir)
make_file_paths(direct+subdir+'/'+'split_data')
if not os.path.exists(direct+subdir+'/'+'split_data'+'/{}_{}_test.json'.format(subset, grams)):

    print('read data')
    sys.stdout.flush()
    text, author_id, indices, gender, age, line_num, words, lineBytes = read_data(direct+subdir+'/'+filebeg+filen)





    print('make train-test split')
    sys.stdout.flush()
    train, test = train_test_split(indices, random_state = random_state, stratify= author_id, test_size=test_size)

    train_set = []
    test_set = []

    y_train_author = []
    y_test_author = []
    y_train_gender = []
    y_test_gender = []
    y_train_age = []
    y_test_age = []

    train_bytes = []
    test_bytes = []


    print('subset the data accordingly')
    sys.stdout.flush()
    ####make test_ident dic for easy identification
    for el in test:
        test_set.append(text[el])
        y_test_author.append(author_id[el])
        y_test_gender.append(gender[el])
        y_test_age.append(age[el])

    for el in train:
        train_set.append(text[el])
        y_train_author.append(author_id[el])
        y_train_gender.append(gender[el])
        y_train_age.append(age[el])

    del text
    del author_id
    del gender
    del age
    del indices

    gc.collect()


    print('make vocab')
    sys.stdout.flush()
    words = [token for sublist in train_set for token in sublist]
    vocab = {}
    count = []
    count.extend(collections.Counter(words).most_common((2**18)-1))


    i = 0
    for token, tcount in count:
        vocab[token] = i
        i += 1

    with open(direct+subdir+'/'+'split_data'+'/{}_{}_vocab.json'.format(subset, grams), 'w') as f:
        json.dump(vocab, f)

    del count
    del words

    gc.collect()
    print('save the data')
    sys.stdout.flush()
    ##save data splits

    with open(direct+subdir+'/'+'split_data'+'/{}_{}_vocab.json'.format(subset, grams), 'w') as f:
        json.dump(vocab, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_{}_train.json'.format(subset, grams), 'w', encoding='utf-8') as f:
        json.dump(train_set, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_{}_test.json'.format(subset, grams), 'w', encoding='utf-8') as f:
        json.dump(test_set, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_author_train.json'.format(subset), 'w', encoding='utf-8') as f:
        json.dump(y_train_author, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_author_test.json'.format(subset), 'w', encoding='utf-8') as f:
        json.dump(y_test_author, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_gender_train.json'.format(subset), 'w', encoding='utf-8') as f:
        json.dump(y_train_gender, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_gender_test.json'.format(subset), 'w', encoding='utf-8') as f:
        json.dump(y_test_gender, f)
    with open(direct+subdir+'/'+'split_data'+'/{}_age_train.json'.format(subset), 'w', encoding='utf-8') as f:
        json.dump(y_train_age, f)

    with open(direct+subdir+'/'+'split_data'+'/{}_age_test.json'.format(subset), 'w', encoding='utf-8') as f:
        json.dump(y_test_age, f)

else:
    print('load data')
    sys.stdout.flush()
    train_set = json.load(open(direct+subdir+'/'+'split_data'+'/{}_{}_train.json'.format(subset, grams)))
    test_set = json.load(open(direct+subdir+'/'+'split_data'+'/{}_{}_test.json'.format(subset, grams)))

    y_train_author = json.load(open(direct+subdir+'/'+'split_data'+'/{}_author_train.json'.format(subset)))
    y_test_author = json.load(open(direct+subdir+'/'+'split_data'+'/{}_author_test.json'.format(subset)))
    y_train_gender = json.load(open(direct+subdir+'/'+'split_data'+'/{}_gender_train.json'.format(subset)))
    y_test_gender = json.load(open(direct+subdir+'/'+'split_data'+'/{}_gender_test.json'.format(subset)))
    y_train_age = json.load(open(direct+subdir+'/'+'split_data'+'/{}_age_train.json'.format(subset)))
    y_test_age = json.load(open(direct+subdir+'/'+'split_data'+'/{}_age_test.json'.format(subset)))

if not os.path.exists(direct+subdir+'/'+'split_data'+'/{}_{}_vocab.json'.format(subset, grams)):


    print('make vocab')
    sys.stdout.flush()

    words = [token for sublist in train_set for token in sublist]
    vocab = {}
    count = []
    count.extend(collections.Counter(words).most_common((2**18)-1))


    i = 0
    for token, tcount in count:
        vocab[token] = i
        i += 1

    del count
    del words
    with open(direct+subdir+'/'+'split_data'+'/{}_{}_vocab.json'.format(subset, grams), 'w') as f:
        json.dump(vocab, f)


else:

    vocab = json.load(open(direct+subdir+'/'+'split_data'+'/{}_{}_vocab.json'.format(subset, grams)))

gc.collect()



classifiers = {"SVM": SGDClassifier()}


parameter_grid = {"SVM": [{'clf__penalty': ['elasticnet'], 'clf__class_weight': ['balanced', None],
                       'clf__alpha':[0.0001,0.0005, 0.001], 'clf__l1_ratio':[0, 0.5, 1],
                       'clf__loss':['hinge'],
                       'clf__learning_rate':['optimal'],
                       'clf__n_jobs': [cores]}]}
#classifiernames = ["PA", "MNB", "SVM", "LOG", "Ridge", "GBC"]
classifiernames = ["SVM"]


if str(phase) != 'None':
    y_list = [['gender', y_train_gender], ['author', y_train_author]]

else:
    y_list = [['gender', y_train_gender], ['age', y_train_age], ['author', y_train_author]]



vectorizer = CountVectorizer(tokenizer = identity_tokenizer, vocabulary = vocab, lowercase=False)
train_set = vectorizer.transform(train_set)
test_set = vectorizer.transform(test_set)

gc.collect()

for classifier in classifiernames:
    print('begin grid search...')
    sys.stdout.flush()
    for y in y_list:
        ##encode labels
        print('...for category {}'.format(y[0]))
        sys.stdout.flush()
        if not os.path.exists(direct+subdir+'/'+savedir+filebeg+'_{}_encoder.json'.format(y[0])):
            le = LabelEncoder()
            le = le.fit(y[1])
            print(type(y[1]))
            print(type(y[1][0]))
            sys.stdout.flush()

            dump(le, direct+subdir+'/'+savedir+filebeg+'_{}_encoder.json'.format(y[0]))

        else:

            le = load(direct+subdir+'/'+savedir+filebeg+'_{}_encoder.json'.format(y[0]))


        Y_train_cat = le.transform(y[1])




        pipeline = Pipeline([('clf', classifiers[classifier])])

        params = parameter_grid[classifier]
        #params[0]['vect__ngram_range'] = [(1, 1), (1, 2)]
        #params[1]['vect__ngram_range'] = [(1, 1), (1, 2)]


        print('start grid search for classifier {} and {}'.format(classifier, y[0]))
        start = time.time()
        sys.stdout.flush()

        gs_clf = GridSearchCV(pipeline, params, cv=5, iid=False, pre_dispatch = copies, scoring = 'f1_weighted', refit = True)
        gs_clf = gs_clf.fit(train_set, Y_train_cat)

        print('finished GridSearch...continue with predictions')

        predictions = gs_clf.predict(test_set)

        with open(direct+subdir+'/'+savedir+filebeg+'_'+classifier+'_'+y[0]+'_predictions.json', 'w') as pred:
            json.dump(predictions, pred)


        with open(direct+subdir+'/'+savedir+filebeg+'_'+classifier+'_'+y[0]+'_grid_search_cv_results.json', 'w') as est:
            json.dump(gs_clf.cv_results_, est)

        
        dump(gs_clf, direct+subdir+'/'+savedir+filebeg+'_'+classifier+'_'+y[0]+'_grid_search_out.jlib')

        print('finished grid search for classifier {} and {} after {} hours '.format(classifier, y[0], (time.time()-start/3600)))
        sys.stdout.flush()





print('done')
