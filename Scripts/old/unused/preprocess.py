#!/usr/bin/env python3


import pandas as pd
import numpy as np
import ndjson
import jsonlines
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc


#datapath = '/cobra/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'


combinefile = 'combined_preprocess_mult'
combined = 'combined_mult.ndjson'

#regex for rpeprocessing
http = re.compile(r'(https{0,1}:\S+)')
www = re.compile(r'(www\.\S+)')
emoji = regex.compile(r'\X')
tagging = re.compile(r'#\S+')
retweet = re.compile(r'(RT\s{0,1}@\w+(?![@,\\])[\w]:{0,1})')
mention = re.compile(r'(@\w+(?![@,\\])\w)')

def preprocess(dic):

    dic = dic.to_dict() ##convert pandas series slice to dict
    #tweet_pre = ['§BEGIN§']
    tweet_pre = []
    tweet_pre.extend(retweet.split(dic['text_org']))
    tweet_pre = [retweet.sub('§RETWEET§', tweet_pre[i]) for i in range(0, len(tweet_pre))]
    #tweet_pre.append('§END§')
    tmp_tweet = []
    #find and substitute HTTP-URLs
    for i in range(0, len(tweet_pre)):
        tmp = http.split(tweet_pre[i])
        tmp = [http.sub('§LINK§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute www-URLs
    for i in range(0, len(tweet_pre)):
        tmp = www.split(tweet_pre[i])
        tmp = [www.sub('§LINK§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
    
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute mentions
    for i in range(0, len(tweet_pre)):
        tmp = mention.split(tweet_pre[i])
        tmp = [mention.sub('§MENTION§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
        
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute Tagging
    for i in range(0, len(tweet_pre)):
        tmp = tagging.split(tweet_pre[i])
        tmp = [tagging.sub('§TAG§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
        
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #make character lists; keep unicode emojis as well as subsitutes as singel individual character
    for i in range(0, len(tweet_pre)):
        
        if tweet_pre[i] not in ['§RETWEET§', '§LINK§', '§MENTION§', '§TAGGING§', '§BEGIN§', '§END§']:
            tmp = emoji.findall(tweet_pre[i])
            tmp_tweet.extend(tmp)
        else:
            tmp_tweet.append(tweet_pre[i])
    dic['text_pre'] = tmp_tweet
    del tmp_tweet

    ##make ngrams
    bigrams = []
    trigrams = []
    quadgrams = []
    leng = len(dic['text_pre'])
    for i in range(0, leng):
        if i <leng-4:
            quadgrams.append(dic['text_pre'][i] + dic['text_pre'][i+1] + 
                              dic['text_pre'][i+2] + dic['text_pre'][i+3])
            trigrams.append(dic['text_pre'][i] + dic['text_pre'][i+1] + dic['text_pre'][i+2])
            bigrams.append(dic['text_pre'][i] + dic['text_pre'][i+1])
        elif i <leng-3:
            trigrams.append(dic['text_pre'][i] + dic['text_pre'][i+1] + dic['text_pre'][i+2])
            bigrams.append(dic['text_pre'][i] + dic['text_pre'][i+1])            
        elif i<leng-1:
            bigrams.append(dic['text_pre'][i] + dic['text_pre'][i+1])
    dic['bigrams'] = bigrams
    dic['trigrams'] = trigrams
    dic['quadgrams'] = quadgrams
    del bigrams
    del trigrams
    del quadgrams

    gc.collect()
    return dic



##########################################
def listener(q):
    '''listens for messages on the q, writes to file. '''
    fp = open(datapath+combinefile+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer = jsonlines.Writer(fp)
    prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
    fp_sp = open(datapath+combinefile+'_sports'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_sp = jsonlines.Writer(fp_sp)
    fp_per = open(datapath+combinefile+'_performer'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_per = jsonlines.Writer(fp_per)
    fp_cre = open(datapath+combinefile+'_creator'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_cre = jsonlines.Writer(fp_cre)
    fp_pol = open(datapath+combinefile+'_politics'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_pol = jsonlines.Writer(fp_pol)
    fp_man = open(datapath+combinefile+'_manager'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_man = jsonlines.Writer(fp_man)
    fp_sci = open(datapath+combinefile+'_science'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_sci = jsonlines.Writer(fp_sci)
    fp_pro = open(datapath+combinefile+'_professional'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_pro = jsonlines.Writer(fp_pro)
    fp_rel = open(datapath+combinefile+'_religious'+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer_rel = jsonlines.Writer(fp_rel)
    while True:
        mess = q.get()
        if mess == 'kill':
            break

        writer.write(mess)
        if mess['occupation'] == 'sports':
            writer_sp.write(mess)
            fp_sp.flush()

        elif mess['occupation'] == 'performer':
            writer_per.write(mess)
            fp_per.flush()

        elif mess['occupation'] == 'creator':
            writer_cre.write(mess)
            fp_cre.flush()

        elif mess['occupation'] == 'politics':
            writer_pol.write(mess)
            fp_pol.flush()

        elif mess['occupation'] == 'manager':
            writer_man.write(mess)
            fp_man.flush()

        elif mess['occupation'] == 'science':
            writer_sci.write(mess)
            fp_sci.flush()

        elif mess['occupation'] == 'professional':
            writer_pro.write(mess)
            fp_pro.flush()

        else:
            writer_rel.write(mess)
            fp_rel.flush()


    #close subfiles
    writer_rel.close()
    writer_pro.close()
    writer_sci.close()
    writer_man.close()
    writer_pol.close()
    writer_cre.close()
    writer_per.close()
    writer_sp.close()
    fp_rel.close()
    fp_pro.close()
    fp_sci.close()
    fp_man.close()
    fp_pol.close()
    fp_cre.close()
    fp_per.close()
    fp_sp.close()

    writer.close()
    fp.close()


def load_data(fname):

	data = []
	f = open(fname,'r', encoding='utf-8')
	data = ndjson.load(f)
	df = pd.DataFrame(data)
	return df

def main():
    print('load data into memory')
    df = load_data(datapath+combined)

    print('create managed queue...')
    #must use Manager queue here, or will not work
    manager = mp.Manager()
    q = manager.Queue()    
    #pool = mp.Pool(mp.cpu_count()-2)
    pool = mp.Pool(mp.cpu_count(), maxtasksperchild=100)
    print('create listener for saving of data...')
    #put listener to work first
    watcher = pool.apply_async(listener, (q,))
    #fire off workers
    jobs = []
    #create jobs
    print('make job queue...')
    for row in df.itertuples(): #get bytes for singular lines
        job = pool.apply_async(preprocess,(df.iloc[row[0]], q,))
        jobs.append(job)  
    print('collect results from jobs...')
    # collect results from the workers through the pool result queue
    for job in jobs: 
        job.get()
    print('kill all remaining workers...')
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool and exit :)')
    pool.close()
    pool.join()



if __name__ == "__main__":
    main()
