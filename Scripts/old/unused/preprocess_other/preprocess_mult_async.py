#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time
import psutil

##expects occupation ['ngram', n] as sys.argv


datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '/cobra/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'

grams = ['bigrams', 2]
grams = json.loads(sys.argv[2])
grams[1] = int(grams[1])
combinefile = 'combined_preprocess'
combined = 'combined_mult.ndjson'
#workers = cp.num_cores()
workers = 100

#regex for rpeprocessing
http = re.compile(r'(https{0,1}:\S+)')
www = re.compile(r'(www\.\S+)')
emoji = regex.compile(r'\X')
tagging = re.compile(r'#\S+')
retweet = re.compile(r'(RT\s{0,1}@\w+(?![@,\\])[\w]:{0,1})')
mention = re.compile(r'(@\w+(?![@,\\])\w)')
splitter = re.compile(r'(§RETWEET§|§MENTION§|§LINK§|§TAG§)')


def chunkify(fname,size=np.NaN):
    linebytes = []
    if np.isnan(size):
        print('size is nan...look at single lines only')
        i = 0
        #fileEnd = os.path.getsize(fname)

        with open(fname, 'r', encoding='utf-8') as f:

            nextLineByte = f.tell()
            while True:
                line = f.readline()
                line = ndjson.loads(line)[0]
                if line['occupation'] == sys.argv[1]:
                    linebytes.append(nextLineByte)
                nextLineByte = f.tell() #returns the location of the next line
                #if i == 10000:
                #   break
                #i+=1
                if not line or line == '':
                    break
                del line
    else:
        sys.exit('Chunking not yet implemented')


    return linebytes
def preprocess(text, q):

    text = retweet.sub('§RETWEET§', text)
    #find and substitute HTTP-URLs
    text = http.sub('§LINK§', text)
    #find and substitute www-URLs
    text = www.sub('§LINK§', text)
    #find and substitute mentions
    text = mention.sub('§MENTION§', text)
    #find and substitute Tagging
    text = tagging.sub('§TAG§', text)
    #split text
    text = splitter.split(text)
    #make character lists; keep unicode emojis as well as subsitutes as singel individual character
    text_l = []
    for el in text:
        if el not in ['§RETWEET§', '§LINK§', '§MENTION§','§TAG§']:
            text_l.extend(emoji.findall(el))
        else:
            text_l.append(el)
    del text
    gc.collect()

    return text_l

def ngrams(text, n)
    ngrams = zip(*[text[i:] for i in range(n)])
    return ["".join(ngram) for ngram in ngrams]

    return ngrams

def process_wrapper(chunkStart, q):
    with open(datapath+combined, 'r', encoding='utf-8') as f:
        f.seek(chunkStart)
        dic = f.readline()
        dic = ndjson.loads(lines)[0]
    
    dic['text_gram'] = ngrams(preprocess(dic.pop('text_org'), q), int(grams[1]))
    q.put(dic)
    del dic
    return 1





def listener(q):
    '''listens for messages on the q, writes to file. '''
   #print('finished making files..')
    sys.stdout.flush()
    prof = [sys.argv[1]]
    writers = []
    filehandles = []
    pathmodify = ['{}/'.format(grams[0])]
    types =['_{}'format(grams[0])]
    for i in range(0, len(types)):
        item = types[i]
        gramwriter = []
        #f = open(datapath+pathmodify[i]+combinefile+'{}.ndjson'.format(item), mode='a', encoding='utf-8')
        #writer = jsonlines.Writer(f, flush=True)
        #gramwriter.append(writer)

        #filehandles.append(f)

        for j in range(0, len(prof)):
            job = prof[j]
            f_p = open(datapath+pathmodify[i]+combinefile+'{}_{}.ndjson'.format(item, job), mode='a', encoding='utf-8')
            writer_p = jsonlines.Writer(f_p, flush=True)
            gramwriter.append(writer_p)
            filehandles.append(f_p)

        writers.append(gramwriter)
    print('made writers...')
    sys.stdout.flush()

    while True:
        #print('waiting for get...')
        #sys.stdout.flush()

        m = q.get() ##Q:
        #print('gotten...')
        #sys.stdout.flush()
        if m == 'kill':
            break
        
        writers[0][0].write(m)
        del m
        


    for r in range(0, len(writers)):
    	for w in range(0, len(writers[r])):
    		writers[r][w].close()

    for r in range(0,len(filehandles)):
    	filehandles[r].close()


def make_file_paths():
    prof = [sys.argv[1]]
    pathmodify = ['{}/'.format(grams[0])]
    types =['_{}'format(grams[0])]
    for el in pathmodify:
        os.makedirs(datapath+el, exist_ok=True)

    for i in range(0, len(types)):
        item = types[i]
        #f = open(datapath+pathmodify[i]+combinefile+'{}.ndjson'.format(item), mode='w', encoding='utf-8')
        #f.flush()
        #f.close()
        for j in range(0, len(prof)):
            job = prof[j]
            fp = open(datapath+pathmodify[i]+combinefile+'{}_{}.ndjson'.format(item, job), mode='w', encoding='utf-8')
            fp.flush()
            fp.close()



def main():
    pid = os.getpid()
    py = psutil.Process(pid)
    print('make files...')
    sys.stdout.flush()
    make_file_paths()
    print('create managed queue...')
    sys.stdout.flush()
    #must use Manager queue here, or will not work
    manager = mp.Manager()
    q = manager.Queue()    
    pool = mp.Pool(workers, maxtasksperchild=10000)
    print('create listener for saving of data...')
    sys.stdout.flush()
    #put listener to work first
    watcher = pool.apply_async(listener, (q,))
    #fire off workers
    jobs = []
    #create jobs
    print('collect lineBytes')
    sys.stdout.flush()
    #lineBytes = chunkify(datapath+combined)

    with open(datapath+'linebytes.json', 'r', encoding='utf-8') as f:
        lineBytes = json.load(f)
    n = 1000000
    lineBytes = [lineBytes[i:i + n] for i in range(0, len(lineBytes), n)] ##chunkify list

    print('make job queue...')
    sys.stdout.flush()
    for lineByte in lineBytes:
        for i in range(0, len(lineByte)):
            byte = lineByte.pop(0)
            job = pool.apply_async(process_wrapper,(byte, q,))
            jobs.append(job)
            #print('Process memory used by parent after async: {}\nVirtual Memory used by parent after async: {}\n\n'.format(py.memory_info()[0]*9.31*10e-10,py.memory_info()[1]*9.31*10e-10))
            if q.qsize() > 100000:
                time.sleep(10)
                sys.stdout.flush()
                gc.collect()

        # collect results from the workers through the pool result queue
        print('collect results from job cycle...')
        sys.stdout.flush()
        for i in range(0, len(jobs)):
            tmp = jobs.pop(0)
            tmp.get()
            del tmp
        print('sleep after cycle...')
        sys.stdout.flush()
        #time.sleep(20)

    print('kill all remaining workers...')
    sys.stdout.flush()
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool and exit :)')
    sys.stdout.flush()
    pool.close()
    pool.join()



if __name__ == "__main__":
    main()

