#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time



datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '/cobra/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'


feeds = 'feeds.ndjson'
labels = 'labels.ndjson'


combinefile = 'combined_preprocess'
#workers = mp.num_cores()
workers = 160

#regex for rpeprocessing
http = re.compile(r'(https{0,1}:\S+)')
www = re.compile(r'(www\.\S+)')
emoji = regex.compile(r'\X')
tagging = re.compile(r'#\S+')
retweet = re.compile(r'(RT\s{0,1}@\w+(?![@,\\])[\w]:{0,1})')
mention = re.compile(r'(@\w+(?![@,\\])\w)')
splitter = re.compile(r'(§RETWEET§|§MENTION§|§LINK§|§TAG§)')



#load labels
with open(datapath+labels, 'r', encoding='utf-8') as f:
    data = ndjson.load(f)
    lb = pd.DataFrame(data)



def preprocess(text, q):

    text = retweet.sub('§RETWEET§', text)
    #find and substitute HTTP-URLs
    text = http.sub('§LINK§', text)
    #find and substitute www-URLs
    text = www.sub('§LINK§', text)
    #find and substitute mentions
    text = mention.sub('§MENTION§', text)
    #find and substitute Tagging
    text = tagging.sub('§TAG§', text)
    #split text
    text = splitter.split(text)
    #make character lists; keep unicode emojis as well as subsitutes as singel individual character
    text_l = []
    for el in text:
        if el not in ['§RETWEET§', '§LINK§', '§MENTION§','§TAG§']:
            text_l.extend(emoji.findall(el))
        else:
            text_l.append(el)
    del text
    gc.collect()

    return text_l



def zipngram(text, q):
    ngrams = zip(*[text[i:] for i in range(2)])
    return ["".join(ngram) for ngram in ngrams]



def combine(dic, q):
    diclist = []
    i=0
    label = lb.loc[lb['id'] == dic['id']]
    for tweet in dic['text']:
        savedic = {}
        i +=1
        savedic['text_org'] = tweet
        savedic['tweet_id'] = str(dic['id'])+'_'+str(i)
        identifiers_l = str(dic['id'])+'_'+str(i)
        savedic['author_id'] = int(dic['id'])
        authorid_l = int(dic['id'])
        savedic['birthyear'] = int(label.iloc[0]['birthyear'])
        savedic['gender'] = label.iloc[0]['gender']
        savedic['occupation'] = label.iloc[0]['occupation']
        savedic['fame'] = label.iloc[0]['fame']
        savedic['bigram'] = zipngram(preprocess(savedic.pop('text_org', None), q), q=q)
        diclist.append(savedic)
    return diclist


def process_wrapper(chunkStart, fname, q, chunkSize=None):
    with open(datapath+fname, 'r', encoding='utf-8') as f:
        #f.seek(chunkStart)
        #lines = f.read(chunkSize)
        ##this is a list holding the processed lines in dictionary form (objectlike)
        f.seek(chunkStart)
        lines = f.readline()
        dic = ndjson.loads(lines)[0]
    res = combine(dic, q)
    q.put(res)
    return 1


def chunkify(fname,size=np.NaN):
    linebytes = []
    i = 0
    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            nextLineByte = f.tell()
            while True:
                linebytes.append(nextLineByte)
                line = f.readline()
                nextLineByte = f.tell() #returns the location of the next line
                #if i == 1000:
                #   break
                #i+=1
                if not line or line == '':
                    break
    else:
        sys.exit('Chunking not yet implemented')
    return linebytes
            
##########################################
def listener(q):
    prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']
    writers = []
    filehandles = []
    pathmodify = ['bigrams_dir/']
    types =['_bigrams']
    for i in range(0, len(types)):
        item = types[i]
        gramwriter = []
        f = open(datapath+pathmodify[i]+combinefile+'{}.ndjson'.format(item), mode='a', encoding='utf-8')
        writer = jsonlines.Writer(f, flush=True)
        gramwriter.append(writer)

        filehandles.append(f)

        for j in range(0, len(prof)):
            job = prof[j]
            f_p = open(datapath+pathmodify[i]+combinefile+'{}_{}.ndjson'.format(item, job), mode='a', encoding='utf-8')
            writer_p = jsonlines.Writer(f_p, flush=True)
            gramwriter.append(writer_p)
            filehandles.append(f_p)

        writers.append(gramwriter)
    print('made writers...')
    sys.stdout.flush()
    while True:
        #print('waiting for get...')
        #sys.stdout.flush()

        mess = q.get() ##Q:
        if mess == 'kill':
            break
        for i in range(0, len(mess)):
            index = prof.index(mess[i]['occupation'])+1
            writers[0][0].write(mess[i])
            writers[0][index].write(mess[i])
        del mess


    for r in range(0, len(writers)):
        for w in range(0, len(writers[r])):
            writers[r][w].close()

    for r in range(0,len(filehandles)):
        filehandles[r].close()
    print('writer is finished')
    sys.stdout.flush()





def make_file_paths():
    prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']

    pathmodify = ['bigrams_dir/']
    types =['_bigrams']
    for el in pathmodify:
        os.makedirs(datapath+el, exist_ok=True)

    for i in range(0, len(types)):
        item = types[i]
        f = open(datapath+pathmodify[i]+combinefile+'{}.ndjson'.format(item), mode='w', encoding='utf-8')
        f.flush()
        f.close()
        for j in range(0, len(prof)):
            job = prof[j]
            fp = open(datapath+pathmodify[i]+combinefile+'{}_{}.ndjson'.format(item, job), mode='w', encoding='utf-8')
            fp.flush()
            fp.close()



#####################################################################
##main function


def main():
    print('make files...')
    sys.stdout.flush()
    make_file_paths()
    print('create managed queue...')
    manager = mp.Manager()
    q = manager.Queue()    
    #pool = mp.Pool(mp.cpu_count()-2)
    pool = mp.Pool(workers, maxtasksperchild=100)
    print('create listener for saving of data...')
    #put listener to work first
    watcher = pool.apply_async(listener, (q,))
    #fire off workers
    jobs = []
    #create jobs
    print('make job queue...')
    print('collect lineBytes')
    sys.stdout.flush()
    lineBytes = chunkify(datapath+feeds)
    #n = 8000 ###48000 lines
    #lineBytes = [lineBytes[i:i + n] for i in range(0, len(lineBytes), n)] ##chunkify list
    #for lineByte in lineBytes:
    print('enter cycle...')
    sys.stdout.flush()
    for i in range(0, len(lineBytes)):
        byte = lineBytes.pop(0)
        job = pool.apply_async(process_wrapper,(byte, feeds, q,))
        jobs.append(job)

    print('collect results from jobs...')
    sys.stdout.flush()
    # collect results from the workers through the pool result queue
    for j in range(0, len(jobs)):
        tmp = jobs.pop(0)
        tmp = tmp.get()
        del tmp
    print('sleep after cycle...')
    sys.stdout.flush()
    time.sleep(20)
    print('kill all remaining workers...')
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool and exit :)')
    pool.close()
    pool.join()
    print('done')


if __name__ == "__main__":
    main()



