#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/output_async.out
#SBATCH -e ./out/errors_asnyc.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J async
# Queue:
#SBATCH --partition=medium
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
# for OpenMP:
#SBATCH --cpus-per-task=30
#SBATCH --mem=185000
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly
export OMP_PLACES=cores



module load gcc/8
module load anaconda/3/5.1

# Run the program:
srun python /cobra/u/mschuber/PAN/attributionfeatures/Scripts/preprocess_mult_async.py
echo "job finished"