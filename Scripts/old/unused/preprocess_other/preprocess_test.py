#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time
import psutil

datapath = '/cobra/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'


combinefile = 'combined_preprocess_mult'
combined = 'combined_mult.ndjson'


#regex for rpeprocessing
http = re.compile(r'(https{0,1}:\S+)')
www = re.compile(r'(www\.\S+)')
emoji = regex.compile(r'\X')
tagging = re.compile(r'#\S+')
retweet = re.compile(r'(RT\s{0,1}@\w+(?![@,\\])[\w]:{0,1})')
mention = re.compile(r'(@\w+(?![@,\\])\w)')



def chunkify(fname,size=np.NaN):
    linebytes = []
    if np.isnan(size):
        print('size is nan...look at single lines only')
        i = 0
        #fileEnd = os.path.getsize(fname)

        with open(fname, 'r', encoding='utf-8') as f:

            nextLineByte = f.tell()
            while True:
                linebytes.append(nextLineByte)
                line = f.readline()
                nextLineByte = f.tell() #returns the location of the next line
                #if i == 10000:
                #   break
                #i+=1
                if not line or line == '':
                    break
    else:
        sys.exit('Chunking not yet implemented')


    return linebytes
def preprocess(dic):

    #tweet_pre = ['§BEGIN§']
    tweet_pre = []
    tweet_pre.extend(retweet.split(dic.pop('text_org', None)))

    tweet_pre = [retweet.sub('§RETWEET§', tweet_pre[i]) for i in range(0, len(tweet_pre))]
    #tweet_pre.append('§END§')
    tmp_tweet = []
    #find and substitute HTTP-URLs
    for i in range(0, len(tweet_pre)):
        tmp = http.split(tweet_pre[i])
        tmp = [http.sub('§LINK§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute www-URLs
    for i in range(0, len(tweet_pre)):
        tmp = www.split(tweet_pre[i])
        tmp = [www.sub('§LINK§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
    
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute mentions
    for i in range(0, len(tweet_pre)):
        tmp = mention.split(tweet_pre[i])
        tmp = [mention.sub('§MENTION§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
        
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #find and substitute Tagging
    for i in range(0, len(tweet_pre)):
        tmp = tagging.split(tweet_pre[i])
        tmp = [tagging.sub('§TAG§', tmp[j]) for j in range(0, len(tmp)) if tmp[j] != '']
        tmp_tweet.extend(tmp)
        
    tweet_pre = tmp_tweet
    tmp_tweet = []
    #make character lists; keep unicode emojis as well as subsitutes as singel individual character
    for i in range(0, len(tweet_pre)):
        
        if tweet_pre[i] not in ['§RETWEET§', '§LINK§', '§MENTION§', '§TAGGING§', '§BEGIN§', '§END§']:
            tmp = emoji.findall(tweet_pre[i])
            tmp_tweet.extend(tmp)
        else:
            tmp_tweet.append(tweet_pre[i])
    #dic['text_pre'] = tmp_tweet
    #del tmp_tweet
    gc.collect()

    return dic, tmp_tweet


def ngrams(charlist):
    bigrams = []
    trigrams = []
    quadgrams = []
    leng = len(charlist)
    for i in range(0, leng):
        if i <leng-4:
            quadgrams.append(charlist[i] + charlist[i+1] + 
                              charlist[i+2] + charlist[i+3])
            trigrams.append(charlist[i] + charlist[i+1] + charlist[i+2])
            bigrams.append(charlist[i] + charlist[i+1])
        elif i <leng-3:
            trigrams.append(charlist[i] + charlist[i+1] + charlist[i+2])
            bigrams.append(charlist[i] + charlist[i+1])            
        elif i<leng-1:
            bigrams.append(charlist[i] + charlist[i+1])

    return [bigrams, trigrams, quadgrams]

def process_wrapper(chunkStart, q):
    pid = os.getpid()
    py = psutil.Process(pid)
    print('Process memory used by worker at start {}: {}\nVirtual memory used by worker at start {}: {}\n\n'.format(pid, py.memory_info()[0]*9.31*10e-10,
        pid, py.memory_info()[1]*9.31*10e-10))
    sys.stdout.flush()
    res = []
    grams = []
    with open(datapath+combined, 'r', encoding='utf-8') as f:
        f.seek(chunkStart)
        lines = f.readline()
        lines = ndjson.loads(lines)[0]
    print('Process memory used by worker after data load {}: {}\nVirtual memory used by worker after data load {}: {}\n\n'.format(pid, py.memory_info()[0]*9.31*10e-10,
        pid, py.memory_info()[1]*9.31*10e-10))
    sys.stdout.flush()
    dic, text_pre = preprocess(lines)
    res.append(dic)
    grams.append(text_pre)
    del text_pre
    del dic
    tmp = ngrams(grams[0])
    grams.extend(tmp)
    res.append(grams)
    q.put(res)
    print('Process memory used by worker after preprocess {}: {}\nVirtual memory used by worker after preprocess {}: {}\n\n'.format(pid, py.memory_info()[0]*9.31*10e-10,
        pid, py.memory_info()[1]*9.31*10e-10))
    sys.stdout.flush()
    del lines
    del grams
    del tmp
    return 'finished'





def listener(q):
     '''listens for messages on the q, writes to file. '''
    pid = os.getpid()
    py = psutil.Process(pid)
    print('Process memory used by listener: {}\nVirtual Memory used by listener: {}\n\n'.format(py.memory_info()[0]*9.31*10e-10,
        py.memory_info()[1]*9.31*10e-10))
    fp = open(datapath+combinefile+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    writer = jsonlines.Writer(fp, flush=True)
    while True:
        m = q.get()

        if m == 'kill':
            break
        dic = m[0]
        dic['text_pre'] = m[1]
        dic['text_bi'] = m[2]
        dic['text_tri'] = m[3]
        dic['text_quad'] = m[4]
        writer.write(dic)
        del dic
        del m
        print('Process memory used by listener after queue get: {}\nVirtual Memory used by listener after queue get: {}\n\n'.format(py.memory_info()[0]*9.31*10e-10,
            py.memory_info()[1]*9.31*10e-10))
        sys.stdout.flush()
    writer.close()
    fp.close()


def make_file_paths():
    prof = ['sports', 'performer', 'creator', 'politics', 'manager','science', 'professional', 'religious']

    pathmodify = ['singlegrams/', 'bigrams/', 'trigrams/', 'quadgrams/']
    types =['', '_bigrams', '_trigrams', '_quadgrams']
    for el in pathmodify:
        os.makedirs(datapath+el, exist_ok=True)

    for i in range(0, len(types)):
        item = types[i]
        f = open(datapath+pathmodify[i]+combinefile+'{}.ndjson'.format(item), mode='w', encoding='utf-8')
        f.flush()
        f.close()




        for j in range(0, len(prof)):
            job = prof[j]
            fp = open(datapath+pathmodify[i]+combinefile+'{}_{}.ndjson'.format(item, job), mode='w', encoding='utf-8')
            fp.flush()
            fp.close()



def main():
    pid = os.getpid()
    py = psutil.Process(pid)
    print('make files...')
    sys.stdout.flush()
    make_file_paths()
    print('create managed queue...')
    sys.stdout.flush()
    #must use Manager queue here, or will not work
    manager = mp.Manager()
    q = manager.Queue()    
    pool = mp.Pool(mp.cpu_count(), maxtasksperchild=1)
    print('create listener for saving of data...')
    sys.stdout.flush()
    #put listener to work first
    watcher = pool.apply_async(listener, (q,))
    #fire off workers
    jobs = []
    #create jobs
    print('collect lineBytes')
    sys.stdout.flush()
    lineBytes = chunkify(datapath+combined)

    with open(datapath+'linebytes.json') as f:
        json.dump(lineBytes, f)
    n = 20000
    #lineBytes = [lineBytes[i:i + n] for i in range(0, len(lineBytes), n)] ##chunkify list

    print('make job queue...')
    print('Process memory used by parent before async: {}\nVirtual Memory used by parent before async: {}\n\n'.format(py.memory_info()[0]*9.31*10e-10,
        py.memory_info()[1]*9.31*10e-10))
    sys.stdout.flush()
    for lineByte in lineBytes:
        job = pool.apply_async(process_wrapper,(lineByte, q,))
        jobs.append(job)
        print('Process memory used by parent after async: {}\nVirtual Memory used by parent after async: {}\n\n'.format(py.memory_info()[0]*9.31*10e-1,py.memory_info()[1]*9.31*10e-10))
        sys.stdout.flush()

        # collect results from the workers through the pool result queue
    print('get results from jobs')
    print('Process memory used by parent after async: {}\nVirtual Memory used by parent after async: {}\n\n'.format(py.memory_info()[0]*9.31*10e-1,
    py.memory_info()[1]*9.31*10e-10))
    sys.stdout.flush()
    for i in range(0, len(jobs)):
        tmp = jobs.pop(0)
        tmp.get()
        del tmp
        print('Process memory used by parent after job.get #{}: {}\nVirtual Memory used by parent after job.get #{}: {}\n\n'.format(i, py.memory_info()[0]*9.31*10e-10,
            j, py.memory_info()[1]*9.31*10e-10))
        sys.stdout.flush()


    print('Process memory used by parent after all jobs gotten: {}\nVirtual Memory used by parent after all jobs gotten: {}\n\n'.format(py.memory_info()[0]*9.31*10e-10,
        py.memory_info()[1]*9.31*10e-10))
    sys.stdout.flush()

    print('kill all remaining workers...')
    sys.stdout.flush()
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool and exit :)')
    sys.stdout.flush()
    pool.close()
    pool.join()



if __name__ == "__main__":
    main()

