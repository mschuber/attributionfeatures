#!/usr/bin/env python3
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import os
import random as rd
import matplotlib
import matplotlib.pyplot as plt
import json
import multiprocessing as mp
import re, regex

datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'

feeds = 'feeds.ndjson'
labels = 'labels.ndjson'
outputfile = 'combined_mult_test'





#load labels
with open(datapath+labels, 'r', encoding='utf-8') as f:
	data = ndjson.load(f)
	lb = pd.DataFrame(data)


def combine(dic, q):
    diclist = []
    i=0
    label = lb.loc[lb['id'] == dic['id']]
    for tweet in dic['text']:
        savedic = {}
        i +=1
        savedic['text_org'] = tweet
        savedic['tweet_id'] = str(dic['id'])+'_'+str(i)
        identifiers_l = str(dic['id'])+'_'+str(i)
        savedic['author_id'] = int(dic['id'])
        authorid_l = int(dic['id'])
        savedic['birthyear'] = int(label.iloc[0]['birthyear'])
        savedic['gender'] = label.iloc[0]['gender']
        savedic['occupation'] = label.iloc[0]['occupation']
        savedic['fame'] = label.iloc[0]['fame']
        diclist.append([savedic, authorid_l, identifiers_l])
    return diclist


def process_wrapper(chunkStart, fname, q, chunkSize=None):
    with open(datapath+fname, 'r', encoding='utf-8') as f:
        #f.seek(chunkStart)
        #lines = f.read(chunkSize)
        ##this is a list holding the processed lines in dictionary form (objectlike)
        f.seek(chunkStart)
        lines = f.readline()
        dic = ndjson.loads(lines)[0]
    res = combine(dic, q)
    q.put(res)
    return res


def chunkify(fname,size=np.NaN):
    linebytes = []
    if np.isnan(size):
        print('size is nan...look at single lines only')
        
        with open(fname, 'r', encoding='utf-8') as f:
            nextLineByte = f.tell()
            line = f.readline()
            while line:
                linebytes.append(nextLineByte)
                nextLineByte = f.tell() #returns the location of the next line
                line = f.readline()
    else:
        sys.exit('Chunking not yet implemented')
    return linebytes
            
##########################################
def listener(q):
    '''listens for messages on the q, writes to file. '''
    fp = open(datapath+outputfile+'.ndjson', mode='w+', encoding='utf-8')  # writable file-like object
    fp.flush()
    writer = jsonlines.Writer(fp)
    ident = open(datapath+'identifiers_mult_test.json', mode='w+', encoding='utf-8')
    aut = open(datapath+'author_id_mult_test.json', mode='w+', encoding='utf-8')

    while True:	
        m = q.get() ##Q:[ author[author/tweet[tweettext],[auhorid],[id]], [[],[],[]] ]
        if m == 'kill':
            break
    ##expects a list of objectlike
        for mess in m:
            #fp.write(json.dumps(mess[0])+'\n')
            writer.write(mess[0])
            aut.write('['+str(mess[1])+']\n')##authorid
            ident.write('["'+str(mess[2])+'"]\n') #identifier

            fp.flush()
            aut.flush()
            ident.flush()


    aut.close()
    ident.close()
    writer.close()
    fp.close()

#####################################################################
##main function


def main():
    print('create managed queue...')
    #must use Manager queue here, or will not work
    manager = mp.Manager()
    q = manager.Queue()    
    #pool = mp.Pool(mp.cpu_count()-2)
    pool = mp.Pool(mp.cpu_count())
    print('create listener for saving of data...')
    #put listener to work first
    watcher = pool.apply_async(listener, (q,))
    #fire off workers
    jobs = []
    #create jobs
    print('make job queue...')
    for lineByte in chunkify(datapath+feeds, size = np.NaN):
    #fs = open(datapath+feeds, 'r', encoding='utf-8')
    #byte = fs.tell()
    #fs.close()
    #for lineByte in [byte]:
        job = pool.apply_async(process_wrapper,(lineByte, feeds, q,))
        jobs.append(job)  
    print('collect results from jobs...')
    # collect results from the workers through the pool result queue
    for job in jobs: 
        tmp = job.get()
        del tmp
    print('kill all remaining workers...')
    #now we are done, kill the listener
    q.put('kill')
    print('closing down the pool and exit :)')
    pool.close()
    pool.join()


if __name__ == "__main__":
    main()



