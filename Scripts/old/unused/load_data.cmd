#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/output_load_data.out
#SBATCH -e ./out/errors_load_data.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J load_data
# Queue:
#SBATCH --partition=general
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly
export OMP_PLACES=cores


module load gcc/8
module load anaconda/3/5.1

# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/load_data.py
echo "job finished"