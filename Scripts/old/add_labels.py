
import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time



datapath = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
datapath = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
feeds = 'feeds.ndjson'
labels = 'labels.ndjson'
labels_new = {}

combinefile = 'combined_preprocess'




with open(datapath+labels, 'r', encoding='utf-8') as f:
    labels = ndjson.load(f)


for i in range(0, len(labels)):
	labels_new[str(labels[i]['id'])] =  labels[i]

with open(datapath+'labels_pre.json', 'w', encoding='utf-8') as f:
	json.dump(labels_new, f)