#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/concat.out
#SBATCH -e ./out/concat.err

# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J concat
# Queue:
#SBATCH --partition=small
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
##SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=16
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=cores
##export SLURM_HINT=multithread 


module load gcc/8
module load anaconda/3/5.1
module load scikit-learn/0.19.1

# Run the program:
srun python /draco/u/mschuber/PAN/attributionfeatures/Scripts/concat.py
echo "job finished"
