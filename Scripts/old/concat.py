#!/usr/bin/env python3

import pandas as pd
import numpy as np
import ndjson
import jsonlines
import json
import os
import sys
import random as rd
import json
import multiprocessing as mp
import re, regex
import gc
import psutil
import time
from shutil import copyfile







path = '/draco/ptmp/mschuber/PAN/Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#path = '../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/'
#dirs = ['workset','random_sample', 'life_phase', 'gender_balanced']
dirs = ['workset']
#life_phases = ['child_18', 'young_adult_35', 'adult_50', 'old_adult_65', 'retiree']
life_phases = []
grams = ['singlegram', 'bigram']


def check_existence(test, substring):

	if substring in test:
		res = True

	else:
		res = False

	return res



def subprocess_life(phase, di, gram):
	pid = os.getpid()
	print('worker {} starts with phase {}'.format(pid, phase))
	sys.stdout.flush()
	bigramfiles = []
	#singfiles = []
	for dirpath, dirnames, filenames in os.walk(path+di+'/'+phase):

		bigramfiles.extend([os.path.join(dirpath, fn) for fn in filenames if check_existence(fn, gram)])
		#singfiles.extend([os.path.join(dirpath, fn) for fn in filenames if check_existence(fn, 'singlegram')])

	print('worker {} copies first files'.format(pid))
	sys.stdout.flush()
	try:
		#copyfile(singfiles[0], path+di+'/'+phase+'/'+di+'_'+phase+'_preprocessed_singlegram.ndsjon')
		copyfile(bigramfiles[0], path+di+'/'+phase+'/'+di+'_'+phase+'_preprocessed_{}.ndsjon'.format(gram))
	except:
		print('The file {} I should copy to in order to start it off already exists. I am therefore finished.'.format(di+'_'+phase+'_preprocessed_singlegram.ndsjon'.format(gram)))
		sys.stdout.flush()
		return 1
	#make writers

	w = open(path+di+'/'+phase+'/'+di+'_'+phase+'_preprocessed_{}.ndsjon'.format(gram), 'a', encoding='utf-8')
	writer_ss = jsonlines.Writer(w, flush=True)
	#q = open(path+di+'/'+phase+'/'+di+'_'+phase+'_preprocessed_bigram.ndsjon', 'a', encoding='utf-8')
	#writer_bb = jsonlines.Writer(q, flush=True)
	for j in range(1, len(bigramfiles)):
		print('worker {} starts with file {} for phase {}'.format(pid, bigramfiles[j], phase))
		sys.stdout.flush()
		with open(bigramfiles[j], 'r', encoding = 'utf-8') as o:
			for line in o:
				tmp = ndjson.loads(line)[0]
				writer_ss.write(tmp)

		#print('worker {} starts with file {} for phase {}'.format(pid, singfiles[j], phase))
		#sys.stdout.flush()
		#with open(singfiles[j]) as o:
		#	for line in o:
		#		tmp = ndjson.loads(line)[0]
		#		writer_ss.write(tmp)
	#writer_bb.close()
	writer_ss.close()
	w.close()
	#q.close()
	print('worker {} finished with phase {}'.format(pid, phase))
	sys.stdout.flush()

	return 1




def analyze(di, gram):
	i= 0
	bigramfiles = []
	#singfiles = []
	pid = os.getpid()
	if di != 'life_phase':
		print('worker {} gets filenames for set {}'.format(pid, di))
		sys.stdout.flush()
		for dirpath, dirnames, filenames in os.walk(path+di):
			i+= 1
			bigramfiles.extend([os.path.join(dirpath, fn) for fn in filenames if check_existence(fn, gram)])
			#singfiles.extend([os.path.join(dirpath, fn) for fn in filenames if check_existence(fn, 'singlegram')])

		print('worker {} copies first files'.format(pid))
		sys.stdout.flush()
		try:
			copyfile(bigramfiles[0], path+di+'/'+di+'_preprocessed_{}.ndjson'.format(gram))
			#copyfile(bigramfiles[0], path+di+'/'+di+'_preprocessed_bigram.ndjson')
		except:
			print('The file {} I should copy to in order to start it off already exists. I am therefore finished.'.format(path+di+'/'+di+'_preprocessed_{}.ndjson'.format(gram)))
			sys.stdout.flush()
			return 1
		#make writers
		f = open(path+di+'/'+di+'_preprocessed_{}.ndjson'.format(gram), 'a', encoding='utf-8')
		writer_s = jsonlines.Writer(f, flush=True)
		#g = open(path+di+'/'+di+'_preprocessed_bigram.ndjson', 'a', encoding='utf-8')
		#writer_b = jsonlines.Writer(g, flush=True)
		for j in range(1, len(bigramfiles)):
			print('worker {} starts with file {}'.format(pid, bigramfiles[j]))
			sys.stdout.flush()
			with open(bigramfiles[j], 'r', encoding = 'utf-8') as o:
				for line in o:
					tmp = ndjson.loads(line)[0]
					writer_s.write(tmp)
			#print('worker {} starts with file {}'.format(pid, singfiles[j]))
			#sys.stdout.flush()
			#with open(singfiles[j]) as o:
			#	for line in o:
			#		tmp = ndjson.loads(line)[0]
			#		writer_s.write(tmp)
		#writer_b.close()
		writer_s.close()
		f.close()
		#g.close()

	else:
		bigramfiles = []
		#singfiles = []
		names = []

		
		for dirpath, dirnames, filenames in os.walk(path+di):
			names = dirnames
			break
		for name in names:
			for dirpath, dirnames, filenames in os.walk(path+di+'/'+name):
				bigramfiles.extend([os.path.join(dirpath, fn) for fn in filenames if check_existence(fn, gram)])
				#singfiles.extend([os.path.join(dirpath, fn) for fn in filenames if check_existence(fn, 'singlegram')])
				break
		copyfile(bigramfiles[0], path+di+'/'+di+'_preprocessed_{}.ndsjon'.format(gram))
		#copyfile(bigramfiles[0], path+di+'/'+di+'_preprocessed_bigram.ndsjon')
		#make writers
		f = open(path+di+'/'+di+'_preprocessed_{}.ndsjon'.format(gram), 'a', encoding='utf-8')
		writer_s = jsonlines.Writer(f, flush=True)
		#g = open(path+di+'/'+di+'_preprocessed_bigram.ndsjon', 'a', encoding='utf-8')
		#writer_b = jsonlines.Writer(g, flush=True)
		for j in range(1, len(bigramfiles)):
			print('worker {} starts with file {} for {}'.format(pid, bigramfiles[j], di))
			sys.stdout.flush()
			with open(bigramfiles[j], 'r', encoding = 'utf-8') as o:
				for line in o:
					tmp = ndjson.loads(line)[0]
					writer_s.write(tmp)
			#print('worker {} starts with file {} for {}'.format(pid, singfiles[j], di))
			#sys.stdout.flush()
			#with open(singfiles[j]) as o:
			#	for line in o:
			#		tmp = ndjson.loads(line)[0]
			#		writer_s.write(tmp)
		#writer_b.close()
		writer_s.close()
		f.close()
		#g.close()
	print('worker {} is done'.format(pid))
	sys.stdout.flush()
	return 1




def main():

	print('make pool')
	sys.stdout.flush()
	pool = mp.Pool(mp.cpu_count())
	print('enter cycle...')
	jobs = []
	sys.stdout.flush()
	for di in dirs:
		if di != 'life_phase':
			for gram in grams:
				job = pool.apply_async(analyze,(di, gram))
				jobs.append(job)
		else:
			for phase in life_phases:
				for gram in grams:
					job = pool.apply_async(subprocess_life,(phase, di, gram))
					jobs.append(job)


	for j in range(0, len(jobs)):
		tmp = jobs.pop(0)
		tmp = tmp.get()
		del tmp

	print('kill all remaining workers...')
	pool.close()
	pool.join()

	print('process life_phase')

	#for gram in grams:
	#	ret = analyze('life_phase', gram)

	print('done')


if __name__ == "__main__":
    main()














