#!/bin/bash -l


# Standard output and error:
#SBATCH -e ./../../jobscripts/out/subset_char_tag_dep_pos_err.%j
#SBATCH -o ./../../jobscripts/out/subset_char_tag_dep_pos_out.%j
# Initial working directory:
#SBATCH -D /ptmp/mschuber/PAN/attributionfeatures/Scripts/ML
#SBATCH -J char_tag_dep_pos
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=72
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00


module purge
module load gcc/10 impi/2021.2
module load anaconda/3/2020.02
module load tensorflow/cpu/2.5.0
module load scikit-learn/0.24.1

# Set the number of OMP threads *per process* to avoid overloading of the node!
export OMP_NUM_THREADS=1


srun python sparse_matrices.py -f char tag dep pos

echo "job finished"