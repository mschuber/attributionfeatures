#!/bin/bash -l


# Standard output and error:
#SBATCH -e ./../../jobscripts/out/dist_gender_err.%j
#SBATCH -o ./../../jobscripts/out/dist_gender_out.%j
# Initial working directory:
#SBATCH -D /ptmp/mschuber/PAN/attributionfeatures/Scripts/ML
#SBATCH -J  distG

# Queue:
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=37
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=10:00:00

module purge
module load gcc/10
module load anaconda/3/2020.02
module load tensorflow/cpu/2.5.0
module load scikit-learn/0.24.1

# For pinning threads correctly:
#export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#export OMP_PLACES=cores

# Set the number of OMP threads *per process* to avoid overloading of the node!
export OMP_NUM_THREADS=1


srun python dyn_aa.py $SLURM_CPUS_PER_TASK dynAA gender 100 250 500
echo "job finished"