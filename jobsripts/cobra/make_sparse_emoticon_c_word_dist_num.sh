#!/bin/bash -l


# Standard output and error:
#SBATCH -e ./../../jobscripts/out/subset_emoticon_c_word_dist_num_err.%j
#SBATCH -o ./../../jobscripts/out/subset_emoticon_c_word_dist_num_out.%j
# Initial working directory:
#SBATCH -D /ptmp/mschuber/PAN/attributionfeatures/Scripts/ML
#SBATCH -J  emoticon_c_word_dist_num

# Queue:
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=40
#SBATCH --mem=185000
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

module purge
module load gcc/10
module load anaconda/3/2020.02
module load tensorflow/cpu/2.5.0
module load scikit-learn/0.24.1

# For pinning threads correctly:
#export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#export OMP_PLACES=cores

# Set the number of OMP threads *per process* to avoid overloading of the node!
export OMP_NUM_THREADS=1


srun python sparse_matrices.py -f emoticon_c word dist num
echo "job finished"