#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./../jobscripts/out/comparison.%j
#SBATCH -e ./../jobscripts/error/comparison.%j
# Initial working directory:
#SBATCH -D /ptmp/mschuber/ehs/r
# Job Name:
#SBATCH -J comparison
#Partition
#SBATCH --partition=medium
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=20
# Enable Hyperthreading:
#SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=4
#
#SBATCH --mem=180000
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
#
# Wall clock limit:
#SBATCH --time 24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=threads
export SLURM_HINT=multithread 

# Run the program:
module load r_anaconda
srun Rscript cluster_script.R
