#!/bin/bash -l
# Standard output and error:
# #SBATCH --open-mode=truncate
#SBATCH -o ./out/Preprocessing/preproc_%j.out
#SBATCH -e ./out/Preprocessing/preproc_%j.err

# Initial working directory:
#SBATCH -D /ptmp/mschuber/PAN/Preprocessing
# Job Name:
#SBATCH -J preprocess
# Queue:
#SBATCH --partition=medium
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
# #SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=80
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
# For pinning threads correctly:
##export OMP_PLACES=cores
# Set the number of OMP threads *per process* to avoid overloading of the node!
#export OMP_NUM_THREADS=1

module purge
module load gcc/8
module load anaconda/3/2020.02
module load tensorflow/cpu/2.5.0
module load scikit-learn/0.24.1



##bash script foo multinode processing

names=(creator performer sports)
for i in ${names[@]}; do 
	# Run the program:
	srun -N 1-n1 python preprocess.py -p ../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31 -f workset_${typ}.ndjson -s ../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/preprocessed -c "(2,5)" -w "(1,3)" -t "(1,3)" -d "(1,5)" -o "(1,5)" --workset=workset --part=${typ} --rerun --both --asis --spacy --encase_list emoji emoticon
done
wait
echo "job finished"
