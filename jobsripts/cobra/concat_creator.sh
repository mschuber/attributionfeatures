#!/bin/bash -l


# Standard output and error:
#SBATCH -o ./../../jobscripts/out/preprocess_creat_out.%j
#SBATCH -e ./../../jobscripts/out/preprocess_creat.%j
# Initial working directory:
#SBATCH -D /ptmp/mschuber/PAN/attributionfeatures/Scripts/Preprocessing
# Job Name:
#SBATCH -J pre_creat
# Queue:
#SBATCH --partition=broadwell
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=1
# Enable Hyperthreading:
##SBATCH --ntasks-per-core=2#
#SBATCH --mem=20000
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:

export OMP_PLACES=cores
#export SLURM_HINT=multithread
# Set the number of OMP threads *per process* to avoid overloading of the node!
##export OMP_NUM_THREADS=1

module purge
module load gcc/8
module load anaconda/3/2020.02

typ=creator



srun python concat_output.py -s ../../Data/pan19-celebrity-profiling-training-dataset-2019-01-31/preprocessed --workset=workset --part=${typ} 

echo "job finished"