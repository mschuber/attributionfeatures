#!/bin/bash -l


# Standard output and error:
#SBATCH -o ./../../jobscripts/out/subset_err.%j
#SBATCH -e ./../../jobscripts/out/subset_out.%j
# Initial working directory:
#SBATCH -D /ptmp/mschuber/PAN/attributionfeatures/Scripts/ML
#SBATCH -J subsets
# Queue:
#SBATCH --partition=broadwell
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# Enable Hyperthreading:
#SBATCH --ntasks-per-core=2
# for OpenMP:
#SBATCH --cpus-per-task=80
#SBATCH --mem=50000
#SBATCH --mail-type=none
#SBATCH --mail-user=schubert@coll.mpg.de
# Wall clock limit:
#SBATCH --time=24:00:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:

export OMP_PLACES=threads
export SLURM_HINT=multithread
# Set the number of OMP threads *per process* to avoid overloading of the node!
##export OMP_NUM_THREADS=1

module purge
module load gcc/8
module load anaconda/3/2020.02



srun python make_subset.py

echo "job finished"