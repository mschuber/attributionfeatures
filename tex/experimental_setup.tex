\section{Experimental Design and Data}
In order to conduct our stability analysis, we conduct an experiment as used in the field of machine-learning by introducing controlled variations to an underlying, given dataset. To that end, we use a fixed setup of machine-learning (ML) models and test their internal stability, when they are exposed to these controlled variations.

\paragraph{Synopsis}
The dataset used for the experiment is the PAN @CLEF 2019 Celebrity Profiling (PAN2019) dataset.\footnote{The dataset may be downloaded from the website of the PAN challenge: \href{https://bit.ly/2Yk9so9}{PAN Challenge 2019}.}
As our goal is to assess te performance and the stability of importance in regards to single features, we try to reduce variation present directly within the authors as much as possible.
Therefore, we focus only on authors from one category, namely those dubbed ``creator''. As suggested in the guidelines of the original PAN challenge, we change the age from a numerical variable to their categorical one consisting of five age brackets.
As a prediction target, we select ``age" and ``gender", two commonly used characteristics in the social sciences.
Moreover, in order to exclude further any variation introduced by an imbalanced dataset, we undersample the data in such a way that the genders as well as the age groups are balanced.
For the comparison in the author dimension, we create four subsets consisting of 50, 150, 500, and 1000 authors, respectively.
The upper limit of 1000 authors reflects the maximum number of authors for whom it is still possible to balance the dataset.
Furthermore, we repeat the experiment three times for different minimum lenghts per training instance, as the text lengths were shown to impact classifier performance; in doing this, we hold the model setup constant \parencite{custodio2021stacked}.
The minimum lengths are 100, 250, and 500 characters, respectively. In order to achieve these minimum lengths, tweets from the same author were concatenated.
As feature types we use the following ones, sorted in ascending order in terms of encoded context information: DIST, CHAR, ASIS, POS, TAG, DEP, LEMMA, WORD, NUM. For all types, we apply the n-gram ranges found to be useful by prior research \parencite{custodio2021stacked}. The evaluation is conducted using the 500-score as the performance measure. To assess the stability of features, we use Spearman's Rho for rank order correlation. All evaluations were done on a separate hold-out dataset, the test set. That data was not used during training at any point.
In the following paragraphs, we outline our design choices in detail.

\subsection{Data}
While the literature for authorship analysis is abundant and only increased during recent years, there are no easily identified commonly used datasets across a wide range of studies.
Comparison between studies is therefore difficult.
This is well illustrated by \textcite{Neal2017SurveyingApplications} who list 13 datasets used more than once and a multitude of others used less frequently.
However, for Twitter, they list only two.
This means that most studies on authorship analysis additionally suffer from at least one of two limitations.
Either they focus on authorship analysis while using traditional, longer texts, such as articles or blog posts, or they make use of custom datasets \parencite{Neal2017SurveyingApplications}. The latter are sometimes described as a great challenge in the field of authorship analysis, making replication as all well verification of results difficult \parencite{Halvani2016AuthorshipTopics}.
The former implies that past studies not focusing on online short text messages are looking at a fundamentally different research problem compared to Twitter texts.
At the same time, most automated authorship analysis research acknowledges the fact that use cases for these tools will consist of attributing micro-blog texts to an author \parencite[see, for example,][]{Narayanan2012OnIdentification, Rocha2017AuthorshipForensics, Spitters2016AuthorshipForums}.
Consequently, the dataset used is from that platform, as its prevalence makes it especially relevant. Moreover, it reflects the text data, in style and characteristics commonly found for chat messages. Especially in terms of length, it is also similar to text data generated during studies and experiments within the social sciences.\\

%TODO Dateset table with variance and means
\input{tables/dataset.tex}
Most studies focusing on short-text online media such as Twitter use different data sets.
This is due to the fact that the user agreement for the API of this particular platform does not generally give permission to publish a scraped data set online \parencite{Theophilo2019AMicro-messages}.
At this point in time, we know of three public datasets: Twisty \parencite{Verhoeven2016Twisty:Profiling}, ISOT \parencite{Brocardo2015AuthorshipAuthentication}, and PAN \parencite{Stamatatos2015OverviewLab}, a yearly challenge tackling different aspects of authorship analysis.
The Twisty dataset includes a multitude of languages, making it unusable for this task as it was shown that language has major impact on the results \parencite{Halvani2016AuthorshipTopics}.
Another problem mentioned before concerns the high number of troll profiles, as well as potential alias accounts \parencite{Varol2017OnlineCharacterization} in an arbitrarily captured data set.
This is sometimes referred to as the ground truth problem \parencite{Narayanan2012OnIdentification}.
As the research question is focused on characteristics of individual people, this is particularly problematic.
For this reason, the ISOT dataset, too, is unusable, as neither the problematic of troll profiles nor the problem of double accounts for a single user can be addressed.\\

To overcome this, a special version of the PAN dataset focusing on profiling celebrities \parencite{PAN2019CelebrityProfiling} is used.
For this dataset it can at least be established that the accounts relate to a real, individual human.
Naturally, there may be new limitations, e.g., it may not be guaranteed that celebrities always write their own posts.
However, Twitter is more and more considered to be a medium offering the possibility of interacting directly with followers by circumventing the filter, interpretation, and comments of traditional media (thus enabling "authenticity") \parencite{Schmidt2014TwitterPublics}.
Consequently, the problem of other people messaging instead of the celebrities themselves is considered minor by the authors of the dataset when compared to the problem of having unknown fake profiles.

\subsection{Feature Engineering}
In order to use text input for machine-learning models, the text has to be transformed into a numerical representation. The chosen representation we call \textit{feature~type} here. Within one feature~type, there may be many features. For an example of two words, each may be mapped to a number, so there would be two features.\\
For automated authorship analysis, one may in principle choose from or combine a wide range of possible features for prediction.
The natural approach would be to use word-based features. However, this comes with the limitation that rather than finding features predictive of a certain gender or age, it is more likely that the topic is a latent variable driving the result.
As our goal is to control most of the information from outside the feature itself, e.g., topic or other context, the selection has to be more nuanced.
This brings us to character-based features.
Character n-grams, are based on concatenating characters; in the form of 1-grams they equal uni-grams, i.e., single characters. They are maybe one of the most commonly used feature sets within the literature \parencite{Rocha2017AuthorshipForensics}.
\textcite{Spitters2016AuthorshipForums} find in their exhaustive review of the literature that most studies employ them in one form or another.
This is due to the fact that such character n-grams were shown in multiple studies to perform robustly \parencite{Keselj2003N-gram-basedAttribution, Stamatatos2009AMethods, Peng2003LanguageModels}.
Some authors like \textcite{Forstall2010FeaturesSound} link this performance to the fact that n-grams are very closely related to pronunciation.
Moreover, due to the fact that the n-gram length may be reduced, many outside influences which introduce context in terms of topics, text type, and even language as a whole may be removed.
For example, the cross-domain analysis by \textcite{Stamatatos2013OnFeatures} shows that, compared to traditional word-based features, character n-grams outperform them in terms of cross-domain stability.
N-grams were also shown to capture many different features such as punctuation or spelling mistakes.
Regarding the length of n-grams, it must be noted that for English, those with a length of three and above are shown to capture content again partially, thus becoming topic-dependent \parencite{Narayanan2012OnIdentification, Spitters2016AuthorshipForums}.
Therefore, not only is the type of feature important when controlling for the relevance of topic and content but also the n-grams themselves are crucial. 
As such, we have a layered approach, controlling for the feature~type, while also varying the n-grams employed within one feature~type.
In the following, we construct a hierarchy ranging from the type of features mostly removed from content to the ones which partially capture content. In between, we can place those features which are still related to style and structure, but which necessitate a certain amount of text.
In terms of the actual feature types as well as range of the n-grams, this study mainly follows \textcite{custodio2021stacked} with the numerical features taken from \textcite{huang2020contribution}. It gives us the following types as input in ascending order, when compared on their context-dependency.

\paragraph{Text Distortion}
Symbols within text are usually disregarded for the standard approaches of text-based models. However, past research has shown that these features serve as valuable information in the context of authorship analysis \parencite{Stamatatos2017authorship}. For this feature~type, all a-z characters are mapped to ``*'', which only leaves punctuation and other markers. We call this type of feature \textit{DIST} and apply n-grams $\in[2,5]$

\paragraph{Character}
Character n-grams were shown to capture many idiosyncrasies present in text, while yielding a stable performance \parencite{Stamatatos2013OnFeatures}. Moreover, the amount of context present in the n-grams can easily be adjusted by their range \parencite{Rocha2017AuthorshipForensics}. Thus, we include them in the range of [2,5], referring to them as \textit{CHAR}.

\paragraph{Unprocessed Text}
In essence, this feature~type is a combination of text distortion and character n-grams. As input, the unprocessed text, including all special characters and punctuation, is transformed into n-grams. The n-gram range is also [2,5]. This feature~type we refer to as \textit{ASIS}.

\paragraph{Part-of-Speech}
Part-of-Speech tags capture linguistic style patters and general information such as grammatical classes, e.g. ``noun'' or ``verb''. We tag the text by employing the SpaCy\footnote{https://spacy.io} tagger. This feature~type is called \textit{POS} and the n-gram range is [1,3].

\paragraph{Language-specific morphological features}
Within a language, one is also able to find more fine-grained features related to morphology. Such features concern, for example, the gender of a word, tenses and others. To extract these, the tags generated by SpaCy on its \textit{TAG} level are used. Following this, we call this feature~type \textit{TAG} and employ the n-gram range [1,3].

\paragraph{Syntactic Dependencies}
This type of feature captures structural information, e.g., the use of the passive over the active voice. The dependencies are generated using SpaCy's dependency parser. We refer to it as \textit{DEP} and the n-gram range is [1,3] as well.

\paragraph{Lemma}
Lemmas are essentially word-like features, although the words are reduced to a common, lowercase form. For example, "I'm" would be converted to "i" and "am", while "played" and "playing" would both be mapped onto "play". In such a way, words are captured but not their transformations. We call this feature \textit{LEMMA} and include it with [1,2]-grams.

\paragraph{Words}
This feature~type is created by forming the n-grams directly from the words without any preprocessing besides lowercasing, and removing all characters that are not within the A-Z range. Again, we employ [1,2]-grams, the feature~type is called \textit{WORD}.

\paragraph{Numerical Features}
\textcite{huang2020contribution} additionally suggest numerical features describing the content and the form of the tweet. The feature~type \textit{NUM} is thus comprised of the following attributes: average tweet length, number of URLs, number of dates and times, number of emoticons, number of emojis, as well as polarity and subjectivity.

\paragraph{Preprocessing, Models, and Targets}
For the preprocessing, we apply the specific ones outlined above to each feature~type. In general, emojis and emoticons were always counted as one singular feature and marked by an $<EMOJI>$ or $<EMOTICON>$ token in the beginning and end. Furthermore, we replaced the unicode string by the textual description using the package demoji.\footnote{\url{https://pypi.org/project/demoji/}}
For all text-based features, we apply count vectorization and tf-idf scaling. The individual features were kept when they appeared in more than 1\% of the samples. For the feature~type NUM, we apply scaling and centering.
For the model, there is the option of linear and non-linear models. While neuronal nets and transfer-learning models gain huge popularity, for our case of authorship analysis, it turns out that simple linear models regularly outperform the more complex ones \parencite{Rocha2017AuthorshipForensics, custodio2021stacked}. Moreover, as we also address the social sciences as well as the law community, interpretability and transparency are key aspects. Thus, we focus here on well-researched models which also enable a mathematically global interpretation, as well as attribution of outcome to individual features of the input. While there is a wide range of models employed, the most common ones are a SVM, a logistic classifier, and Naive Bayes Classifiers \parencite{Rocha2017AuthorshipForensics}. We test all three of them and use the overall best-performing one for the evaluation. For the SVM, the analysis is limited to a linear kernel as only this type enables us to interpret the weight matrix directly in terms of feature importance.
As target, we selected two author characteristics of high relevance for the social sciences, namely \textit{age} and \textit{gender}. 


\subsection{Experimental Setup}
In order to assess how different combinations of feature types impact the outcome, we use three different approaches to feed them  into a classifier.
\begin{enumerate}
	\item Baseline: Here, the model gets only one feature~type (although with varying n-gram ranges). Thus, it enables us to compare the performance of individual feature types against one another.
	\item Cumulated: For this approach, we feed the classifier combinations of feature types such that we combine them in an ascending order in terms of context-content.
	\item Stacked: Here, too, we use different feature types as input. However, we first make predictions using individual feature types (as in the baseline setup) and then apply a second classifier, a logistic one, on top, using the predictions as input to predict the target again. That, in essence, is an ensemble approach \parencite{dietterich2000ensemble} and a variation of the successful DynAA model by \textcite{custodio2021stacked}.
\end{enumerate}

To assess the stability in performance as well as relevance, we compare the different feature types we introduce a small, controlled variation on the input data. In order to simulate (possible) shifting variations in the patterns of feature use, we vary the number of authors within the dataset. To that end, we construct four subsets from our dataset. Each subset is comprised of a different number of authors (50, 150, 500, 1000). Furthermore, the sets are constructed in such a way that all authors present in the smaller set are also present in all the larger ones. That means the 50 authors from the smallest set are part of all three larger sets as well. We chose that approach in order to increase the number, and thus the potential variation, while at the same time keeping prior information.
Moreover, the authors are balanced in gender as well as in age. That is necessary so that the model has no advantage by focusing on one class to the detriment of others. That gives us a cleaner result when analyzing the impact of the individual feature types as well as n-grams.\\

We conduct the whole experiment three times, varying the input length of the individual text instances each time. As previous research has shown that text length greatly influences the outcome \parencite{custodio2021stacked}, we construct input instances of different minimum lengths. We do so by concatenating different tweets by the same author together until the minimum length is reached. No n-grams are constructed in such a way that they would contain information from two different tweets. Naturally, when we increase the minimum length, the number of individual training instances declines, as more tweets are needed to form one training instance. As minimum lengths we use 150, 250, and 500 characters. The summary statistics for the dataset may be found in \autoref{tab:dataset}.\\

In order to have no spillover of information between evaluation and training, we split the dataset into two subsets, training and testing. All training was done on the training dataset, while all evaluations shown here are done on the hold-out test set. For the stacked approach, we split the test again, this time into a validation and test set. Here, the first layer of the model is trained with the training set, which is the same for all classifiers. The second layer is then trained on the validation set. Finally the evaluations are conducted on the hold-out test set.
Due to this setup, all classifiers are trained on exactly the same first-layer input in order to increase comparability. 


%%%%%%%%%%%%%%%%%%%%
\subsection{Evaluation Measures}
We seek to answer the question of stability in predictions as well as stability on the level of features.
For the former, we test the predictive power of the classifier for both targets on different inputs and sets varying in the number of authors. 
Our analysis compares different feature types and their performance against each other. Their difference lies in what they capture, especially in terms of the amount of context. We also analyze by how much their inclusion improves the model's performance. As the evaluation metric of choice, we use the \textit{macro} F1-score. The score is an equally weighted mean of precision\footnote{$True Positives * (Predicted Positives)^{-1}$.} and recall\footnote{$True Positives * Positives^{-1}$.}. Moreover, on a balanced dataset, the score is nearly equal to the accuracy. The score is bounded between 0 and 1, with 1 being the optimum.\\
Moreover, we analyze the performance on the author level. That helps us to test whether the models make systematic errors for specific authors. That is indeed important as it tells us something about how patterns, found to be predictive for target categories, may systematically disadvantage some individuals compared to others. For that part of the analysis, we look at author level accuracy as well as the stability in classifications patterns. For the latter, we evaluate confusion matrices. The results for that analysis may be found in \Cref{att:sec:prediction}.\\

The second, central aspect in this study is that of stability on the feature level. The question we try to answer here is by which degree stays the importance assigned to single features constant, when the classifier input-set used for training is slightly changed. The change introduced here is the increase in the number of authors. What we want to assess is by how much the importance of individual features shifts when such a change occurs. 
We developed the following approach: First, we extract the weight matrix of the two relevant models. When all input features are scaled to the same range as well as centered, the matrix contains the information about the relative importance of each feature when predicting the outcome. As we are using linear models, these weights are global, i.e., the importance assigned to a feature is the same no matter which individual instance is assessed.\\

To assess the potential shift in importance, we rank the individual features in terms of weights assigned. In a second step, we then calculate Spearman's $\rho$ in order to assess  by how much the importance placed on individual input features shifts when introducing a small variation in the underlying data. The reason why this works is because the ranking of a feature directly reflects the weight the classifier places on it. Within our linear models, this is a direct mapping on its importance to the prediction result. Thus, when this coefficient is 1, the distribution of importance across features is completely identical; if it were -1, it would be completely inverse. Consequently, we say the stability is high for values going towards 1, while values close to 0 imply that there is no recognizable relationship, and thus very high instability.
Moreover, we selected an ordinal measure, as it allows for more latitude. While the absolute values of the coefficients might change (and indeed have to when more features are included), their ordering may still be constant. Consequently, their importance when compared to each other can still be stable. That means that any stability found here is to be considered the upper bound.\\

However, when increasing the number of authors, the underlying feature set might also increase and thus the two matrices do not have the same dimensionality anymore. To tackle that problem, we follow two approaches: The first is to expand the smaller matrix by adding columns of $\infty$ for the missing features. This ensures, that these will always be assigned the highest possible rank in the smaller matrix (the rankings are sorted in ascending order during comparison). We call this the \textit{extended} Spearman correlation and it enables us to assess the absolute feature importance ranking. The second option is to assess only the features present in both matrices, and therefore to reduce the dimensionality of the larger one. This ensures that we assess relative importance, but ignore that additional features might have great influence on the outcome. That we refer to as the \textit{reduced} Spearman correlation. The result for the analysis of the feature importance and its stability may be found in \Cref{att:sec:importance}.



%%%%%%%
%%%%%%%

%%%%%%%

%%%%%%%%

%%%%%%
%%%%%%
