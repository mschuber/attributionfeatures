\section{Stability of Predictions}
\label{att:sec:prediction}
The results were computed on HPC system, making use of 72 cores with 256GB of RAM.\footnote{\href{https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html}{MPCDF HPC System "Raven".}}
Of our classifiers, the SVM outperformed the logistic classifier as well as the Naive Bayes Classifier as a first-layer model.
Thus, for all results a SVM was used as the first-layer model. For the stacked model, a logistic classifier was used for the second layer to stay close to the setup by \textcite{custodio2021stacked}.
Overall, per target, the result of the experimental setup is comprised of 1200 SVMs as well stacked models, i.e., 2400 models in total for both targets.
To enable a concise analysis, we opted to showcase the results for the text set for which individual inputs have a length of 500 characters. The results for the other text sets may be found in the Appendix.

\subsection{Aggregate Overview for Feature-Stability}
First, we will look at the aggregated results for our experiments. In \autoref{fig:f1_500}, we see the F1-scores for the classifiers trained on the dataset with a minimum character length of 500 per instance. As in previous results, the score declines markedly in the number of authors. However, especially the models trained either with different feature types as input (labeled "cumulated") or those trained similarly to the DynAA model by \textcite{custodio2021stacked} (labeled "stacked") perform consistently. 
Moreover, overall the results are in line with previous top-performing results on the PAN2019 dataset \parencite{wiegmann2019overview}.
The baseline models trained on feature sets consisting of singular types have a high variance in performance. That is not surprising, when we look at individual models, each trained on one type of features. As can be seen in \autoref{tab:gender_f1_dist_500_baseline_individual} and \autoref{tab:age_f1_dist_500_baseline_individual}, the performance is on the upper end for the feature types such as CHAR with an $F1_{50}^{CHAR-2}$ up to 0.88/0.82  and on the lower end for feature types such as NUM with an $F1_{50}^{NUM}$ of 0.59/0.29 for the targets gender/age.
Consequently, the feature types used, as well as their combinations, not only have a great impact on the outcome, but some features do encode little or next to no information for our classification task. Hence, we exclude those from our further analysis.
The same findings apply to the models trained on instances with a minimum length of 100 characters and 250 characters respectively (\autoref{fig:f1_100} and \autoref{fig:f1_250} in the Appendix).
\begin{figure}[!htpb]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/gender/f1_scores_500.pdf}
		\caption{Results for target \textit{gender}.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/age/f1_scores_500.pdf}
		\caption{Results for target \textit{age}.}
	\end{subfigure}
	\caption*{\scriptsize \textit{Notes}: The figure shows boxplots for the F1-score of all models estimated for a given combination of feature types used and way of input, i.e., baseline, cumulated, or stacked.}
		\caption{F1-score input instance length of 500 characters.}
	\label{fig:f1_500}
\end{figure}
Having comparable results in terms of accuracy and f1-score to what is found in the literature for this dataset serves as a basis for our following evaluation.
A high performance lends credence to the assumption that our classifier is indeed working well and extracting the relevant information from the input data.
In reverse, we may therefore assume that the features used are indeed those holding the relevant information for the respective task. Thus, our approach of extracting the feature importance via the associated weights is sensible.\\
\autoref{fig:ext_spearman_500} shows the results for the extended distortion calculated via Spearman's Rho.
The comparison is always done between two models, varying in the amount of authors the respective model is trained on.
In the comparison, model 1 is trained on the lower number of authors (e.g., 50) whereas model 2 is trained on the next-highest number of authors (e.g., 150). Overall, we thus have 3 comparisons in the number of authors.\\
For target \textit{gender}, on average, and irrespective of the model, we find only little correlation when increasing the number of authors from 50 to 150 ($0.05 < \rho < 0.20$). For the others, when increasing the number of authors, correlation goes down to a maximum of $0.05$. At the same time, we show that the performance in terms of f1-score remains relatively stable.
This implies that the stability in performance comes at the expense of the stability in feature importance. When increasing the number of authors, different features are therefore predictive in terms of the target. These findings do not generally imply that the previous features lose their importance. The do however mean that, when ordering all features in terms of the associated importance, the ordering changes fundamentally. That fact is reflected in Spearman's Rho.
Regardless by how much we increase the number of authors, on average the correlation lies between 5\% and 15\% for all models.
That is interesting, as the number of features additionally available when increasing the number of authors is never above 30\%. Thus, it cannot be that mainly those additional features are the ones being used for predictions, as the correlation coefficient would then still be higher than what we find. 
Rather, it seems to be the case that the model weighs the previous and new features in such a way that the new ordering imposed  differs completely from the previous one.

\begin{figure}[!htpb]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/gender/spearman_ext_500.pdf}
		\caption{Results for target \textit{gender}.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/age/spearman_ext_500.pdf}
		\caption{Results for target \textit{age}.}
	\end{subfigure}
	\caption*{\scriptsize \textit{Notes}: The figure shows the boxplots for the extended $\rho$ of all models estimated for a given combination of feature types used and way of input, i.e., baseline, cumulated, or stacked.}
	\caption{Extended Spearman correlation input instance length of 500 characters.}
	\label{fig:ext_spearman_500}
\end{figure}
Similar results are found for the target \textit{age}. However, here the decline in correlation is even more unidirectional when increasing the number of authors (increase in authors yields a decline in correlation).\\

The only outlier to these results is the baseline model trained on individual feature types. While on average the correlation is the same as for the models on combinations of feature types, the outliers show a very high positive correlation (up to $\rho_{150|50}^{NUM}: 0.42$, see \autoref{tab:age_f1_dist_500_baseline_individual}). 
While that may look like it stands in opposition to the other results, their low predictive power helps to explain this phenomenon. \autoref{tab:age_f1_dist_500_baseline_individual} shows that the F1-score for the prediction of \textit{age} is only at $F1_{150}^{NUM}:0.25$ with the random guess benchmark being 0.2. Thus, while the feature importance remains stable for some features, their predictive power is negligible. Thus, the importance assigned to these features may simply be random noise without any signal.
As such, when looking at the average feature stability over all feature type sets, the conclusion is that the features are, on average, not stable and the distortion of importance when increasing the number of authors in a dataset is already high for a low number of authors (from 50 to 150).
These findings hold regardless of the character length of the input text, as \autoref{fig:ext_spearman_100} and \autoref{fig:ext_spearman_250} in the Appendix show.

\subsection{Author-Level Analysis}
For the author-level analysis, we look at the results gained by feeding the classifier the full set of feature types in a cumulated way. That choice assures that our results are predicted making use of the full information. Furthermore, cumulating the input yields the best results overall (compare \autoref{sec:att_app_tables}). In that way, the analysis is done using the best possible set. However, the findings hold for any of the feature-type approach combinations.\\

\autoref{fig:acc_auth_500}a shows the error at the author level when predicting gender. We see, that overall we have very few authors for which the accuracy is lower than the random-guess accuracy (0.5), i.e, for which our prediction error is higher than 0.5. We see that the relative number of authors for which the classifier performs below the random-guess threshold stays stable, when compared to the number of authors in the set. Consequently, the relative overall-classification performance at the author level stays stable, even when increasing the the number of authors in the set. However, there is a small but stable proportion of authors for which the classifier is \textit{systematically unable} to predict the target correctly.
\begin{figure}[!htpb]
	\centering
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/gender/500/bars/proportions_cumulated_dist_char_asis_pos_tag_dep_lemma_word_emoticon_c_polarity_num_1_500_color_gender.pdf}
		\caption{Author-level errors for target \textit{gender}.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/age/500/bars/proportions_cumulated_dist_char_asis_pos_tag_dep_lemma_word_emoticon_c_polarity_num_1_500_color_centered_age.pdf}
		\caption{Author-level errors for target \textit{age}.}
	\end{subfigure}
	\caption*{\scriptsize \textit{Notes}: The figure shows the results when using the full feature set as cumulated input. Each author is a unique instance on the x-axis. The proportion per author is then shown as the y-value. The authors are sorted by their appearance in the respective subsets (i.e., 50, 150, 500, 1000) and according to the proportion of errors within those subsets. The result per author shows the result over all subsets.}
	\caption{Author-level results for the full feature set with an input instance length of 500 characters.}
	\label{fig:acc_auth_500}
\end{figure}
When looking at the distribution of the errors across genders, we find that a higher number of those authors for whom our classifier makes systematic errors seems to be female. 
On the high level, we find that the performance remains relatively stable when we increase the number of authors as depicted in \autoref{fig:gender_conf_auth_500}a. However, as soon as we reach the two upper-most brackets of authors, we see that the result for male authors remains relatively stable, while the outcome for female authors declines markedly from $acc_{150}^{Female}:0.82$ to $acc_{1000}^{Female}:0.69$. For the same sets, the mostly stable accuracy for males declines only from $acc_{150}^{Male}:0.83$ to $acc_{1000}^{Male}:0.80$. Looking closer, we can see that that drop for females happens when the number of authors increases from 150 to 500. The implication is thus that we add female authors who are systematically difficult to classify. Bringing this together with our observations from before, we include only those authors in \autoref{fig:gender_conf_auth_500}b for whom the classifier performs worse than the random-guess threshold. Here, we find some support for our previous assumption. When looking at the errors we make for female authors, we see that, starting from 500 authors onward, the number of those below the random-guess threshold jumps up.\\

While that seems to point to the fact that apparently female authors are difficult to classify, the true reason may be slightly more nuanced. When looking at the male authors in \autoref{fig:gender_conf_auth_500}b, we see that, while obviously low in absolute numbers, the pattern is inverse for the datsets comprised of 50 and 150 authors. The underlying driver, however, does not seem to be the gender \textit{per se}, but rather the lack of stability in regards to feature importance. Per design, we limited the amount of features for each feature type to those appearing at least in 1\% of all training instances. Hence, the number of features for word-based feature types increases only \textit{sublinearly} relative to the number of authors. Thus, the amount of author-individual fitting the classifier is able to achieve declines. Indeed, that is the very essence of reducing overfitting. However, as shown in \autoref{fig:ext_spearman_500}a, the stability of feature importance is low. Taken together, that simply means that systematic patterns within a limited number of features for authors of the same gender decline when the number of authors increases, i.e., the patterns seem to be merely correlational -- they start to break down or become unstable and more complex. The underlying reason is that additional authors introduce new features, while using the old features in a different way. As the classifier is only able to estimate one weight per feature and the number of additional features is limited, the ability to represent all the necessary information declines.\\
\begin{figure}[!htpb]
	\centering
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/gender/500/confusion/confusion_cumulated_dist_char_asis_pos_tag_dep_lemma_word_emoticon_c_polarity_num_1_500_mode_basic.pdf}
		\caption{All authors (row-wise normalization).}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/gender/500/confusion/confusion_cumulated_dist_char_asis_pos_tag_dep_lemma_word_emoticon_c_polarity_num_1_500_mode_major_false.pdf}
		\caption{Below random guess accuracy (matrix-wise normalization).}
	\end{subfigure}
	\caption*{\scriptsize \textit{Notes}: The figure shows confusion matrices for the results produced by using the full feature set as cumulated input on an input instance length of 500 Characters. The matrix for the respective set of authors is calculated by looking at the respective set in isolation.}
	\caption{Confusion matrices for target \textit{gender}.}
	\label{fig:gender_conf_auth_500}
\end{figure}

For the target \textit{age}, we also find only comparatively few authors with an average accuracy below the random-guess threshold of $0.2$ (see \autoref{fig:acc_auth_500}b). When comparing it to the results of the target \textit{gender}, it becomes clear that the number of those below the threshold jumps up significantly when the number of authors increases from 150 to 500. Moreover, it seems to be the case that authors of the intermediate age brackets (\textit{1975} and \textit{1985}) seem to be more difficult to classify. Overall, the patterns seem to be less pronounced when compared to the ones found for \textit{gender}.
Looking at the category-wise analysis presented in \autoref{fig:age_conf_auth_500}a, we find that, overall there are only few pronounced patterns of confusion. As already suggested by \autoref{fig:acc_auth_500}b, only the intermediate age brackets have a systematic pattern. Especially for the datasets consisting of 500 and 100 authors, the confusion between the true age bracket \textit{1985} and the youngest age~bracket \textit{1995} is pronounced. Here, only 38\% of the instances are classified correctly as \textit{1985}, while 28\% are confused as \textit{1995}. While here the interpretation might be that the distinction between the youngest authors might be difficult, the results of \textit{1975}, the intermediate category, make it more difficult. We see that the confusion with the category \textit{1963} as well as \textit{1995} is of similar size. It might be that, instead of predicting only \textit{age}, the classifier picks up on a proxy in the way people express themselves. While certainly dependent on age in terms of punctuation for older authors \parencite{Flekova2016ExploringTwitter} as well as on stability of language use in younger authors \parencite{DeJonge2012TextmessageStudents}, the way of expression also depends on the groups to which we belong \parencite{Chan2018SocialTwitter}. Consequently, some of those authors confused might simply be part of peer groups where the mode of expression is reflective of younger age brackets. As a consequence, they get misclassified.
These relatively distinctive patterns for the oldest and youngest authors are also most likely the reason why the prediction accuracy for those is markedly high, even for the set with the highest number of authors.\\

When looking only at those authors below the random-guess threshold, as depicted in \autoref{fig:age_conf_auth_500}b, we find that there are only two age brackets for which we have a systematic and pronounced confusion. For the set with 150 authors, this is the age bracket \textit{1947}, which is most often confused with the two adjacent age brackets \textit{1963} and \textit{1975}. For the set with 1000 authors, the age bracket \textit{1985} is mostly confused with belonging either to the youngest or the oldest age bracket.
As that bracket is also overall the most confused one, it stands to reason that the variance in expression is the highest. Consequently, there is no pronounced pattern in the features on which the classifier is able to pick up.\\
\begin{figure}[!htpb]
	\centering
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/age/500/confusion/confusion_cumulated_dist_char_asis_pos_tag_dep_lemma_word_emoticon_c_polarity_num_1_500_mode_basic.pdf}
		\caption{All authors (row-wise normalization).}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/age/500/confusion/confusion_cumulated_dist_char_asis_pos_tag_dep_lemma_word_emoticon_c_polarity_num_1_500_mode_major_false.pdf}
		\caption{Below random guess accuracy (matrix-wise normalization).}
	\end{subfigure}
	\caption*{\scriptsize \textit{Notes}: The figure shows confusion matrices for the results produced by using the full feature set as cumulated input on an input instance length of 500 Characters. The matrix for the respective set of authors is calculated by looking at the respective set in isolation.}
	\caption{Confusion matrices for target \textit{age}.}
	\label{fig:age_conf_auth_500}
\end{figure}

